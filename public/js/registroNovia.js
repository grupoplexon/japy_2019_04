$(document).ready(function() {
    var form = $('#formRegister');
    form.submit(function (e) {
        e.preventDefault();
        if($('#password').val()==$('#last_password').val()) {
            $.ajax({
                dataType: 'json',
                url: $('#URL').val()+'checkUser',
                method: 'POST',
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content"),
                    user: $('#email').val(),
                },
                success: function(response) {
                    if (response.data == 'exist') {
                        swal('Error', 'El correo ya fue registrado, intente con otro.', 'warning');
                        return false;
                    } else {
                        console.log('registrar');
                        $('#formRegister').unbind('submit').submit();
                    }
                },
                error: function() {
        
                }
            });
        } else {
            swal('Error', 'Las contraseñas no coinciden', 'warning');
            return false;
        }
    });
});