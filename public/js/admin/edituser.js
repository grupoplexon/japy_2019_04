var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    $('.modal').modal();
    $('#newState').on('click', () => {
        $('#modal1').modal('open');
    });
    $('.closeModal').on('click', () => {
        $('#modal1').modal('close');
    });
    $('#savegeneral').on('click', function() {
        // alert($('#name').val()+' '+$('#email').val()+' '+$('#user').val()+' '+$('#password').val()+' '+$('#confirmpassword').val());
        if($('#name').val()!='' && $('#user').val()!='') {
            if($('#password').val()==$('#confirmpassword').val()) {
                $.ajax({
                    dataType: 'json',
                    url: URLactual+'savegeneral',
                    method: 'post',
                    data: {
                        "_token": $("meta[name='csrf-token']").attr("content"),
                        id: $('#idUser').val(),
                        name: $('#name').val(),
                        user: $('#user').val(),
                        password: $('#password').val()
                    },
                    success: function(response) {
                        if(response.status=='ok') {
                            swal("Los datos fueron modificados", {
                            icon: "success",
                            });
                            setTimeout(function(){
                                location.href =URLactual+"usuarios";
                            },1000);
                        }
                    },
                    error: function() {
                        console.log('error');
                    },
                });
            } else {
                swal("Las contraseñas no coinciden", {
                icon: "error",
                });
            }
        } else {
            swal("El nombre y usuario son obligatorios", {
            icon: "warning",
            });
        }
    });
});

$('#newStateSelect').on('change', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractCities',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            idState: $('#newStateSelect').val(),
        },
        success: function(response) {
            var options = '<option value="">Seleccione una Ciudad</option>';
            for (var i = 0; i < response.cities.length; i++) {
                options += '<option value="'+response.cities[i].city+'">'+response.cities[i].city+'</option>';
            }
            document.getElementById('newCity').innerHTML = options;
        },
        error: function() {
            console.log('error');
        }
    });
});

$('#saveNew').on('click', function() {
    if($('#newStateSelect').val()!='' && $('#newCity').val()!= '' && $('input:radio[name=group2]:checked').val()!='') {
        $.ajax({
            dataType: 'json',
            url: URLactual+'saveNewLocation',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                idUser: $('#idUser').val(),
                state: $('#newStateSelect option:selected').html(),
                city: $('#newCity').val(),
                nameContact: $('#newContact').val(),
                email: $('#newEmail').val(),
                phone: $('#newPhone').val(),
                cellphone: $('#newCellphone').val(),
                website: $('#newWebsite').val(),
                active: $('input:radio[name=group2]:checked').val()
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal("Datos guardados correctamente", {
                    icon: "success",
                    });
                    setTimeout(function() {
                        location.reload();
                    },1500);
                } else {
                    swal("Lo sentimos ocurrio un error", {
                    icon: "error",
                    });
                }
            },
            error: function() {
                console.log('error');
            }
        });
    } else {
        swal("Por favor complete los campos obligatorios", {
        icon: "error",
        });
    }
});

$('#saveContact').on('click', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'editContact',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: $('#state').val(),
            nameContact: $('#contact').val(),
            email: $('#email').val(),
            phone: $('#phone').val(),
            cellphone: $('#cellphone').val(),
            website: $('#website').val(),
            active: $('input:radio[name=group1]:checked').val()
        },
        success: function(response) {
            if(response.status=='ok') {
                swal("Datos guardados correctamente", {
                icon: "success",
                });
            } else {
                swal("Ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
});

$('#state').on('change', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractLocations',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: $('#state').val()
        },
        success: function(response) {
            $('#contact').val(response.data.name_contact);
            $('#phone').val(response.data.phone);
            $('#cellphone').val(response.data.cellphone);
            $('#website').val(response.data.website);
            $('#email').val(response.data.email);
            if(response.data.active==0) {
                $('#radioEnabled').attr('checked', false);
                $('#radioDisabled').attr('checked', true);
            } else {
                $('#radioDisabled').attr('checked', false);
                $('#radioEnabled').attr('checked', true);
            }
        },
        error: function() {
            console.log('error');
        },
    });
});

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') - 4);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}