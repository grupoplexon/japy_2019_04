var URLactual = getAbsolutePath();
var $select;
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    var table = $('#usersTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{  
            url: URLactual+'extractusers',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content")
            }, 
        },
        "columns": [
            {data: 'id'},
            {data: 'name'},
            {data: 'user'},
            {
                "targets": 3,  
                "render": function (data, type, row, meta){
                    if(row.role_user.role_id==3) {
                        return '<p>Proveedor</p>';
                    } else if(row.role_user.role_id==2) {
                        return '<p>Cliente</p>';
                    } else {
                        return '<p>Administrador</p>';
                    }
                }
            },
            {  
                "targets": 5,  
                "render": function (data, type, row, meta){
                    return '<a href="edit/'+row["id"]+'"><button type="button" class="btn blue">Editar</button></a>';
                }
            },
        ],
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    } );
});

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}