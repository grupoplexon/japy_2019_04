var URLactual = getAbsolutePath();
var $select;
var selectCategories = '';
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    categories();
    var table = $('#providersTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{  
            url: URLactual+'extractproviders',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content")
            }, 
        },
        "columns": [
            {data: 'name'},
            {  
                "targets": 1,
                "width": "15%",
                "render": function (data, type, row, meta) {
                    $select = $('<select class="inputs large" id="account_type"><option value="0">Basico</option><option value="1">Plata</option><option value="2">Oro</option><option value="3">Diamante</option></select>');
                    $select.find('option[value="'+row.provider.account_type+'"]').attr('selected', 'selected');
                    return $select[0].outerHTML;
                }
            },
            {
                "targets": 2,
                "width": "15%",
                "render": function(row) {
                    var options = '';
                    options += '<select class="inputs large" id="time">';
                    options += '<option value="" selected disabled>Seleccione</option>';
                    for (var i = 1; i < 13; i++) {
                        if(i==1) {
                            options += '<option value="'+i+'">'+i+' mes</option>';
                        } else {
                            options += '<option value="'+i+'">'+i+' meses</option>';
                        }
                    }
                    options += '</select>';
                    return options;
                }
            },
            {
                "targets": 3, 
                "render": function (data, type, row, meta) {
                    if(row.provider.expiration!='' && row.provider.expiration!=null) {
                        fecha = new Date(row.provider.expiration.replace(/-/g, '\/'));
                        var options = { year: 'numeric', month: 'long', day: 'numeric' };
                        var date = fecha.toLocaleDateString("es-ES", options);
                        return '<span>'+date+'</span>';
                    } else {
                        return '<span>Aun no se asigna</span>';
                    }
                }
            },
            {
                "targets": 4,
                "width": "15%",
                "render": function (data, type, row, meta){
                    $select = $('<select class="inputs large select'+row.id+'" id="active"><option value="0">Desactivado</option><option value="1">Pendiente</option><option value="2">Activo</option></select>');
                    $select.find('option[value="'+row.active+'"]').attr('selected', 'selected');
                    return $select[0].outerHTML;
                }
            },
            {
                "targets": 5,
                "width": "20%",
                "render": function (data, type, row) {
                    $select = $(selectCategories);
                    $select.find('option[value="'+row.provider.category_id+'"]').attr('selected', 'selected');
                    return $select[0].outerHTML;
                }
            },
        ],
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    } );
    $('#providersTable tbody').on( 'change', 'select', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        var options = $(this).find('option:selected').val();
        let tableIndice = this.id;
        if(data) {
            updateData(tableIndice, options, data.id, data.active);
        }
    });
});

function categories() {
    var options = '';
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractCategories',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
        },
        success: function(response) {
            options += '<select class="inputs large" id="category">';
            for (var i = 0; i < response.categories.length; i++) {
                options += '<option value="'+response.categories[i].id+'">'+response.categories[i].name_category+'</option>';
            }
            options += '</select>';
            selectCategories = options;
        },
        error: function() {
            console.log('error');
        }
    });
}

function updateData(tabla, valor, idProveedor, valorOriginal) {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updateData',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: idProveedor,
            value: valor,
            type: tabla
        },
        success: function(response) {
            if(response.status=='true') {
                swal("Datos guardados correctamente", {
                icon: "success",
                });
            } else if(response.status=='noComplete') {
                console.log(".select"+idProveedor);
                console.log(valor);
                $(".select"+idProveedor).val(valorOriginal);
                swal("Para activar al proveedor debe cargar mínimo 8 fotografías", {
                icon: "warning",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        }
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}