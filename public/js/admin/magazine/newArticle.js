var URLactual = getAbsolutePath();
var indicator = 0;
var froala = new FroalaEditor('textarea#froala-editor', {
    imageMove: true,
    imageUploadParam: 'file',
    imageUploadMethod: 'post',
    imageUploadURL: URLactual+'uploadImage',
    imageUploadParams: {
        id: 'my_editor',
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: $("meta[name='csrf-token']").attr("content") // This passes the laravel token with the ajax request.
    },
    language: 'es', 
    events:{
        'file.uploaded': (function (response) {
            // Do something here.
            // this is the editor instance.
            alert(this);
        })
    }
});
$( document ).ready(function(){
    $('#principal').change(function(e) {
        $('#imgPrincipal').removeClass('hide');
        addImage(e); 
    });

    $(".upload").each(function (i, elem) {
        var dropzone = new Dropzone(elem, {
        url: URLactual+'saveArticle',
        method: 'POST',
        paramName: 'files', // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        maxFiles: 1,
        thumbnailWidth: 300,
        thumbnailHeight: 300,
        uploadMultiple: false,
        createImageThumbnails: true,
        acceptedFiles: 'image/*',
        autoProcessQueue: false,
        dataType: 'json',
        accept: function(file, done) {
            $(elem).find("text").hide();
            done();
            if(file){
                $('.dz-success-mark').hide();
                $('.dz-error-mark').hide();
            }
            indicator = 1;
        },
        success:function (file) {
        }, 
        error: function(data, xhr) {
            // swal('Error', 'Sucedio un error, recargue la página', 'warning');
        },
        init: function() {
            this.on("sending", function(file, xhr, formData) {
                formData.append("category", $('#category').val());
                formData.append("title", $('#title').val());
                formData.append("contenido", $('.contenido').val());
                var p = ($("#publish").is(":checked"))? 1 : 0;
                formData.append("publish", p);
                formData.append("_token",  $("meta[name='csrf-token']").attr("content"));
            });
            this.on('success', function(file, response) {
                swal('Correcto', 'Los datos fueron guardados', 'success');
                setTimeout(function() {
                    location.href =URLactual+"blog";
                }, 300);
            });
        },
        });
            var file = $(".archivo").val();
            if (file != null && file != "") {
                var mockFile = {name: "Filename", size: 12345};
                dropzone.emit("addedfile", mockFile);
                dropzone.emit("thumbnail", mockFile, file);
                dropzone.emit("complete", mockFile);
                $('.dz-success-mark').hide();
                $('.dz-error-mark').hide();
                $('.dz-details').hide();
                $(elem).find("text").hide();
            }
    });

    $(".eliminar-foto").on("click", function (elm) {
        console.log("entro eliminar");
        var $prom = $("#upload");
        $prom.find("div.dz-preview").remove();
        $prom.find("text").show();
        $prom.find(".archivo").val("");
    });

    $( ".saveArticle" ).on( "click", function() {
        if($('#category').val()!='' && $('#title').val()!='' && $('.contenido').val()!=''  ) {
                if(indicator == 1 ){
                    var myDropzone = Dropzone.forElement(".upload");
                    myDropzone.processQueue();
                }
                // else{
                //     save();
                // }
        } else {
            console.log(indicator);
            swal('Error', 'Por favor complete todos los campos', 'warning');
        }
    });

});
function addImage(e){
    var file = e.target.files[0],
    imageType = /image.*/;
    if (!file.type.match(imageType))
    return;

    var reader = new FileReader();
    reader.onload = fileOnload;
    reader.readAsDataURL(file);
}
function fileOnload(e) {
    var result=e.target.result;
    $('#imgPrincipal').attr("src",result);
}
function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}