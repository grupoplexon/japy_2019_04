var URLactual = $('#url').val();
$( document ).ready(function(){
    $('#magazineTable').DataTable({
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        } 
    });
    
});
function deleteArticle(id){
    swal({
        title: '¿Estás seguro que deseas eliminar este articulo?',
        icon: 'error',
        buttons: ['Cancelar', true],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
            url:  URLactual+'deleteArticle',
            method: "POST",
            data: {
                idArticle: id,
                _token: $("meta[name='csrf-token']").attr("content")
            },
            success: function(response) {
                console.log("SUCCES");
                swal('Deleted', 'El articulo fue eliminado', 'success');
                location.reload();
            },
            error: function(e) {
                console.log("ERROR");
            },
        });
        }
    });
};