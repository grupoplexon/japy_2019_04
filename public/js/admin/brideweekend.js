$('.dropdown-trigger').dropdown();
var URLactual = $('#URL').val();
var table = $('#brideweekendTable').DataTable( {
    "serverSide": true,
    "ajax":{  
        url: URLactual+'extractBW',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content")
        }, 
    },
    "columns": [
        {data: 'id'},
        {data: 'city'},
        {
            "targets": 2,  
            "render": function (data, type, row, meta){
                fecha = new Date(row.initial_date.replace(/-/g, '\/'));
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                var date = fecha.toLocaleDateString("es-ES", options);
                return '<span>'+date+'</span>';
            }
        },
        {
            "targets": 3,  
            "render": function (data, type, row, meta){
                fecha = new Date(row.final_date.replace(/-/g, '\/'));
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                var date = fecha.toLocaleDateString("es-ES", options);
                return '<span>'+date+'</span>';
            }
        },
        {
            "targets": 4,  
            "render": function (data, type, row, meta){
                $select = $('<select class="inputs large select'+row.id+'" id="active"><option value="0">Desactivada</option><option value="1">Activada</option></select>');
                $select.find('option[value="'+row.active+'"]').attr('selected', 'selected');
                return $select[0].outerHTML;
            }
        },
        {
            "targets": 5,  
            "render": function (data, type, row, meta){
                return '<a class="btn blue" href="'+URLactual+'editarBW/'+row.id+'">Editar</a>';
            }
        },
    ],
    language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    }
});
$('#brideweekendTable tbody').on('change', 'select', function (e) {
    var data = table.row( $(this).parents('tr') ).data();
    var options = $(this).find('option:selected').val();
    let tableIndice = this.id;
    console.log(data.id);
    if(data) {
        $.ajax({
            dataType: 'json',
            url: URLactual+'activeOrInactive',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                id: data.id,
                value: options,
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal("Guardado con éxito", {
                    icon: "success",
                    });
                } else {
                    swal("Lo sentimos, ocurrio un error", {
                    icon: "success",
                    });
                }
            },
            error: function() {
                console.log('error');
            }
        });
    }
});