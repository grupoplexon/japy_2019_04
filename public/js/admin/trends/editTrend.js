var URLactual = $('#url').val();
var indicator = 0;
$(document).ready(function() {

    categories();

        // $(".upload").each(function (i, elem) {
        //     var dropzone = new Dropzone(elem, {
        //     url: URLactual+'saveTrend',
        //     method: 'POST',
        //     paramName: 'files', // The name that will be used to transfer the file
        //     maxFilesize: 6, // MB
        //     maxFiles: 1,
        //     thumbnailWidth: 300,
        //     thumbnailHeight: 300,
        //     uploadMultiple: false,
        //     createImageThumbnails: true,
        //     acceptedFiles: 'image/*',
        //     autoProcessQueue: false,
        //     dataType: 'json',
        //     accept: function(file, done) {
        //         $(elem).find("text").hide();
        //         done();
        //         if(file){
        //             $('.dz-success-mark').hide();
        //             $('.dz-error-mark').hide();
        //         }
        //         indicator = 1;
        //     },
        //     success:function (file) {
        //     }, 
        //     error: function(data, xhr) {
        //     },
        //     init: function() {
        //         this.on("sending", function(file, xhr, formData) {
        //             formData.append("category", $('#category').val());
        //             formData.append("name", $('#name').val());
        //             formData.append("description", $('#description').val());
        //             formData.append("designer", $('#designer').val());
        //             formData.append("season", $('#season').val());
        //             formData.append("select0", $('#select0').val());
        //             formData.append("select1", $('#select1').val());
        //             formData.append("_token",  $("meta[name='csrf-token']").attr("content"));
        //         });
        //         this.on('success', function(file, response) {
        //             swal('Correcto', 'Los datos fueron guardados', 'success');
        //             setTimeout(function() {
        //                 location.href =URLactual+"trends";
        //             }, 300);
        //         });
        //     },
        //     });
        //         var file = $(".archivo").val();
        //         if (file != null && file != "") {
        //             var mockFile = {name: "Filename", size: 12345};
        //             dropzone.emit("addedfile", mockFile);
        //             dropzone.emit("thumbnail", mockFile, file);
        //             dropzone.emit("complete", mockFile);
        //             $('.dz-success-mark').hide();
        //             $('.dz-error-mark').hide();
        //             $('.dz-details').hide();
        //             $(elem).find("text").hide();
        //         }
        // });
        // $( ".save" ).on( "click", function() {
        //     if($('#category').val()!='' && $('#name').val()!='' && $('#description').val()!='' &&
        //         $('#designer').val()!='' && $('#season').val()!='' ) {
        //             if(indicator == 1 ){
        //                 var myDropzone = Dropzone.forElement(".upload");
        //                 myDropzone.processQueue();
        //             }
        //     } else {
        //         console.log(indicator);
        //         swal('Error', 'Por favor complete todos los campos', 'warning');
        //     }
        // });
});
function categories() {
    var category = $('#category').val();
    $.ajax({
        dataType: 'json',
        url: URLactual+'trends/categories',
        method: 'post',
        data: {
            category: category,
            "_token": $("meta[name='csrf-token']").attr("content"),
        },
        success: function(response) {
            if(response != null){
                $('.categories').empty();
                $(".trOpc").addClass("hide");
                for (var i = 0; i < response.categories.length; i++) {
                    $("#opcion"+i).text(response.categories[i]);
                    $(".opc"+i).removeClass("hide");
                    $('#select'+i).append("<option value=''disabled selected> Elige una opción </option>");
                    for (var j = 0; j < response.options.length; j++) {
                        if(response.categories[i] == response.options[j].group){
                            console.log(response.options[j].name);
                            $('#select'+i).append("<option value="+response.options[j].id+"> "+response.options[j].name+" </option>");
                        }
                    }
                }
            }
        },
        error: function() {
            console.log('error');
        },
    });
};