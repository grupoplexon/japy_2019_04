var URLactual = $('#url').val();
$( document ).ready(function(){
    $('#magazineTable').DataTable({
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        } 
    });
    
});
function deleteTrend(id){
    swal({
        title: '¿Estás seguro que deseas eliminar este registro?',
        icon: 'error',
        buttons: ['Cancelar', true],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
            url:  URLactual+'deleteTrend',
            method: "POST",
            data: {
                idTrend: id,
                _token: $("meta[name='csrf-token']").attr("content")
            },
            success: function(response) {
                console.log("SUCCES");
                swal('Eliminado', 'Se ha eliminado correctamente', 'success');
                location.reload();
            },
            error: function(e) {
                console.log("ERROR");
            },
        });
        }
    });
};