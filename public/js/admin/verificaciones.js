var URLactual = getAbsolutePath();
var $select;
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    chargeVerifications();
});

function chargeVerifications() {
    $('#verificationsTable').dataTable().fnDestroy();
    var table = $('#verificationsTable').DataTable({
        "serverSide": true,
        "ajax":{  
            url: URLactual+'extractVerifications',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content")
            }, 
        },
        "columns": [
            {data: 'id_verification'},
            {data: 'name'},
            {
                "targets": 1,  
                "render": function (data, type, row, meta){
                    if(row.type=='prices') {
                        return '<p>Actualizó sus precios</p>';
                    } else if(row.type=='name') {
                        return '<p>Cambio el nombre de su empresa</p>';
                    } else if(row.type=='description') {
                        return '<p>Cambio la descripción de su empresa</p>';
                    } else if(row.type=='images') {
                        return '<p>Completó sus imagenes</p>';
                    }
                }
            },
            {  
                "targets": 2,  
                "render": function (data, type, row, meta){
                    if(row.type=='prices') {
                        if(row.price_min!=null && row.price_max!=null) {
                            return '<p>'+row.price_min+' - '+row.price_max+'</p>';
                        } else {
                            return '<p>0 - 0</p>';
                        }
                    } else if(row.type=='name') {
                        return '<p>'+row.name+'</p>';
                    } else if(row.type=='description') {
                        return '<p>'+row.description+'</p>';
                    } else if(row.type=='images') {
                        return '<p></p>';
                    }
                }
            },
            {  
                "targets": 3,  
                "render": function (data, type, row, meta){
                    if(row.type=='images') {
                        return '<p>El provedor ha cargado '+row.data+' imagenes</p>';
                    } else {
                        return '<p>'+row.data+'</p>';
                    }
                }
            },
            {  
                "targets": 4,  
                "render": function (data, type, row, meta){
                    if(row.type=='images') {
                        return '<p class="btn green" id="aceptado">Activar</p>&nbsp;&nbsp;&nbsp;<p class="btn blue" id="rechazado">Leído</p>';
                    } else {
                        return '<p class="btn green" id="aceptado">Aceptar</p>&nbsp;&nbsp;&nbsp;<p class="btn red" id="rechazado">Rechazar</p>';
                    }
                }
            },
        ],
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    });
    $('#verificationsTable tbody').on('click', 'p', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        // var options = $(this).find('option:selected').val();
        let indicator = this.id;
        if(data) {
            updateData(indicator, data.type, data.id_verification, data.data, data.user_id);
        }
    });
}

function updateData(status, tabla, id, datos, id_user) {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updateVerifications',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            idVerification: id,
            idUser: id_user,
            status: status,
            indicator: tabla,
            data: datos,
        },
        success: function(response) {
            if(response.status=='saved') {
                chargeVerifications();
                admNotifications();
                swal("Guardado con éxito", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos, ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        }
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}