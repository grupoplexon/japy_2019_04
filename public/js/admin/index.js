$( document ).ready(function(){
    $('.dropdown-trigger').dropdown();
});

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
var data = google.visualization.arrayToDataTable([
    ['Día', 'Guadalajara', 'Puebla', 'Querétaro'],

    ['2014', 1, 5, 2],
    ['2015', 1, 4, 2],
    ['2016', 6, 1, 3],
    ['2017', 1, 5, 3],
    ['2018', 1, 4, 5]
]);

var options = {
    chart: {
    title: '',
    subtitle: '',
    },
    bars: 'vertical' // Required for Material Bar Charts.
};

var chart = new google.charts.Bar(document.getElementById('barchart_material'));

chart.draw(data, google.charts.Bar.convertOptions(options));
}