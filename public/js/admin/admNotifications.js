$(document).ready(function() {
    window.setInterval(function() {
        admNotifications();
    }, 60000);
});
function admNotifications() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'admNotifications',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(response) {
            if(response.data==0) {
                $('#admNotifications').addClass('oculto');
            } else {
                $('#admNotifications').removeClass('oculto');
                $('#admNotifications').text(response.data);
            }
        },
        error: function() {
            console.log('error');
        }
    });
}