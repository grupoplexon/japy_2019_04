$(document).ready(function() {
    var URLactual = getAbsolutePath();

    ////////////Obtiene la localización de donde se accede al sitio/////////////////
    $.ajax({
        url: 'https://geoip-db.com/jsonp/',
        jsonpCallback: 'callback',
        dataType: 'jsonp',
        timeout: 8000,
        success: function(location) {
            if(location){
                if(location) {
                    $('#pais').val(location.country_name);
                    $('#estado').val(location.state);
                    $('#ciudad').val(location.city);
                    $('#latitude').val(location.latitude);
                    $('#longitude').val(location.longitude);
                }
            }
        },
    });
    $('select').formSelect();

    ///////////////////Carga el select de sector de actividad/////////////////
    $('#usuario').on('change', function() {
        $.ajax({
            dataType: 'json',
            url: URLactual+'extractsector',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content")
            },
            success: function(response) {
                var crear="";
                var options = '<option>';
                options += 'Selecciona un sector de actividad';
                options += '</option>';
                crear = crear + options;
                for(i=0; i<response.data.length; i++) {
                    options = '<option value="'+response.data[i].id+'">';
                    options += response.data[i].category;
                    options += '</option>';
                    crear = crear + options;
                }
                document.getElementById("sector").innerHTML = crear;
                $('#sector').formSelect();
            },
            error: function() {
                console.log('error');
            },
        });
        var tipo_usuario = $('#usuario').val();
        if(tipo_usuario=='Proveedor') {
            document.getElementById("labelname").innerHTML = 'Nombre de la empresa:';
            $('.indicator').removeClass('oculta');
            $('.indicatorone').addClass('oculta');
        } else {
            document.getElementById("labelname").innerHTML = 'Nombre:';
            $('.indicator').addClass('oculta');
            $('.indicatorone').removeClass('oculta');
        }
    });

    ///////////////////Carga el select de categorías/////////////////
    $('#sector').on('change', function() {
        $.ajax({
            dataType: 'json',
            url: URLactual+'extractcategory',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                sector: $('#sector').val()
            },
            success: function(response) {
                var crear="";
                var options = '<option>';
                options += 'Selecciona una categoria';
                options += '</option>';
                crear = crear + options;
                for(i=0; i<response.data.length; i++) {
                    options = '<option>';
                    options += response.data[i].service;
                    options += '</option>';
                    crear = crear + options;
                }
                document.getElementById("category").innerHTML = crear;
                $('#category').formSelect();
            },
            error: function() {
                console.log('error');
            },
        });
    });

    // $('#save_register').on('click', function() {
    //     alert($('#name').val()+' '+$('#last_name').val()
    //     +' '+$('#email').val()+' '+$('#user').val()
    //     +' '+$('#phone').val()+' '+$('#cellphone').val()
    //     +' '+$('#adress').val()+' '+$('#pais').val()
    //     +' '+$('#estado').val()+' '+$('#ciudad').val()
    //     +' '+$('#postal_code').val()+' '+$('#password').val()
    //     +' '+$('#confirm_password').val()+' '+$('#usuario').val()
    //     +' '+$('#name_company').val()+' '+$('#description').val()
    //     +' '+$('#web').val()+' '+$('#sector').val()
    //     +' '+$('#category').val());
    // });
});

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}