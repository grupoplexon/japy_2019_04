var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    $('.tabs').tabs();
    console.log(URLactual);
    address($('input[name=group1]:checked').val());
    // $.ajax({
	// 	url: "https://geoip-db.com/jsonp",
	// 	jsonpCallback: "callback",
	// 	dataType: "jsonp",
	// 	success: function( location ) {
	// 		console.log(location.state);  
	// 	}
    // });
});
$('.radio-button').on('click', function() {
    address($(this).val());
});

function address(idLocation) {
    $.ajax({
        dataType: 'json',
        url: $('#url').val()+'extractLocations',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: idLocation,
        },
        success: function(response) {
            document.getElementById('address').innerHTML = response.data.address+', '+response.data.city+'.';
            document.getElementById('cellPhone').innerHTML = response.data.cellphone;
        },
        error: function() {
            console.log('error');
        }
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}