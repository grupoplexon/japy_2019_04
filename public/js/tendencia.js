$(document).ready(function() {
  function swiper(){
    $(".responsive").slick({
      dots: true,
      infinite: false,
      arrows: true,
      prevArrow:
        '<button type="button" class="slick-prev"><i class="fa fa-chevron-left flecha" aria-hidden="true"></i></button>',
      nextArrow:
        '<button type="button" class="slick-next"><i class="fa fa-chevron-right flecha" aria-hidden="true"></i></button>',
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }

  function dataFilter(){
    $(".novia").on("change", function() {
      var designer = $("#designer").val();
      var cut = $("#cut").val();
      var neckline = $("#neckline").val();
      var season = $("#season").val();
      $.ajax({
        type: "GET",
        url: "/vestidos/categoria/search",
        data: {
          categoria: "designer",
          designer: designer,
          cut: cut,
          neckline: neckline,
          season: season
        },
        success: function(result) {
          $("#data").html("");
          $(".trendsByCategory").html("");
         
          $.each(result, function(id, data) {
            //console.log(id);
            $(".trendsByCategory").append(
              "\
                            <h4>" +
                id +
                '</h4>\
                            <div class="responsive">\
                             </div>\
                            '
            );
            $.each(data, function(id, data) {
              console.log(data);
              var image = data.image;
              $(".responsive").append(
                '\
                                <div class="comandSlider__item"><img src="http://brideadvisor.test/resources/uploads/trends/' +
                  image +
                  '" style="width:100%; height:100%;" alt=""></div>\
                                '
              );
            });
          });
          swiper();
        }
      });
    });
  }
  $("#categoria").on("change", function() {
    var categoria = this.value;
    if (categoria) {
      $.ajax({
        type: "GET",
        url: "/vestidos/categoria",
        data: { categoria: categoria },
        success: function(result) {
          $("#trend").html("");
          $("#data").html("");
          $(".vestidos-novia").html("");
          $(".trendsByCategory").html("");

          var dataCategoria = result[1];
          var trendsByDesigner = result[2];
          console.log(result[0]);
          switch (result[0]) {
            case "1":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default novia">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="cut" class="select browser-default novia">\
                            <option value=""  selected>Corte</option>\
                            <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="neckline" class="select browser-default novia">\
                                <option value=""  selected>Escote</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default novia">\
                                <option value=""  selected>Temporada</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#cut").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#neckline").append(
                  $("<option>", {
                    value: dataCategoria[id].neckline,
                    text: dataCategoria[id].neckline
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              $.each(trendsByDesigner, function(id, data) {
                //console.log(id);
                $(".trendsByCategory").append(
                  "\
                                <h4>" +
                    id +
                    '</h4>\
                                <div class="responsive">\
                                 </div>\
                                '
                );
                $.each(data, function(id, data) {
                  console.log(data);
                  var image = data.image;
                  $(".responsive").append(
                    '\
                                    <div class="comandSlider__item"><img src="http://brideadvisor.test/resources/uploads/trends/' +
                      image +
                      '" style="width:100%; height:100%;" alt=""></div>\
                                    '
                  );
                });
              });
              dataFilter();
              break;
// ##################################################################################################################################################
            case "2":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default novio">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="style" class="select browser-default novio">\
                                <option value=""  selected>Estilo</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default novio">\
                                <option value=""  selected>Temporada</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#style").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
            //##########################################################################################################################################################
            case "3":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default fiesta">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="largo" class="select browser-default fiesta">\
                                <option value=""  selected>Largo</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default fiesta">\
                                <option value=""  selected>Temporado</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#largo").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
            //#####################################################################################################################################
            case "4":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default  joyeria">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="type" class="select browser-default">\
                                <option value=""  selected>Tipo</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default">\
                                <option value=""  selected>Temporado</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#type").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
// ###############################################################################################################################################################################################
            case "5":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default zapatos">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las iseñadors</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                        <select id="category" class="select browser-default zapatos">\
                            <option value=""  selected>Categoria</option>\
                            <option value="" > Todos las Categorias</option>\
                        </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default zapatos">\
                                <option value=""  selected>Temporada</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#category").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
              
// ###############################################################################################################################################################################################
            case "6":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="category" class="select browser-default lenceria">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="type" class="select browser-default lenceria">\
                                <option value=""  selected>Tipo</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default lenceria">\
                                <option value=""  selected>Temporada</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#type").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
// ###############################################################################################################################################################################################
            case "7":
              $("#trend").append(
                '\
                        <div class="col l2 s12">\
                            <select id="designer" class="select browser-default complementos">\
                                <option value=""  selected>Diseñador</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="type" class="select browser-default complementos">\
                                <option value=""  selected>Tipo</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="season" class="select browser-default complementos">\
                                <option value=""  selected>Categoria</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>\
                        <div class="col l2 s12">\
                            <select id="category" class="select browser-default complementos">\
                                <option value=""  selected>Temporada</option>\
                                <option value="" > Todos las Categorias</option>\
                            </select>\
                        </div>'
              );
              $.each(dataCategoria, function(id, data) {
                $("#designer").append(
                  $("<option>", {
                    value: dataCategoria[id].designer,
                    text: dataCategoria[id].designer
                  })
                );
              });
              $.each(dataCategoria, function(id, data) {
                $("#type").append(
                  $("<option>", {
                    value: dataCategoria[id].cut,
                    text: dataCategoria[id].cut
                  })
                );
                $("#category").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
                $("#season").append(
                  $("<option>", {
                    value: dataCategoria[id].season,
                    text: dataCategoria[id].season
                  })
                );
              });
              break;
          }

          swiper();
        }
      });
    }
  });
});
