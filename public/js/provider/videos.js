var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.videos').addClass('selected');
    $('.dropdown-trigger').dropdown();
    var form = document.getElementById('form_subir');
	form.addEventListener("submit", function(event) {
        event.preventDefault();
        var formulario = this;
        $.ajax({
            dataType: 'json',
            url: URLactual+'checkVideo',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
            },
            success: function(response) {
                if(response.status=='ok') {
                    subir_archivos(formulario);
                } else {
                    swal("Solo puede cargar 1 video", {
                    icon: "warning",
                    });
                }
            },
            error: function() {
                console.log('error');
            },
        });
	});
});

function subir_archivos(form) {
    var input = document.getElementById('archivo');
    var file = input.files[0];

    if(file.size<524288000) {
        var archivo = $('#archivo').val();
        var extensiones = archivo.substring(archivo.lastIndexOf("."));
        if(extensiones=='.mp4') {
            $('.barra').removeClass('oculto');
            var barra_estado = form.children[1].children[0],
            span = barra_estado.children[0],
            boton_cancelar = form.children[2].children[1];

            document.getElementById('barra').classList.remove('barra_verde', 'barra_roja');

            var peticion = new XMLHttpRequest();

            peticion.upload.addEventListener("progress", (event) => {
                var porcentaje = Math.round((event.loaded / event.total) * 100);
                // console.log(porcentaje);
                document.getElementById('barra').classList.add('barra_verde');
                document.getElementById('barra').style.width = porcentaje+'%';
                document.getElementById('progreso').innerHTML = porcentaje+'%';
                if(porcentaje==100) {
                    swal("Su archivo se cargó con éxito", {
                    icon: "success",
                    });
                }
            });

            peticion.addEventListener("load", () => {
                document.getElementById('barra').classList.add('barra_verde');
                document.getElementById('barra').innerHTML = 'Proceso completado';
            });

            peticion.open('POST', 'uploadVideo');
            peticion.send(new FormData(form));

            peticion.onreadystatechange = function(response) {
                if(peticion.status==200) {
                    location.reload();
                }
                // var res = JSON.parse(peticion.response);
                // console.log(res.id);
                // console.log(peticion.response);
            };
        } else {
            swal("El archivo debe estar en formato MP4", {
            icon: "warning",
            });
        }
    } else {
        swal("El archivo debe pesar menos de 500 Mb", {
        icon: "warning",
        });
    }

	// $('#cancelar').on('click', function() {
	// 	peticion.abort();
	// 	document.getElementById('barra').classList.remove('barra_verde');
	// 	document.getElementById('barra').classList.add('barra_roja');
	// 	document.getElementById('barra').innerHTML = 'Proceso cancelado';
	// });
}

$('.delete').on('click', function() {
    var id = $(this).attr('id');
    deleteVideo(id);
});

function deleteVideo(id) {
    swal({
        title: "¡Atención!",
        text: "¿Seguro que desea eliminar este video?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                dataType: 'json',
                url: URLactual+'deleteVideo',
                method: 'post',
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content"),
                    id: id
                },
                success: function(response) {
                    if(response.status=='deleted') {
                        var cad = 'card-'+id;
                        $('#'+cad).remove();
                    } else {
                        swal("Lo sentimos ocurrio un error", {
                        icon: "error",
                        });
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        }
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}