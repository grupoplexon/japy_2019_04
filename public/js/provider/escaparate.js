var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.escaparate').addClass('selected');
    $('.dropdown-trigger').dropdown();
    init_tinymce_mini('textarea.editable');

    const $registerForm = $('#formCredentials').parsley();
    const $registerContact = $('#formContact').parsley();

    $('#btnContact').on('click', function(e) {
        e.preventDefault();
        $registerContact.validate();
        if ($registerContact.isValid()) {
            $.ajax({
                url: URLactual+'updateContact',
                method: 'post',
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content"),
                    name: $('#name_contact').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    cellphone: $('#cellphone').val(),
                    website: $('#website').val(),
                    id: $('#locations').val(),
                },
                success: function(response) {
                    if(response.status=='saved') {
                        swal("Datos guardados con éxito", {
                        icon: "success",
                        });
                    } else {
                        swal("Lo sentimos ocurrio un error", {
                        icon: "error",
                        });
                        return false;
                    }
                },
                error: function() {
                    console.log('error');
                }
            });
        }
    });

    $('#btnCredentials').on('click', function(e) {

        e.preventDefault();

        $registerForm.validate();
        
        if ($registerForm.isValid()) {
            if($('#passwordActual').val()!='' && $('#password').val()!='' && $('#password_2').val()!='') {
                $.ajax({
                    url: URLactual+'checkPassword',
                    method: 'post',
                    data: {
                        "_token": $("meta[name='csrf-token']").attr("content"),
                        password: $('#passwordActual').val(),
                        idUser: $('#idUser').val(),
                    },
                    success: function(response) {
                        if(response.status=='coincide') {
                            if($('#password').val()==$('#password_2').val()) {
                                $.ajax({
                                    url: URLactual+'updateCredentials',
                                    method: 'post',
                                    data: {
                                        "_token": $("meta[name='csrf-token']").attr("content"),
                                        user: $('#user').val(),
                                        password: $('#password').val(),
                                    },
                                    success: function(response) {
                                        if(response.status=='saved') {
                                            $('#passwordActual').val('');
                                            $('#password').val('');
                                            $('#password_2').val('');
                                            swal("Datos guardados con éxito", {
                                            icon: "success",
                                            });
                                        } else {
                                            swal("Lo sentimos ocurrio un error", {
                                            icon: "error",
                                            });
                                            return false;
                                        }
                                    },
                                    error: function() {
                                        console.log('errror');
                                        return false;
                                    }
                                });
                                // $('#formCredentials').submit();
                            } else {
                                swal('Error', 'Las contraseñas no coinciden', 'error');
                                return false;
                            }
                        } else {
                            swal('Error', 'La contraseña actual no coincide', 'error');
                            return false;
                        }

                        // $('#formCredentials').submit();
                    },
                    error: function() {
                        swal('Error', 'Lo sentimos ocurrio un error', 'error');
                        return false;
                    },
                });
            } else {
                $('#passwordActual').val('');
                $('#password').val('');
                $('#password_2').val('');
                $.ajax({
                    url: URLactual+'updateCredentials',
                    method: 'post',
                    data: {
                        "_token": $("meta[name='csrf-token']").attr("content"),
                        user: $('#user').val(),
                        password: $('#password').val(),
                    },
                    success: function(response) {
                        if(response.status=='saved') {
                            swal("Datos guardados con éxito", {
                            icon: "success",
                            });
                        } else {
                            swal("Lo sentimos ocurrio un error", {
                            icon: "error",
                            });
                            return false;
                        }
                    },
                    error: function() {
                        console.log('errror');
                        return false;
                    }
                });
            }
        }
    });
});

$('#btnPrices').on('click', function() {
    if($('#priceMin').val()!='' && $('#priceMax').val()!='') {
        $.ajax({
            dataType: 'json',
            url: URLactual+'updatePrices',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                priceMin: $('#priceMin').val(),
                priceMax: $('#priceMax').val()
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal("Sus precios han sido enviados. El administrador de BrideAdvisor revisará su información, una vez revisada y validada se reflejaran sus cambios", {
                    icon: "warning",
                    });
                } else {
                    swal("Lo sentimos, ocurrio un error", {
                    icon: "error",
                    });
                }
            },
            error: function() {
                console.log('error');
            }
        });
    } else {
        swal("Complete ambos precios", {
        icon: "error",
        });
    }
});

$('#locations').on('change', function() {
    $.ajax({
        url: URLactual+'extractLocations',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: $('#locations').val(),
        },
        success: function(response) {
            $('#name_contact').val(response.data.name_contact);
            $('#phone').val(response.data.phone);
            $('#cellphone').val(response.data.cellphone);
            $('#website').val(response.data.website);
        },
        error: function() {
            console.log('errror');
        }
    });
});

$('#btnCompany').on('click', function() {
    if($('#name_company').val()!='' && tinymce.get('description').getContent()!='') {
        $.ajax({
            url: URLactual+'updateCompany',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                name_company: $('#name_company').val(),
                description: tinymce.get('description').getContent(),
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal("Su información ha sido enviada el administrador de BrideAdvisor la revisará, una vez revisada y validada se reflejaran sus cambios", {
                    icon: "warning",
                    });
                } else {
                    swal("No modificó ningun dato", {
                    icon: "warning",
                    });
                    return false;
                }
            },
            error: function() {
                console.log('error');
            }
        });
    } else {
        swal("El nombre de la empresa y la descripción son obligatorias", {
        icon: "warning",
        });
    }
});

$('#btnCategory').on('click', function() {
    $.ajax({
        url: URLactual+'updateCategory',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            category: $('#category').val(),
        },
        success: function(response) {
            if(response.status=='saved') {
                swal("Datos guardados con éxito", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
                return false;
            }
        },
        error: function() {
            console.log('error');
        }
    });
});

function init_tinymce_mini(elem) {
    if (typeof tinymce != 'undefined') {
        tinymce.init({
            selector: elem,
            relative_urls: false,
            menubar: '',
            plugins: [
                'advlist autolink lists link  preview anchor',
                'searchreplace code ',
                'insertdatetime paste ',
            ],
            statusbar: false,
            height: 350,
            imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
            browser_spellcheck: true,
            toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
        });
    }
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}