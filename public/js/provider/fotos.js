var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.fotos').addClass('selected');
    $('.dropdown-trigger').dropdown();
    // $('.tabs').tabs();
    var myDropzone = '';
    $('#upload').dropzone({
        url: URLactual+'uploadImages',
        method: 'POST',
        paramName: 'files', // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        uploadMultiple: false,
        createImageThumbnails: false,
        acceptedFiles: 'image/*',
        // autoProcessQueue: false,
        dataType: 'json',
        accept: function(file, done) {
            done();
        },
        success:function (file) {
            
        },
        error: function(data, xhr) {
            
        },
        init: function() {
            // var submitButton = document.querySelector("#save");
            myDropzone = this;
            // submitButton.addEventListener("click", function() {
            //     myDropzone.processQueue();
            // });
            this.on("sending", function(file, xhr, formData) {
                formData.append("_token", $("meta[name='csrf-token']").attr("content"));
            });
            this.on('success', function(file, response) {
                var options = '';
                options = '<div class="col l3 m6 s8 offset-s2" id="card-'+response.data.id+'">';
                                options += '<div class="card height">';
                                    options += '<div class="card-image waves-effect waves-block waves-light">';
                                        options += '<img class="activator height cover" src="resources/uploads/providers/'+response.status+'">';
                                    options += '</div>';
                                    options += '<div class="card-reveal">';
                                        options += '<span class="card-title grey-text text-darken-4">Editar<i class="material-icons right">close</i></span>';
                                        options += '<p>';
                                            options += '<label>';
                                                options += '<input name="group1" type="radio" class="filled-in logo" onclick="logo('+response.data.id+')"/>';
                                                options += '<span>Logotipo</span>';
                                            options += '</label>';
                                        options += '</p>';
                                        options += '<p>';
                                            options += '<label>';
                                                options += '<input name="group2" type="radio" class="filled-in" onclick="perfil('+response.data.id+')"/>';
                                                options += '<span>Foto principal</span>';
                                            options += '</label>';
                                        options += '</p>';
                                        // options += '<span class="btn green white-text width space-top">Inspiracion</span>';
                                        options += '<span class="btn red white-text width space-top delete" onclick="deleteImage('+response.data.id+')">Eliminar</span>';
                                    options += '</div>';
                                options += '</div>';
                            options += '</div>';
                document.getElementById('gallery').innerHTML += options;
                // swal('Correcto', 'Los datos fueron guardados', 'success');
                myDropzone.removeFile(file);
                // setTimeout(function() {
                //     location.href ="<?php echo base_url() ?>admin/trends";
                // }, 300);
            });
        },
    });
});

$('.delete').on('click', function() {
    var id = $(this).attr('id');
    deleteImage(id);
});

function perfil(id) {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updatePhoto',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id
        },
        success: function(response) {
            if(response.status=='saved') {
                swal("Establecida como foto principal", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
}
function logo(id) {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updateLogo',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id
        },
        success: function(response) {
            if(response.status=='saved') {
                // $('.logo').prop('checked', false);
                $(".logo").attr("checked", this.checked);
                swal("Establecida como logotipo", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
}

function deleteImage(id) {
    swal({
        title: "¡Atención!",
        text: "¿Seguro que desea eliminar esta imágen?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                dataType: 'json',
                url: URLactual+'deleteImage',
                method: 'post',
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content"),
                    id: id
                },
                success: function(response) {
                    var cad = 'card-'+id;
                    $('#'+cad).remove();
                },
                error: function() {
                    console.log('error');
                },
            });
        }
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}