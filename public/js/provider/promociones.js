var URLactual = getAbsolutePath();
var id = 0, upload = 0;
var dropzone;
$(document).ready(function() {
    $('.promo').addClass('selected');
    $('.dropdown-trigger').dropdown();
    $('.collapsible').collapsible();
    init_tinymce_mini('textarea.editable');
    const $saveForm = $('#formPromo').parsley();

    $('#btnPromo').on('click', function(e) {
        $('#indicator').val('save');
        e.preventDefault();
        $saveForm.validate();
        if ($saveForm.isValid()) {
            var myDropzone = Dropzone.forElement(".upload");
            myDropzone.processQueue();
            return false;
        }
    });

    $('.updatePromo').on('click', function(e) {
        $('#indicator').val('update');
        id = $(this).attr('id');
        const $updateForm = $('#formPromo'+id).parsley();
        e.preventDefault();
        $updateForm.validate();
        if ($updateForm.isValid()) {
            if(upload!=0) {
                var myDropzone = Dropzone.forElement("#dropzoneUpload"+id);
                myDropzone.processQueue();
            } else {
                updateData();
            }
            return false;
        }
    });

    $("#deleteImage").on("click", function (elm) {
        var $btn = $(elm.currentTarget);
        var $prom = $("#" + $btn.data("promocion"));
        $prom.find(".upload div.dz-preview").remove();
        $prom.find("text").show();
    });

    $(".deleteImg").on("click", function (elm) {
        var $btn = $(elm.currentTarget);
        var $prom = $("#form" + $btn.data("promocion"));
        $prom.find(".upload div.dz-preview").remove();
        $prom.find("text").show();
    });

    dropzone();
});

function dropzone() {
    var myDropzone = '';
    $(".upload").each(function (i, elem) {
        dropzone = new Dropzone(elem, {
            url: URLactual+'savePromotion',
            method: 'POST',
            paramName: 'files', // The name that will be used to transfer the file
            maxFilesize: 6, // MB
            uploadMultiple: false,
            createImageThumbnails: true,
            acceptedFiles: 'image/*',
            autoProcessQueue: false,
            dataType: 'json',
            accept: function(file, done) {
                done();
                if(file) {
                    upload = 1;
                    $(elem).find("text").hide();
                    $('.dz-success-mark').hide();
                    $('.dz-error-mark').hide();
                    var t = $(elem).find("div.dz-preview").length;
                    if (t > 1) {
                        $(elem).find("div.dz-preview").each(function (i, value) {
                            if (t - 1 != i) {
                                value.remove();
                            }
                        });
                    }
                }
            },
            success:function (file) {
                
            },
            error: function(data, xhr) {
                
            },
            init: function() {
                // var submitButton = document.querySelector("#save");
                myDropzone = this;
                // submitButton.addEventListener("click", function() {
                //     myDropzone.processQueue();
                // });
                this.on("sending", function(file, xhr, formData) {
                    if($('#indicator').val()=='save') {
                        formData.append("_token", $("meta[name='csrf-token']").attr("content"));
                        formData.append('type', $('#type').val());
                        formData.append('name', $('#name').val());
                        formData.append('price', $('#price').val());
                        formData.append('price2', $('#price2').val());
                        formData.append('date', $('#date').val());
                        formData.append('description', tinymce.get('description').getContent());
                        formData.append('operation', $('#indicator').val());
                    } else {
                        formData.append("_token", $("meta[name='csrf-token']").attr("content"));
                        formData.append("id", id);
                        formData.append('type', $('#typeUpdate'+id).val());
                        formData.append('name', $('#nameUpdate'+id).val());
                        formData.append('price', $('#priceUpdate'+id).val());
                        formData.append('price2', $('#priceTwoUpdate'+id).val());
                        formData.append('date', $('#finalDateUpdate'+id).val());
                        formData.append('description', tinymce.get('description'+id).getContent());
                        formData.append('operation', $('#indicator').val());
                    }
                });
                this.on('success', function(file, response) {
                    myDropzone.removeFile(file);
                    swal("Su promoción se guardo con éxito", {
                    icon: "success",
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                });
            },
        });

        var prom = $(elem).data("promocion");
        if (prom) {// Create the mock file:
            var file = $("#form" + prom).find(".archivo").attr("value");
            if (file != null && file != "") {
                var mockFile = {size: 12345};
                dropzone.emit("addedfile", mockFile);
                dropzone.emit("thumbnail", mockFile, file);
                dropzone.emit("complete", mockFile);
                $('.dz-success-mark').hide();
                $('.dz-error-mark').hide();
                $(elem).find("text").hide();
                var img = $('.dz-image').find("img");
                img.css('width', '100%');
            }
        }
    });
}

var band = 0;
$('#createPromo').on('click', () => {
    if(band==0) {
        $('.promotion').removeClass('oculto');
        band = 1;
    } else {
        $('.promotion').addClass('oculto');
        band = 0;
    }
});

$('#saveDiscount').on('click', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'saveDiscount',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            discount: $('input:radio[name=group1]:checked').val()
        },
        success: function(response) {
            if(response.status=='saved') {
                swal("Su descuento se guardo con éxito", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
});

function updateData() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updatePromotion',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id,
            type: $('#typeUpdate'+id).val(),
            name: $('#nameUpdate'+id).val(),
            price: $('#priceUpdate'+id).val(),
            price2: $('#priceTwoUpdate'+id).val(),
            date: $('#finalDateUpdate'+id).val(),
            description: tinymce.get('description'+id).getContent(), 
        },
        success: function(response) {
            if(response.status=='saved') {
                swal("Su promoción se editó con éxito", {
                icon: "success",
                });
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
}

function init_tinymce_mini(elem) {
    if (typeof tinymce != 'undefined') {
        tinymce.init({
            selector: elem,
            relative_urls: false,
            menubar: '',
            plugins: [
                'advlist autolink lists link  preview anchor',
                'searchreplace code ',
                'insertdatetime paste ',
            ],
            statusbar: false,
            height: 350,
            imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
            browser_spellcheck: true,
            toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
        });
    }
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}