var URLactual = getAbsolutePath();
$(document).ready(function() {
    $('.localizacion').addClass('selected');
    $('.dropdown-trigger').dropdown();
    latitud = $('#latitud').val();
    longitud = $('#longitud').val();
    city();
    initMap();
});

var map;
var marker;
var center;
var latitud = 0, longitud = 0;

function initMap() {
    center = getPosicion();
    var elem = document.getElementById("mapa");
    if (center == null) {
        center = {lat: 20.676580, lng: -103.34785};
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (pos) {
                center = {lat: pos.coords.latitude, lng: pos.coords.longitude};
                drawMap(elem, center);
                setPosicion(center);
            }, function () {
                drawMap(elem, center);
            });
        } else {
            drawMap(elem, center);
        }
    } else {
        drawMap(elem, center);
    }
}

function drawMap(elem, center) {
    map = new google.maps.Map(elem, {
        center: center,
        zoom: 14
    });
    marker = new google.maps.Marker({
        position: center,
        map: map,
        animation: google.maps.Animation.DROP,
        title: 'Mueve el mapa'
    });
    map.addListener('center_changed', function () {
        var p = map.getCenter();
        marker.setPosition({lat: p.lat(), lng: p.lng()});
        setPosicion({lat: p.lat(), lng: p.lng()});
    });
}

function setPosicion(center) {
    $("#latitud").val(center.lat);
    $("#longitud").val(center.lng);
    latitud = center.lat;
    longitud = center.lng;
}

function getPosicion() {
    if ($("#latitud").val() != "") {
        return {lat: parseFloat($("#latitud").val()), lng: parseFloat($("#longitud").val())};
    }
    return null;
}

$('#saveLoc').on('click', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'updateAddress',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: $('#states').val(),
            city: $('#cities').val(),
            postal_code: $('#postal_code').val(),
            address: $('#address').val(),
            lat: latitud,
            lng: longitud
        },
        success: function(response) {
            if(response.status=='saved') {
                swal("Datos de "+$('#states option:selected').html()+" guardados con éxito", {
                icon: "success",
                });
            } else {
                swal("Lo sentimos ocurrio un error", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        },
    });
});

$('#states').on('change', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractLocations',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: $('#states').val()
        },
        success: function(response) {
            $('#postal_code').val(response.data.postal_code);
            $('#address').val(response.data.address);
            $('#city').val(response.data.city);
            $('#latitud').val(response.data.latitude);
            $('#longitud').val(response.data.longitude);
            city();
            initMap();
        },
        error: function() {
            console.log('error');
        },
    });
});

function city() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'searchCities',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            state: $('#states option:selected').html()
        },
        success: function(response) {
            var options = '';
            for (var i = 0; i < response.cities.length; i++) {
                options += '<option value="'+response.cities[i].city+'">'+response.cities[i].city+'</option>';
            }
            document.getElementById('cities').innerHTML = options;
            $("#cities").val($('#city').val())
        },
        error: function() {
            console.log('error');
        },
    });
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}