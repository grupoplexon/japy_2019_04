var URLactual = getAbsolutePath();

$(document).ready(function() {
    init_tinymce_mini('textarea.editable');

    $.ajax({
        url: 'https://geoip-db.com/jsonp/',
        jsonpCallback: 'callback',
        dataType: 'jsonp',
        timeout: 8000,
        success: function(location) {
            if(location){
                if(location) {
                    $('#location').val(location.state);
                } else {
                    $('#location').val('brideadvisor');
                }
            }
        },
    });

    const $registerForm = $('#registro').parsley();

    $('#btn_aceptar').on('click', function(e) {

        e.preventDefault();

        $registerForm.validate();
        
        if ($registerForm.isValid()) {
            if($('#password').val()==$('#password_2').val()) {
                $.ajax({
                    url: URLactual+'checkUser',
                    method: 'post',
                    data: {
                        "_token": $("meta[name='csrf-token']").attr("content"),
                        user: $('#user').val(),
                    },
                    success: function(res) {
                        if (res.data == 'exist') {
                            swal('Error', 'El usuario ya existe, intente con otro.', 'warning');
                            return false;
                        }

                        $('#registro').submit();
                    },
                    error: function() {
                        swal('Error', 'Lo sentimos ocurrio un error', 'error');
                    },
                });
            } else {
                swal('Error', 'Las contraseñas no coinciden', 'warning');
                return false;
            }
        }
    });
});

$('#countries').on('change', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractStates',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            idCountry: $('#countries').val()
        },
        success: function(response) {
            var options = '';
            options += '<option value="" selected disabled>Selecciona un estado</option>';
            for (var i = 0; i < response.states.length; i++) {
                options += '<option value="'+response.states[i].id+'">'+response.states[i].state+'</option>';
            }
            document.getElementById('states').innerHTML = options;
        },
        error: function() {
            console.log('error');
        },
    });
});

$('#states').on('change', function() {
    $.ajax({
        dataType: 'json',
        url: URLactual+'extractCities',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            idState: $('#states').val()
        },
        success: function(response) {
            var options = '';
            options += '<option value="" selected disabled>Selecciona una ciudad</option>';
            for (var i = 0; i < response.cities.length; i++) {
                options += '<option value="'+response.cities[i].city+'">'+response.cities[i].city+'</option>';
            }
            document.getElementById('cities').innerHTML = options;
        },
        error: function() {
            console.log('error');
        },
    });
});

function init_tinymce_mini(elem) {
    if (typeof tinymce != 'undefined') {
        tinymce.init({
            selector: elem,
            relative_urls: false,
            menubar: '',
            plugins: [
                'advlist autolink lists link  preview anchor',
                'searchreplace code ',
                'insertdatetime paste ',
            ],
            statusbar: false,
            height: 350,
            imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
            browser_spellcheck: true,
            toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
        });
    }
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}