$(document).ready(function() {
    var formExt = $('#contactEXT');
    formExt.submit(function (e) {
        $('#send').attr('disabled', true);
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: $('#url').val()+'registerBwUSA',
            method: 'POST',
            data: {
                '_token': $("meta[name='csrf-token']").attr("content"),
                nombre: $('#name').val()+' '+$('#last_name').val(),
                correo: $('#email').val(),
                come_from: 'brideweekend',
                genero: '2',
                telefono: $('#phone').val(),
                msg: $('#message').val(),
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal('Correcto', 'Tu mensaje se envio con éxito.', 'success');
                    $('.inputs').val('');
                } else {
                    swal('Error', 'Lo sentimos ocurrio un error.', 'error');
                }
                $('#send').attr('disabled', false);
            },
            error: function() {
                $('#send').attr('disabled', false);
                console.log('error');
            }
        });
    });
});