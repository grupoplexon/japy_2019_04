$(document).ready(function() {
    var formExt = $('#contactEXT');
    formExt.submit(function (e) {
        $('#send').attr('disabled', true);
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: $('#url').val()+'registerBwUSA',
            method: 'POST',
            data: {
                '_token': $("meta[name='csrf-token']").attr("content"),
                nombre: $('#name').val(),
                correo: $('#email').val(),
                come_from: 'brideweekend',
                genero: '2',
                telefono: $('#phone').val(),
                msg: $('#message').val(),
            },
            success: function(response) {
                if(response.status=='saved') {
                    swal('Correcto', 'Tu mensaje se envio con éxito.', 'success');
                    $('.inputs-contact').val('');
                } else {
                    swal('Error', 'Lo sentimos ocurrio un error.', 'error');
                }
                $('#send').attr('disabled', false);
            },
            error: function() {
                $('#send').attr('disabled', false);
                console.log('error');
            }
        });
    });
    var formLoc = $('#contactLOC');
    formLoc.submit(function (e) {
        // jsShowWindowLoad('Creando perfil, por favor espere!!');
        // $('#send').attr('disabled', true);
        e.preventDefault();
        // $.ajax({
        //     dataType: 'json',
        //     url: $('#url').val()+'brideweekend/register',
        //     method: 'post',
        //     data: {
        //         nombre: $('#name').val(),
        //         correo: $('#email').val(),
        //         come_from: 'brideweekend',
        //         genero: '2',
        //         telefono: $('#phone').val(),
        //         fecha: $('#date').val(),
        //     },
        //     success: function(response) {
        //         if(response.validate=='success') {
        //             jsRemoveWindowLoad();
        //             swal('Correcto', 'Tu perfil se creo con éxito.', 'success');
        //             $('.inputs-contact').val('');
        //         } else if(response.validate="user_exist") {
        //             jsRemoveWindowLoad()
        //             swal('Atención', 'Este correo ya esta registrado.', 'warning');
        //         } else {
        //             jsRemoveWindowLoad()
        //             swal('Error', 'Lo sentimos ocurrio un error.', 'error');
        //         }
        //         $('#send').attr('disabled', false);
        //     },
        //     error: function() {
        //         jsRemoveWindowLoad();
        //         $('#send').attr('disabled', false);
        //         console.log('error');
        //     }
        // });
    });
});