// $('.view-more').on('click', function() {
//     location.href = $(this).attr('id');
// })
var URLactual = getAbsolutePath();
$(document).ready(function() {
    // $('#priceMin').val(30000);
    if($('#valueState').val()!='' || 
    $('#valueCategory').val()!='' || 
    $('#valueAverage').val()!='' || 
    ($('#valuePriceMin').val()!='' && $('#valuePriceMax').val()!='') || 
    ($('#searchForProvider').val()!='' && $('#searchForProvider').val()!=undefined)) {
        $('html,body').animate({
            scrollTop: $("#areaFiltros").offset().top-70
        }, 1000);
    }
});
$('#stateId').on('change', function() {
    sendForm();
    // // document.location.href = $('#URL').val()+'proveedores/search/'+estado+'/'+calificacion+'/'+$('#categories').val()+'/'+$('#priceMin').val()+'/'+$('#priceMax').val();
});
$('#filterButton').on('click', function() {
    sendForm();
    // // document.location.href = $('#URL').val()+'proveedores/search/'+estado+'/'+calificacion+'/'+$('#categories').val()+'/'+$('#priceMin').val()+'/'+$('#priceMax').val();
});

$('#search').on('click', function() {
    if($('#btn-search').val()!='') {
        document.location.href = $('#URL').val()+'proveedores/buscar/'+$("#btn-search" ).val();
    }
});

$("#btn-search" ).autocomplete({
    source: function (request, response) {
        $.post($('#URL').val()+'proveedores/autocomplete', request, response);
    },
    select: (event, ui) => {
        busqueda = ui.item.value;
        // console.log(ui);
        document.location.href = $('#URL').val()+'proveedores/buscar/'+busqueda;
    }
});

$('.view-more').on('click', function() {
    document.location.href = $('#URL').val()+$(this).attr('id')+'/'+$('#stateId').val();
});

$('.close').on('click', function() {
    if($(this).attr('id')=='removeState') {
        $('#stateId').val('');
        sendForm();
    } else if($(this).attr('id')=='removeCategory') {
        $('#categories').val('');
        sendForm();
    } else if($(this).attr('id')=='removeAverage') {
        $('input[name="rating"]:checked').val('');
        sendForm();
    } else if($(this).attr('id')=='removePrices') {
        $('#priceMin').val('');
        $('#priceMax').val('');
        sendForm();
    } else if($(this).attr('id')=='removeProvider') {
        sendForm();
    }
});

function sendForm() {
    document.getElementById('filterProvider').submit();
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}