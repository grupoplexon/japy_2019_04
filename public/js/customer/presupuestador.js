var editor;
$(document).ready(function() {
    $('.dropdown-trigger').dropdown();
    $('.tabs').tabs();
    var elem = document.querySelector('.collapsible.expandable');
    var instance = M.Collapsible.init(elem, {
        accordion: false,
    });
    $('.collapsible-header').attr('tabindex', 'unset');
    $('.modal').modal();
     events();
   
    if($('#BUDGET').val()=='') {
        $('.modalWelcome').attr('tabindex', 'unset');
        $('#modal1').modal('open');
        $('.closeModal').on('click', function() {
            $('#modal2').modal('close');
        });
        $('.openModal').on('click', function() {
            $('#modal1').modal('close');
            $('#modal2').modal('open');
        });
    }
});

    $(".note").click(function(){
        var budget_id = $(this).find('.add-note').val();
        var note = $('#contenido-nota'+budget_id).val();
        updateBudget(budget_id, 'note', note);

    });
    function deletePayments(){
    $('.delete-payment').on('click', function() {
    
        swal({
            title: "Atención",
            text: "¿Seguro que quiere eliminar este pago?",
            icon: "warning",
            buttons: {
                cancel: 'Cancelar',
                catch: {
                    text: "Ok",
                    value: "catch",
                },
            }
        })
        .then((value) => {
            switch (value) {
                case "catch":
                    deletePayment($(this).attr('data-id'));
                    break;
            }
        });
    });
}
function updateInput(){
    $(".inputs-payment").blur(function(){
        var val = $(this).val();
        var id = $(this).attr('data-id');
        var row = $(this).attr('data-row');
    
        $.ajax({
            type: "put",
            url: "/update/inputs-payment/"+id,
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
              val: val,
              id: id,
              row: row
            },
            success: function(result) {
              $("#data").html("");

            }
          });
        
     });
    }
function events() {
    var idBudget = 0;
    $('.addPayment').on('click', function() {
        var id = $(this).find('.id').val();
 
        $.ajax({
            dataType: 'json',
            url: $('#URL').val()+'addPayment',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                id: idBudget,
            },
            success: function(response) {
                var option = '<tr id="tr-'+response.data+'">';
                    option += '<td><i class="fas fa-check green-text size-delete pointer"></i></td>';
                    option += '<td><input class="inputs-payment payment enter-payment" type="text" data-row="amount" data-id="'+response.data+'"></td>';
                    option += '<td><input class="inputs-payment date-payment" type="date" data-row="expired_at" data-id="'+response.data+'"></td>';
                    option += '<td><input class="inputs-payment enter-payment" type="text" data-row="payment_for" data-id="'+response.data+'"></td>';
                    option += '<td><input class="inputs-payment date-payment" type="date" data-row="payed_at" data-id="'+response.data+'"></td>';
                    option += '<td><select data-row="payment_method" style="display: unset;"><option value="">Elegir metodo de pago</option><option value="1">Tarjeta(Credito/Debito)</option><option value="2">Efectivo</option><option value="3">Otro</option></select></td>';
                    option += '<td><i class="fas fa-trash-alt red-text pointer size-delete delete-payment" data-id="'+response.data+'"></i></td>';
                option += '</tr>';
                document.getElementById('content-payment'+id).innerHTML += option;
                updateInput();
               // addPayments();
            },
            error: function() {
                console.log('error');
            }
        });
    });

    $('.modalPayments').on('click', function() {
        idBudget = $(this).attr('data-id');
        document.getElementById('content-payment'+idBudget).innerHTML = '';
        $.ajax({
            dataType: 'json',
            url: $('#URL').val()+'extractPayment',
            method: 'post',
            data: {
                "_token": $("meta[name='csrf-token']").attr("content"),
                id: idBudget,
            },
            success: function(response) {
                console.log(response);
                var option = '';
                for (var i = 0; i < response.data.length; i++) {
                    var importe = '', vencimiento = '', pagado_por = '', fecha_pago = '', metodo_pago = '';
                    if(response.data[i].amount!=null) {
                        importe = response.data[i].amount;
                    }
                    if(response.data[i].expired_at!=null) {
                        vencimiento = response.data[i].expired_at;
                    }
                    if(response.data[i].payment_for!=null) {
                        pagado_por = response.data[i].payment_for;
                    }
                    if(response.data[i].payed_at!=null) {
                        fecha_pago = response.data[i].payed_at;
                    }
                    if(response.data[i].payment_method!=null) {
                        metodo_pago = response.data[i].payment_method;
                    }
                    option += '<tr id="tr-'+response.data[i].id+'">';
                        option += '<td><i class="fas fa-check green-text size-delete pointer"></i></td>';
                        option += '<td><input class="inputs-payment payment enter-payment" type="text" value="'+importe+'" data-row="amount" data-id="'+response.data[i].id+'"></td>';
                        option += '<td><input class="inputs-payment date-payment" type="date" value="'+vencimiento+'" data-row="expired_at" data-id="'+response.data[i].id+'"></td>';
                        option += '<td><input class="inputs-payment enter-payment" type="text" value="'+pagado_por+'" data-row="payment_for" data-id="'+response.data[i].id+'"></td>';
                        option += '<td><input class="inputs-payment date-payment" type="date" value="'+fecha_pago+'" data-row="payed_at" data-id="'+response.data[i].id+'"></td>';
                        option += '<td><input class="inputs-payment enter-payment" type="text" value="'+metodo_pago+'" data-row="payment_method" data-id="'+response.data[i].id+'"></td>';
                        option += '<td><i class="fas fa-trash-alt red-text pointer size-delete delete-payment" data-id="'+response.data[i].id+'"></i></td>';
                    option += '</tr>';
                }
                document.getElementById('content-payment'+idBudget).innerHTML += option;
                updateInput();
                deletePayments();
                $('#modal3').modal('open');
            },
            error: function() {
                console.log('error');
            }
        });
    });
  

    $('.number_format').maskMoney({prefix:'$ ', allowNegative: true, thousands:',', decimal:'.'});

    $('.input-presupuesto').blur(function(e) {
      
            updateBudget($(this).attr('data-id'), $(this).attr('data-tb'), $(this).val());
        
    });
    $('.input-names').blur(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        
            updateBudget($(this).attr('data-id'), $(this).attr('data-tb'), $(this).val());
        
    });

    $('.date').change(function() {
        updateBudget($(this).attr('data-id'), $(this).attr('data-tb'), $(this).val());
    });
}
function deleteRow(){
    $('.deleteColumn').on('click', function() {
        var id = $(this).find('.deleteRow').val();
        $.ajax({
            type: "delete",
            url: "/deleterow/"+id,
            data: {
              id: id,
              "_token": $("meta[name='csrf-token']").attr("content"),
            },
            success: function(result) {
                $('.row'+id).html("");
            }
        });
    });
    }
    deleteRow();
$('.add').on('click', function() {
    var id = $(this).attr('data-id');
    $.ajax({
        dataType: 'json',
        url: $('#URL').val()+'addBudget',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id,
            
            
        },
        success: function(response) {
            console.log(response);
            var option = '<tr class="row'+response.data.id+'">';
                option += '<td><input class="input-names" type="text" autofocus data-tb="name" data-id="'+response.data.id+'"></td>';
                option += '<td><input class="input-presupuesto" type="text" data-tb="budget" data-id="'+response.data.id+'"></td>';
                option += '<td><input class="input-presupuesto" type="text" data-tb="final_cost" data-id="'+response.data.id+'"></td>';
                option += '<td><span class="pointer modalPayments blue-text lighten-text-3" data-id="'+response.data.id+'">$ 0.00</span></td>';
                option += '<td>$ 0.00</td>';
                option += '<td><input class="date" type="date" data-tb="date_asigned" data-id="'+response.data.id+'"></td>';
                option += '<td>';
                    option += '<i class="material-icons">note</i>';
                    option += '<a class="deleteColumn"><input class="deleteRow" type="hidden" value="'+response.data.id+'"><i class="material-icons">delete</i></a>';
                    option += '<i class="material-icons">book</i>';
                    option += '<i class="material-icons">donut_large</i>';
                option += '</td>';
            option += '</tr>';
            document.getElementById('content-'+id).innerHTML += option;
            deleteRow();
        },
        error: function() {
            console.log('error');
        }
    });
});

/**/
function payments() {
    $('.payment').maskMoney({prefix:'$ ', allowNegative: true, thousands:',', decimal:'.'});
    $('.date-payment').change(function() {
        updatePayment($(this).attr('data-id'), $(this).attr('data-row'), $(this).val());
    });
    $('.delete-payment').on('click', function() {
        swal({
            title: "Atención",
            text: "¿Seguro que quiere eliminar este pago?",
            icon: "warning",
            buttons: {
                cancel: 'Cancelar',
                catch: {
                    text: "Ok",
                    value: "catch",
                },
            }
        })
        .then((value) => {
            switch (value) {
                case "catch":
                    deletePayment($(this).attr('data-id'));
                    break;
            }
        });
    });
    $('.enter-payment').blur(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
       
            updatePayment($(this).attr('data-id'), $(this).attr('data-row'), $(this).val());
        
    });
}

function deletePayment(id) {
    $.ajax({
        dataType: 'json',
        url: $('#URL').val()+'deletePayments',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id,
        },
        success: function(response) {
            if(response.id!='') {
                $('#tr-'+response.id).remove();
                swal("Pago eliminado correctamente", {
                icon: "success",
                });
            } else {
                swal("¡Lo sentimos, ocurrio un error!", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        }
    });
}

function updatePayment(id, campo, valor) {
    $.ajax({
        dataType: 'json',
        url: $('#URL').val()+'updatePayments',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id,
            row: campo,
            value: valor,
        },
        success: function(response) {
            if(response.status=='success') {
                swal("Datos guardados correctamente", {
                icon: "success",
                });
            } else {
                swal("¡Lo sentimos, ocurrio un error!", {
                icon: "error",
                });
            }
        },
        error: function() {
            console.log('error');
        }
    });
}

function updateBudget(id, tabla, valor) {
    if (valor) {
    $.ajax({
        dataType: 'json',
        url: $('#URL').val()+'updateBudget',
        method: 'post',
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            id: id,
            table: tabla,
            value: valor,
        },
        success: function(response) {
            
        },
        error: function() {
            console.log('error');
        }
    });
}
}
