<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_detail_id')->unsigned();
            $table->foreign('budget_detail_id')->references('id')->on('budget_details');
            $table->integer('status')->nullable();
            $table->string('amount', 255)->nullable();
            $table->string('payment_for', 255)->nullable();
            $table->string('payment_method', 255)->nullable();
            $table->date('expired_at')->nullable();
            $table->date('payed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_customers');
    }
}
