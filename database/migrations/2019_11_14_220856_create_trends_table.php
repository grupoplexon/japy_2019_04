<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trends', function (Blueprint $table) {
            $table->increments('id_trend');
            $table->string('name', 255);
            $table->string('description', 800);
            $table->string('image', 255);
            $table->string('cut', 255)->nullable();
            $table->string('neckline', 255)->nullable();
            $table->string('long', 255)->nullable();
            $table->string('type', 255)->nullable();
            $table->string('designer', 255)->nullable();
            $table->string('category', 255)->nullable();
            $table->string('season', 255)->nullable();
            $table->string('style', 255)->nullable();
            $table->integer('id_typeTrend')->unsigned();
            $table->foreign('id_typeTrend')->references('id')->on('type_trend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trends');
    }
}
