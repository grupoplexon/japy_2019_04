<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagazineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magazine', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->increments('id_article');
            $table->string('title', 800);
            $table->string('slug', 800);
            $table->longText('content');
            $table->string('main_image', 255);
            $table->tinyInteger('publish');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id_category')->on('magazine_category');
            $table->timestamps();
        });
    }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('magazine_category');
        Schema::dropIfExists('magazine');
    }
}
