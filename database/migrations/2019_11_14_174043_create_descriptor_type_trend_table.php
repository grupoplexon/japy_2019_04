<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescriptorTypeTrendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descriptor_type_trend', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_descriptor')->unsigned();
            $table->foreign('id_descriptor')->references('id')->on('descriptor_trend');
            $table->integer('id_typeTrend')->unsigned();
            $table->foreign('id_typeTrend')->references('id')->on('type_trend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descriptor_type_trend');
    }
}
