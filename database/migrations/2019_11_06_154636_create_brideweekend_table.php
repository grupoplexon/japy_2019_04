<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrideweekendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brideweekend', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city', 255);
            $table->date('initial_date');
            $table->date('final_date')->nullable();
            $table->string('enclosure', 255);
            $table->text('enclosure_description')->nullable();
            $table->text('enclosure_address')->nullable();
            $table->text('map');
            $table->string('img_enclosure', 255)->nullable();
            $table->string('logo_enclosure', 255)->nullable();
            $table->string('img_head', 255)->nullable();
            $table->string('organized_by', 255)->nullable();
            $table->string('entry_sat', 255)->nullable();
            $table->string('entry_sun', 255)->nullable();
            $table->string('runway_sat', 255)->nullable();
            $table->string('runway_sun', 255)->nullable();
            $table->integer('active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brideweekend');
    }
}
