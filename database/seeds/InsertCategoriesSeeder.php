<?php

use Illuminate\Database\Seeder;

class InsertCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name_category' => 'Flores/Decoración',
                'percentage' => '7'
            ],
            [
                'name_category' => 'Carro de Novios',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Catering/Banquete',
                'percentage' => '20'
            ],
            [
                'name_category' => 'Hogar',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Música/Producción',
                'percentage' => '5'
            ],
            [
                'name_category' => 'Financiamiento',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Foto/Video',
                'percentage' => '3'
            ],
            [
                'name_category' => 'Lugar para eventos',
                'percentage' => '22'
            ],
            [
                'name_category' => 'Hoteles',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Invitaciones',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Joyería/Accesorios',
                'percentage' => '3'
            ],
            [
                'name_category' => 'Maquillaje/Peinado',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Postres',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Ropa de etiqueta',
                'percentage' => '2'
            ],
            [
                'name_category' => 'Salud y Belleza',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Vestido de Novia',
                'percentage' => '7'
            ],
            [
                'name_category' => 'Wedding Planner',
                'percentage' => '4'
            ],
            [
                'name_category' => 'Souvenir/Recuerditos',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Lencería/Trajes de Baño',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Vestidos de Fiesta',
                'percentage' => '1'
            ],
            [
                'name_category' => 'Viajes',
                'percentage' => '4'
            ],
            [
                'name_category' => 'Mobiliario',
                'percentage' => '7'
            ]
        ];

        DB::table('categories')->insert($categories);
    }
}
