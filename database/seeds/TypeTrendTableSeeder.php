<?php

use Illuminate\Database\Seeder;
use App\TypeTrend;
class TypeTrendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new TypeTrend();
        $user->name = "Vestidos de Novia";
        $user->slug = "vestidos-novia";
        $user->sortable = "escote,corte,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Trajes de Novio";
        $user->slug = "trajes-novio";
        $user->sortable = "estilo,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Vestidos de Fiesta";
        $user->slug = "vestidos-fiesta";
        $user->sortable = "largo,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Joyería";
        $user->slug = "joyeria";
        $user->sortable = "tipo,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Zapatos";
        $user->slug = "zapatos";
        $user->sortable = "categoria,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Lencería";
        $user->slug = "lenceria";
        $user->sortable = "escote,corte,diseñador,temporada";
        $user->save();
        $user = new TypeTrend();
        $user->name = "Complementos";
        $user->slug = "complementos";
        $user->sortable = "categoria,tipo,diseñador,temporada";
        $user->save();
    }
}
