<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'categories',
            'roles',
            'users',
            'countries',
            'states',
            'cities',
            'magazine_category',
            'descriptor_trend',
            'descriptor_type_trend',
            'type_trend'
        ]);

        $this->call(InsertCategoriesSeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(categoryMagazine_seeder::class);
        $this->call(TypeTrendTableSeeder::class);
        $this->call(DescriptorTrendTableSeeder::class);
    }

    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
 
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
 
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
