<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "admin";
        $role->alias = "Administrador";
        $role->save();

        $role = new Role();
        $role->name = "usuario";
        $role->alias = "Novios";
        $role->save();

        $role = new Role();
        $role->name = "proveedor";
        $role->alias = "Proveedor";
        $role->save();
    }
}
