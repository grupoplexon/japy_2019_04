<?php

use Illuminate\Database\Seeder;
use App\MagazineCategory;

class categoryMagazine_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m = new MagazineCategory();
        $m->name_category = "Moda";
        $m->slug = "moda";
        $m->save();
        $m = new MagazineCategory();
        $m->name_category = "Belleza";
        $m->slug = "belleza";
        $m->save();
        $m = new MagazineCategory();
        $m->name_category = "Destinos";
        $m->slug = "destinos";
        $m->save();
        $m = new MagazineCategory();
        $m->name_category = "Espectaculos";
        $m->slug = "espectaculos";
        $m->save();
        $m = new MagazineCategory();
        $m->name_category = "Hogar";
        $m->slug = "hogar";
        $m->save();
        $m = new MagazineCategory();
        $m->name_category = "Novia";
        $m->slug = "novia";
        $m->save();
        
    }
}
