<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role_user = Role::where('name','usuario')->first();
        $role_admin = Role::where('name','admin')->first();

        // $user = new User();
        // $user->name = "Usuario";
        // $user->active = 2;
        // $user->user = 'cliente';
        // $user->email ="user@mail.com";
        // $user->registered_from ="Jalisco";
        // $user->password = bcrypt('123456789');
        // $user->save();
        // $user->roles()->attach($role_user);

        $user = new User();
        $user->name = "Administrador";
        $user->active = 1;
        $user->user = 'admin@admin.com';
        $user->registered_from ="Jalisco";
        $user->password = bcrypt('admNupcial2019');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
