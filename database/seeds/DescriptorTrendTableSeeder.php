<?php

use Illuminate\Database\Seeder;
use App\TypeTrend;
use App\DescriptorTrend;
class DescriptorTrendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vnovia = TypeTrend::where('id', 1)->first();

        $desc = new DescriptorTrend();
        $desc->name = "Barco"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Bardot"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Tortuga/ Cuello alto"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "En pico"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Joya"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Bañera"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "En V"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Recto"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Cuadrado"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Palabra de Honor"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Escote Halter"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Redondo"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Corazón"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Ilusión"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Otros"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Asimétrico"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Reina Ana"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Hombros caídos"; $desc->group = "ESCOTE"; $desc->save();
        $desc->type_trends()->attach(1);

        $desc = new DescriptorTrend();
        $desc->name = "Entallado"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Otros"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Sirena"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Imperio"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "En A"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Entallado"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Evase"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Princesa"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);
        $desc = new DescriptorTrend();
        $desc->name = "Recto"; $desc->group = "CORTE"; $desc->save();
        $desc->type_trends()->attach(1);

        $vnovia = TypeTrend::find(2);

        $desc = new DescriptorTrend();
        $desc->name = "Smoking"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Traje clásico"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Frac"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Chaqué"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Levita"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Semilevita"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Otros"; $desc->group = "ESTILO"; $desc->save();
        $desc->type_trends()->attach($vnovia);

        $vnovia = TypeTrend::find(3);

        $desc = new DescriptorTrend();
        $desc->name = "Corto"; $desc->group = "LARGO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Medio"; $desc->group = "LARGO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Largo"; $desc->group = "LARGO"; $desc->save();
        $desc->type_trends()->attach($vnovia);

        $vnovia = TypeTrend::find(4);

        $desc = new DescriptorTrend();
        $desc->name = "Aretes"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Collares"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Pulseras"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Mancuernillas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);

        $vnovia = TypeTrend::find(5);

        $desc = new DescriptorTrend();
        $desc->name = "Novia"; $desc->group = "CATEGORIA"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc->type_trends()->attach(7);
        $desc = new DescriptorTrend();
        $desc->name = "Novio"; $desc->group = "CATEGORIA"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc->type_trends()->attach(7);
        $desc = new DescriptorTrend();
        $desc->name = "Fiesta"; $desc->group = "CATEGORIA"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc->type_trends()->attach(7);

        $vnovia = TypeTrend::find(6);

        $desc = new DescriptorTrend();
        $desc->name = "Ropa interior"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Ropa de dormir"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();

        $vnovia = TypeTrend::find(7);

        $desc->name = "Corbatas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Mantillas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Redondo"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Sombreros"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Camisas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Chalecos"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Corbatas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Mantillas"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Redondo"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Sombreros"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Tocados"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Tops"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Toreras"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Velos"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        $desc = new DescriptorTrend();
        $desc->name = "Otros"; $desc->group = "TIPO"; $desc->save();
        $desc->type_trends()->attach($vnovia);
        

    }
}
