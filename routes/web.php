<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{name}', 'Home@index');

Route::get('/', 'HomeController@index')->name('index');
Route::get('/{slug?}/{state?}', 'HomeController@perfil')->name('/')->where('slug', '(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)$');
// Route::get('/', function() {
//     $user = App\Sector_activity::find(7);
//     return $user->extractCategory;
//     return 'HOLA';
// });

// Route::get('/login', 'HomeController@login')->name('login');
// Route::get('/register', 'HomeController@register')->name('register');
// Route::post('/register', 'HomeController@registerEmail')->name('register');
// Route::post('/extractcategory', 'RegisterController@category');
Route::get('/Admin', 'AdminController@index')->name('Admin');

Route::get('login', 'HomeController@cuenta')->name('login');

// Authentication Routes...
Route::get('loginNovia', 'Auth\LoginController@showLoginForm')->name('loginNovia');
Route::get('loginEmpresa', 'Auth\LoginController@showLoginFormEmpresa')->name('loginEmpresa');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/home', 'ProviderController@index')->name('home');

// Auth::routes();

Route::get('/Admin', function () {
    return view('Admin/index');
})->name('Admin')->middleware('auth','role:admin');

Route::get('proveedores/index', 'HomeController@proveedores')->name('proveedores/index');
Route::post('proveedores/filtrados', 'HomeController@filtros')->name('proveedores/filtrados');
Route::post('proveedores/autocomplete', 'HomeController@autocomplete');
Route::get('proveedores/buscar/{param}', 'HomeController@buscar')->name('proveedores/buscar')->where('param', '(.*)');
Route::get('planeador_boda', 'HomeController@planeador_boda')->name('planeador_boda');

Route::get('nuevo/{param}', 'AdminController@nuevo')->name('nuevo');

// Rutas Magazine
Route::get('/blog', 'MagazineController@index')->name('blog');

Route::get('/newArticle', 'MagazineController@newArticle')->name('newArticle');
Route::post('/saveArticle', 'MagazineController@saveArticle')->name('saveArticle');
Route::post('/uploadImage', 'MagazineController@uploadImage')->name('uploadImage');
Route::get('/editArticle/{id}', 'MagazineController@editArticle')->name('editArticle');
Route::post('/updateArticle', 'MagazineController@updateArticle')->name('updateArticle');
Route::post('/changeArticle', 'MagazineController@changeArticle')->name('changeArticle');
Route::post('/deleteArticle', 'MagazineController@deleteArticle')->name('deleteArticle');

Route::get('/magazine', 'HomeController@magazine')->name('magazine');
Route::get('/magazine/categoria/{slug}', 'HomeController@category');
Route::get('/magazine/articulo/{slug}', 'HomeController@article');

// Rutas Magazine end


// Rutas Tendencia

Route::get('/trends', 'TrendController@index')->name('trends');
Route::get('/newTrend', 'TrendController@newTrend')->name('newTrend');
Route::post('/trends/categories', 'TrendController@categories');
Route::post('/saveTrend', 'TrendController@saveTrend');
Route::post('/deleteTrend', 'TrendController@deleteTrend');
Route::get('/editTrend/{id}', 'TrendController@editTrend')->name('editTrend');
// Rutas Tendencia end

Route::get('/usuarios', 'SessionController@usuarios')->name('usuarios');
Route::get('/extractusers', 'AdminController@extractUsers');
Route::get('/edit/{id}', 'AdminController@editUser');
Route::post('/savegeneral', 'AdminController@savegeneral');
Route::post('editContact', 'AdminController@editContact');
Route::get('/cuenta', 'HomeController@cuenta')->name('cuenta');
Route::get('/registroNovia', 'HomeController@registroNovia')->name('registroNovia');
Route::get('/registroEmpresa', 'HomeController@registroEmpresa')->name('registroEmpresa');
Route::post('extractStates', 'HomeController@extractStates');
Route::post('extractCities', 'HomeController@extractCities');
Route::post('checkUser', 'HomeController@checkUser');
Route::post('registerProvider', 'Auth\RegisterController@register')->name('registerProvider');
Route::post('updateData', 'AdminController@updateData');
Route::get('/proveedores', 'AdminController@proveedores')->name('proveedores')->middleware('role:admin');
Route::get('extractproviders', 'AdminController@extractproviders');
Route::post('extractCategories', 'AdminController@extractCategories');
Route::post('saveNewLocation', 'AdminController@saveNewLocation');
Route::get('/verificaciones', 'AdminController@verificaciones')->name('verificaciones')->middleware('role:admin');
Route::get('extractVerifications', 'AdminController@extractVerifications');
Route::post('updateVerifications', 'AdminController@updateVerifications');
Route::post('admNotifications', 'AdminController@admNotifications');
Route::get('admin/brideweekend', 'AdminController@brideweekend')->name('admin/brideweekend')->middleware('role:admin');
Route::get('extractBW', 'AdminController@extractBW');
Route::post('saveCity', 'AdminController@saveCity')->name('saveCity');
Route::post('activeOrInactive', 'AdminController@activeOrInactive');
Route::get('editarBW/{id}', 'AdminController@editarBW')->name('editarBW');
Route::post('editCity', 'AdminController@editCity')->name('editCity');

//Rutas perfil Proveedor
Route::get('/escaparate', 'ProviderController@escaparate')->name('escaparate')->middleware('role:proveedor');
Route::post('checkPassword', 'ProviderController@checkPassword');
Route::post('updateCredentials', 'ProviderController@updateCredentials');
Route::post('updateContact', 'ProviderController@updateContact');
Route::post('updateCategory', 'ProviderController@updateCategory');
Route::post('updateCompany', 'ProviderController@updateCompany');
Route::get('/localizacion', 'ProviderController@localizacion')->name('localizacion')->middleware('role:proveedor');
Route::post('extractLocations', 'HomeController@extractLocations');
Route::post('searchCities', 'ProviderController@searchCities');
Route::post('updateAddress', 'ProviderController@updateAddress');
Route::get('/fotos', 'ProviderController@fotos')->name('fotos')->middleware('role:proveedor');
Route::post('uploadImages', 'ProviderController@uploadImages');
Route::post('deleteImage', 'ProviderController@deleteImage');
Route::post('updatePhoto', 'ProviderController@updatePhoto');
Route::post('updateLogo', 'ProviderController@updateLogo');
Route::get('/videos', 'ProviderController@videos')->name('videos')->middleware('role:proveedor');
Route::post('uploadVideo', 'ProviderController@uploadVideo');
Route::post('checkVideo', 'ProviderController@checkVideo');
Route::post('deleteVideo', 'ProviderController@deleteVideo');
Route::get('/promociones', 'ProviderController@promociones')->name('promociones')->middleware('role:proveedor');
Route::post('saveDiscount', 'ProviderController@saveDiscount');
Route::post('savePromotion', 'ProviderController@savePromotion');
Route::post('updatePromotion', 'ProviderController@updatePromotion');
Route::post('updatePrices', 'ProviderController@updatePrices');
Route::get('/solicitudes', 'ProviderController@solicitudes')->name('solicitudes')->middleware('role:proveedor');
Route::get('/recomendaciones', 'ProviderController@recomendaciones')->name('recomendaciones')->middleware('role:proveedor');

//Rutas perfil novio/novia
Route::post('registerBride', 'Auth\RegisterController@registerBride')->name('registerBride');
Route::post('saveBudget', 'CustomerController@saveBudget')->name('saveBudget');
Route::post('updateBudget', 'CustomerController@updateBudget');
Route::post('addBudget', 'CustomerController@addBudget');
Route::post('extractPayment', 'CustomerController@extractPayment');
Route::post('addPayment', 'CustomerController@addPayment');
Route::post('updatePayments', 'CustomerController@updatePayments');
Route::post('deletePayments', 'CustomerController@deletePayments');
Route::delete('deleterow/{id}', 'CustomerController@deleteRow');
Route::put('/update/inputs-payment/{id}', 'CustomerController@updateRowPayment');

//BrideWeekend
Route::get('brideweekend', 'BrideweekendController@index')->name('brideweekend');
Route::get('brideweekend/ciudad/{city}', 'BrideweekendController@ciudad')->name('brideweekend/ciudad');
Route::post('registerBwUSA', 'BrideweekendController@registerBwUSA');
Route::get('brideweekend/expositor', 'BrideweekendController@expositor')->name('brideweekend/expositor');
Route::get('brideweekend/concepto', 'BrideweekendController@concepto')->name('brideweekend/concepto');
Route::get('brideweekend/ciudades', 'BrideweekendController@ciudades')->name('brideweekend/ciudades');

Route::get('nameVideo', function() {
    $user = App\User::with('provider')->where('user', 'mmota@grupoplexon.com')->first();
    dd($user);
});
// Rutas Vestidos
Route::get('/vestidos/index', 'DressController@index')->name('vestidos/index');
Route::GET('/vestidos/categoria', 'DressController@categoria')->name('/vestidos/categoria');
Route::GET('/vestidos/categoria/search', 'DressController@search')->name('/vestidos/categoria/search');
