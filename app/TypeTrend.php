<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTrend extends Model
{
    protected $table='type_trend';

    protected $fillable = [
        'name', 'slug', 'sortable', 
    ];
    public function descriptor_trends()
    {
        return $this->belongsToMany("App\DescriptorTrend", "descriptor_type_trend", "id_typeTrend", "id_descriptor");
    }
}
