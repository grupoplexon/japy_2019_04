<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Provider;
use App\Location_provider;
use App\Role;
use App\MagazineCategory;
use App\Magazine;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = User::whereHas('role_user', function($query) {
            return $query->where('role_id', 3);
        })->with('provider')
        ->with('gallery')
        ->orderBy('created_at', 'DESC')
        ->take(3)
        ->get();
        return view('index')->with(['providers' => $providers]);
    }
    public function login()
    {
        return view('login');
    }
    public function register()
    {
        return view('register');
    }
    public function registerEmail(Request $request)
    {
        return view('register')->with('email', $request->input('email'));
    }

    public function cuenta() {
        return view('account');
    }
    public function registroNovia() {
        $states = DB::table('states')
        ->where('country_id', 142)
        ->select('id', 'state')
        ->get();
        return view('registroNovia')->with(['states' => $states]);
    }
    public function registroEmpresa() {
        $data = DB::table('categories')->get();
        $countries = DB::table('countries')->get();
        return view('registroEmpresa')->with(['data' => $data, 'countries' => $countries]);
    }
    public function extractStates(Request $request) {
        $states = DB::table('states')
        ->where('country_id', $request->input('idCountry'))
        ->select('id', 'state')
        ->get();
        return response()->json([
            'states' => $states
        ]);
    }
    public function extractCities(Request $request) {
        $cities = DB::table('cities')
        ->where('state_id', $request->input('idState'))
        ->select('id', 'city')
        ->get();
        return response()->json([
            'cities' => $cities
        ]);
    }
    public function extractLocations(Request $request) {
        $data = Location_provider::where('id', $request->input('id'))->first();
        return response()->json([
            'data' => $data
        ]);
    }
    public function checkUser(Request $request) {
        $user = User::where('user', $request->input('user'))->first();
        if(!empty($user)) {
            return response()->json([
                'data' => 'exist'
            ]);
        } else {
            return response()->json([
                'data' => 'no_exist'
            ]);
        }
    }
    public function proveedores() {
        $filters = array(
            'categoria' => null,
            'estado' => null,
            'promedio' => null,
            'precioMin' => null,
            'precioMax' => null,
            'proveedor' => null,
        );
        $categories = DB::table('categories')->get();
        $providers = User::whereHas('role_user', function($query) {
            $query->where('role_id', 3);
        })
        ->whereHas('location_provider', function($query) {
            $query->where('active', 1);
        })
        // ->where('active', 2)
        ->with('gallery')
        ->with('provider')->paginate(12);
        foreach ($providers as $key => $value) {
            $value->provider->description = strip_tags($value->provider->description);
        }
        return view('proveedores')->with(['categories' => $categories, 'providers' => $providers, 'filtros' => $filters]);
    }
    public function perfil($slug, $state=null) {
        $provider = User::whereHas('provider', function ($query) use($slug) {
            $query->where('slug', $slug);
        })
        ->with('provider')
        ->with('gallery')
        ->with('location_provider')
        ->with('role_user')
        ->with('review')
        ->first();
        $params = array([
            'state' => $state,
            'locations' => sizeof($provider->location_provider)
        ]);
        if(isset(Auth::user()->id)) {
            $user = User::where('id', Auth::user()->id)
            ->with('role_user')
            ->first();
            if($user->role_user->role_id==2) {
                $this->indicadores($provider->id, 'visit_profile');
            }
        } else {
            $this->indicadores($provider->id, 'visit_profile');
        }
        return view('perfilProveedor')->with(['provider' => $provider, 'params' => $params]);
    }
    protected function indicadores($user_id, $type) {
        $indicator = DB::table('indicators')
        ->where('user_id', $user_id)
        ->where('type', $type)
        ->where('created_at', date('Y-m-d'))
        ->first();
        if(empty($indicator)) {
            $saveIndicator = DB::table('indicators')->insert([
                'user_id' => $user_id,
                'type' => $type,
                'quantity' => 1,
                'created_at' => date('Y-m-d')
            ]);
        } else {
            $updateIndicator = DB::table('indicators')
            ->where('user_id', $user_id)
            ->where('type', $type)
            ->where('created_at', date('Y-m-d'))
            ->update(['quantity' => $indicator->quantity+1]);
        }
        return true;
    }

    public function filtros(Request $request) {
        // dd($request->input('rating'));
        $searchCategory = DB::table('categories')->where('id', $request->input('categories'))->first();
        if(empty($searchCategory)) {
            $name = null;
        } else {
            $name = $searchCategory->name_category;
        }
        $state = $request->input('estado');
        $category = $request->input('categories');
        $average = $request->input('rating');
        $priceMin = $request->input('priceMin');
        $priceMax = $request->input('priceMax');
        $filters = array(
            'categoria' => $request->input('categories'),
            'estado' => $request->input('estado'),
            'promedio' => $request->input('rating'),
            'precioMin' => $request->input('priceMin'),
            'precioMax' => $request->input('priceMax'),
            'nombreCategoria' => $name,
            'proveedor' => null,
        );
        if($filters['categoria']==null && $filters['estado']==null && $filters['promedio']==null && ($filters['precioMin']==null || $filters['precioMax']==null)) {
            return redirect('proveedores/index');
        }
        $provider = User::whereHas('location_provider', function ($query) use($state) {
            if(!empty($state)) {
                return $query->where('state', $state);
            }
        })->whereHas('provider', function($query) use($category, $priceMin, $priceMax) {
            if(!empty($category) && !empty($priceMin) && !empty($priceMax)) {
                return $query->where('category_id', $category)->where('price_min', '>=', $priceMin)->where('price_max', '>=', $priceMax);
            } elseif(empty($category) && !empty($priceMin) && !empty($priceMax)) {
                return $query->where('price_min', '>=', $priceMin)->where('price_max', '<=', $priceMax);
            } elseif(!empty($category) && (empty($priceMin) || empty($priceMax))) {
                return $query->where('category_id', $category);
            }
        })->whereHas('provider', function($query) use($average) {
            if(!empty($average)) {
                return $query->where('average', $average);
            }
        })->whereHas('location_provider', function($query) use($state) {
            if(!empty($state)) {
                return $query->where('state', $state)->where('active', 1);
            } else {
                return $query->where('active', 1);
            }
        })
        // ->where('active', 2)
        ->with('location_provider')
        ->with('provider')
        ->with('gallery')
        ->paginate(12);
        // dd($provider);
        $categories = DB::table('categories')->get();
        foreach ($provider as $key => $value) {
            $value->provider->description = strip_tags($value->provider->description);
        }
        return view('proveedores')->with(['categories' => $categories, 'providers' => $provider, 'filtros' => $filters]);
        // dd($provider);
    }

    public function autocomplete(Request $request) {
        $term = $request->input("term");
        $proveedores = User::select('name')->where('name', 'like', '%'.$term.'%')->where("active", 2)->get();
        $categories = DB::table('categories')->select('name_category')->where('name_category', 'like', '%'.$term.'%')->get();
        $aux = [];
        $pos = 0;
        foreach ($proveedores as $key => $v) {
            $aux[$pos] = $v['name'];
            $pos++;
        }
        foreach ($categories as $key => $v) {
            $aux[$pos] = $v->name_category;
            $pos++;
        }
        $busqueda = $aux;
        unset($aux);
        $pos = 0;

        return response()->json($busqueda);
    }

    public function buscar($param) {
        $provider = User::where('name', $param)
        ->where('active', 2)
        ->with('provider')
        ->with('gallery')
        ->paginate(12);
        $categories = DB::table('categories')->get();
        $filters = array(
            'categoria' => null,
            'estado' => null,
            'promedio' => null,
            'precioMin' => null,
            'precioMax' => null,
            'proveedor' => null,
        );
        if(sizeof($provider)>0) {
            $filters['proveedor'] = 'si';
            $provider[0]->provider->description = strip_tags($provider[0]->provider->description);
            $this->indicadores($provider[0]->provider->user_id, 'search_profile');
            return view('proveedores')->with(['categories' => $categories, 'providers' => $provider, 'filtros' => $filters]);
        } else {
            $category = DB::table('categories')->where('name_category', $param)->first();
            if(!empty($category)) {
                $provider = User::whereHas('provider', function($query) use($category) {
                    return $query->where('category_id', $category->id);
                })->whereHas('location_provider', function($query) {
                    $query->where('active', 1);
                })
                // ->where('active', 2)
                ->with('provider')
                ->with('gallery')
                ->paginate(12);
                
                $filters['categoria'] = $category->id;
                $filters['nombreCategoria'] = $param;
            }
            return view('proveedores')->with(['categories' => $categories, 'providers' => $provider, 'filtros' => $filters]);
        }
    }

    ///  ----- MAGAZINE -----

    function magazine(){
        $articles= Magazine::where('publish', 1)->paginate(6);
        $category= MagazineCategory::all();
        // dd($articles);

        return view('magazine.index')->with([
            'articles' => $articles,
            'categories' => $category,
            ]);

    }
    function category($slug)
    {

        $category = MagazineCategory::where('slug', $slug)->first();

        $articles= Magazine::where('publish', 1)->where('type', $category->id_category)->paginate(6);
        $category= MagazineCategory::all();

        return view('magazine.index')->with([
            'articles' => $articles,
            'categories' => $category,
            ]);
    }

    function article($slug)
    {
        $article= Magazine::where('publish', 1)->where('slug', $slug)->first();
        $articles= Magazine::where('publish', 1)->where('id_article', '!=', $article->id_article)->take(3)->get();
        $category= MagazineCategory::all();

        return view('magazine.article')->with([
            'article' => $article,
            'articles' => $articles,
            'categories' => $category,
            ]);
    }


    public function planeador_boda() {
        return view('conocenos');
    }
}
