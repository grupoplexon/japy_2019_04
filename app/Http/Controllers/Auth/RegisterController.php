<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Provider;
use App\Location_provider;
use App\Role;
use App\Customer;
use App\Budget;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        date_default_timezone_set('America/Mexico_City');
    }

    public function showRegistrationForm()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        // $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    public function registerBride(Request $request)
    {
        // $this->validator($request->all())->validate();

        event(new Registered($user = $this->createBride($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $request)
    {
        // dd(strip_tags($request['description']));
        // $role_provider = Role::where('name','proveedor')->first();
        // $user = User::create([
        //     'name' => $request['name_company'],
        //     'user' => $request['user'],
        //     'password' => bcrypt($request['password']),
        //     'registered_from' => $request['location'],
        //     'active' => 1,
        // ]);
        // $user->roles()->attach($role_provider);

        // $country = DB::table('countries')->where('id', $request['country'])->first();
        // $state = DB::table('states')->where('id', $request['state'])->first();

        // $infoUser = Location_provider::create([
        //     'user_id' => $user->id,
        //     'active' => 1,
        //     'country' => $country->country,
        //     'state' => $state->state,
        //     'city' => $request['city'],
        //     'address' => $request['address'],
        //     'postal_code' => $request['postal_code'],
        //     'name_contact' => $request['name'],
        //     'phone' => $request['phone'],
        //     'cellphone' => $request['cellphone'],
        //     'email' => $request['email'],
        //     'website' => $request['website'],
        // ]);

        $slug = str_replace(' ', '-', mb_strtolower($request['name_company']));
        $slug = 'boda-'.$slug;
        $provider = Provider::create([
            'user_id' => $user->id,
            'category_id' => $request['category'],
            'slug' => $slug,
            'description' => $request['description'],
            'discount' => 0,
            'account_type' => 0,
            'average' => 5
        ]);

        // $var = array(
        //     'name' => 'Miguel',
        // );
        // Mail::send('emails.welcomejapy', $var, function($message) use ($data) {
        //     $message->from('noresponda@japybodas.com', 'Japy');
        //     $message->to($data['email'])->subject('Bienvenido a Japy');
        // });

        return $user;
    }

    protected function createBride(array $request) {
        $role_customer = Role::where('name','usuario')->first();
        $user = User::create([
            'name' => $request['name'].' '.$request['last_name'],
            'user' => $request['email'],
            'password' => bcrypt($request['password']),
            'registered_from' => $request['state'],
            'active' => 2,
        ]);
        $user->roles()->attach($role_customer);

        $customer = Customer::create([
            'user_id' => $user->id,
            'email' => $request['email'],
            'phone' => $request['phone'],
            'gender' => $request['gender'],
            'date_wedding' => $request['date'],
        ]);

        $pos = 0;
        $categories = DB::table('categories')->get();
        foreach ($categories as $key => $val) {
            $budget[$pos]['user_id'] = $user->id;
           // $budget[$pos]['name'] = $val->name_category;
            $budget[$pos]['category_id'] = $val->id;
         //   $budget[$pos]['indicator'] = 'principal';
           // $budget[$pos]['percentage'] = $val->percentage;
            $pos++;
        }
        Budget::insert($budget);

        return $user;
    }
}
