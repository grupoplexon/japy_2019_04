<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\TypeTrend;
use App\DescriptorTrend;
use App\Trend;

class TrendController extends Controller
{
    public function index()
    {
        $trends = Trend::all();
        return view('admin.trend.trend')->with('trends', $trends);
    }   
    public function newTrend()
    {
        $category= TypeTrend::all();
        return view('admin.trend.newTrend')->with('category',$category);
    }
    public function categories(Request $request)
    {
        $category = $request->input('category');
        $category = TypeTrend::find($request->input('category'));
        // $category = $category->descriptor_trends;
        $categories = $category->descriptor_trends;
        $aux = [];
        $i=0;
        foreach ($categories as $key => $c) {
            if($i == 0){
                $aux[$i] = $c->group;
                $i++;
            }
            else if($c->group != $aux[$i-1]){
                $aux[$i] = $c->group;
                $i++;
            }  
        }
        $busqueda = $aux;

        return response()->json([
            'options' => $categories,
            'categories' => $busqueda
        ]);
    }

    public function saveTrend(Request $request)
    {

        $destination = 'resources/uploads/trends';
        $quality     = 20;
        $pngQuality  = 9;
        $file = $_FILES["files"];
        $data     = file_get_contents($file["tmp_name"]);
        $extension = pathinfo($_FILES["files"]["name"])["extension"];
        $fileName  = $this->store_file($data, $extension, "uploads/trends");
        $new_name_image = $file = $destination."/".$fileName;

        $trend =  new Trend;
        $trend->name = $request->input('name');
        $trend->description = $request->input('description');
        $trend->image = $fileName;
        $trend->id_typeTrend = $request->input('category');
        $trend->season = $request->input('season');
        $trend->designer = $request->input('designer');
       
        $descriptor = DescriptorTrend::find($request->input('select0'));
        $category = TypeTrend::find($request->input('category'));
        if($category->slug == 'vestidos-novia'){
            $trend->neckline = $descriptor->name;

            $descriptor2 = DescriptorTrend::find($request->input('select1'));
            $trend->cut = $descriptor2->name;
        }else if($category->slug == 'trajes-novio'){
            $trend->style = $descriptor->name;
        }else if($category->slug == 'vestidos-fiesta'){
            $trend->long = $descriptor->name;
        }else if($category->slug == 'joyeria'){
            $trend->type = $descriptor->name;
        }else if($category->slug == 'zapatos'){
            $trend->category = $descriptor->name;
        }else if($category->slug == 'lenceria'){
            $trend->type = $descriptor->name;
        }else if($category->slug == 'complementos'){
            $trend->category = $descriptor->name;
            $descriptor2 = DescriptorTrend::find($request->input('select1'));
            $trend->type = $descriptor2->name;
        }
        $trend->save();

        return response()->json([
            'status' => 'ok'
        ]);

        
    }
    function deleteTrend(Request $request)
    {
        $trends = Trend::where('id_trend', $request->input('idTrend'))->first();

        unlink('resources/uploads/trends/'.$trends->image);
        Trend::where('id_trend', $trends->id_trend)->delete();

        return response()->json([
            'status' => 'delete'
        ]);
    }

    function editTrend($id){
        $category= TypeTrend::all();
        $trend= Trend::where('id_trend', $id)->first();
        return view('admin.trend.editTrend')->with(['trend' => $trend, 'category' => $category]);
    }



    function store_file($file, $extension, $folder) 
    {
        $fileName = uniqid().".".$extension;
        file_put_contents("resources/$folder/$fileName", $file);

        return $fileName;
    }
}