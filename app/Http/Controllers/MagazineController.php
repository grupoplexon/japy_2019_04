<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\MagazineCategory;
use App\Magazine;
use App\Magazine_image;
use Illuminate\Routing\UrlGenerator;

class MagazineController extends Controller
{
    // FUNCTIONS ADMIN MAGAZINE

    public function index()
    {
        $articles= Magazine::all();
        return view('admin.magazine.magazine')->with('articles',$articles);
    }
    public function newArticle()
    {
        $category= MagazineCategory::all();
        return view('admin.magazine.newArticle')->with('category',$category);
    }
    public function saveArticle(Request $request)
    {
            $category = $request->input('category');

            $slug = str_replace(' ', '-', mb_strtolower($request['title']));

            $destination = 'resources/uploads/magazine';
            $quality     = 20;
            $pngQuality  = 9;
            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/magazine");
            $new_name_image = $file = $destination."/".$fileName;
            $article = Magazine::create([
                'title' => $request->input('title'),
                'slug'  => $slug,
                'content' => $request->input('contenido'),
                'main_image' => $fileName,
                'type' => $request->input('category'),
                'publish' => $request->input('publish'),
            ]); 

            $images = Magazine_image::where('id_article', null)->get();
            foreach($images as $i){
                // if($i->id_article == null){
                    Magazine_image::where('id_image', $i->id_image)->update([
                        'id_article' => $article->id
                    ]);
                // }
            }
            if($article){
                // return redirect()->action('MagazineController@index');
                return response()->json([
                    'status' => $article->id
                ]);
            }
    }  
    public function editArticle($id)
    {
        $category= MagazineCategory::all();
        $article= Magazine::where('id_article', $id)->first();
        return view('admin.magazine.editArticle')->with(['article' => $article, 'category' => $category]);
    }
    public function updateArticle(Request $request)
    {

        $image = Magazine::where('id_article', $request->input('id'))->first();
        unlink('resources/uploads/magazine/'.$image->main_image);

        $slug = str_replace(' ', '-', mb_strtolower($request['title']));

        $destination = 'resources/uploads/magazine';
        $quality     = 20;
        $pngQuality  = 9;
        $file = $_FILES["files"];
        $data     = file_get_contents($file["tmp_name"]);
        $extension = pathinfo($_FILES["files"]["name"])["extension"];
        $fileName  = $this->store_file($data, $extension, "uploads/magazine");
        $new_name_image = $file = $destination."/".$fileName;

        $article = Magazine::where('id_article', $request->input('id'))->update([
            'title' => $request->input('title'),
            'slug' =>$slug,
            'content' => $request->input('contenido'),
            'main_image' => $fileName,
            'type' => $request->input('category'),
            'publish' => $request->input('publish'),
        ]); 
        if($article){
            return response()->json([
                'status' => 'saved'
            ]);
        }
    }
    public function changeArticle(Request $request)
    {

        $slug = str_replace(' ', '-', mb_strtolower($request['title']));

        // dd($slug);h

        $article = Magazine::where('id_article', $request->input('id'))->update([
            'title' => $request->input('title'),
            'slug' =>$slug,
            'content' => $request->input('contenido'),
            'type' => $request->input('category'),
            'publish' => $request->input('publish'),
        ]); 
        if($article){
            return response()->json([
                'status' => 'saved'
            ]);
        }
    }
    public function uploadImage(Request $request)
    {
        $image =  $request->file('file');
        $image_name =  uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('resources/uploads/magazine/');
        $image->move($destinationPath, $image_name);
        $imagen = 'http://localhost/japy_2019_04/public/resources/uploads/magazine/'. $image_name;

        Magazine_image::create([
            'image' => $image_name,
        ]);
        return response()->json([
            'link' => $imagen
        ]);
    }

    public function deleteArticle(Request $request)
    {

        $article = Magazine::where('id_article', $request->input('idArticle'))->first();
        $images = Magazine_image::where('id_article', $article->id_article)->get();

        foreach($images as $img){
            unlink('resources/uploads/magazine/'.$img->image);
            Magazine_image::where('id_image', $img->id_image)->delete();
        }
        unlink('resources/uploads/magazine/'.$article->main_image);
        Magazine::where('id_article', $article->id_article)->delete();

        return response()->json([
            'status' => 'delete'
        ]);
    }
    function store_file($file, $extension, $folder) {
        $fileName = uniqid().".".$extension;
        file_put_contents("resources/$folder/$fileName", $file);

        return $fileName;
    }

    // FUNCTIONS END ADMIN MAGAZINE


}
