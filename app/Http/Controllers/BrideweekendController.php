<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

class BrideweekendController extends Controller
{
    public function __construct() {
        date_default_timezone_set('America/Mexico_City');
    }

    public function index() {
        $data = DB::table('brideweekend')
        ->where('active', 1)
        ->where('initial_date',  '>=', DB::raw('curdate()'))
        ->orderBy('initial_date')
        ->get();
        return view('Brideweekend.index')->with(['data' => $data]);
    }

    public function ciudad($city) {
        $data = DB::table('brideweekend')->where('city', 'LIKE', '%'.$city.'%')->first();
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); 
        $fechaIni = Carbon::parse($data->initial_date);
        $fechaFin = Carbon::parse($data->final_date);
        $mes = $meses[($fechaIni->format('n')) - 1];
        $mestwo = $meses[($fechaFin->format('n')) - 1];
        if($mes==$mestwo) {
            $data->dateParse = $mes.' '.$fechaIni->format('d').' y '. $fechaFin->format('d') . ' | ' . $fechaIni->format('Y');
        } elseif(!empty($data->initial_date) && !empty($data->final_date)) {
            $data->dateParse = $mes.' '.$fechaIni->format('d').' y '. $mestwo .' '. $fechaFin->format('d') . ' ' . $fechaIni->format('Y');
        } else {
            $data->dateParse = $mes.' '.$fechaIni->format('d'). ' | ' . $fechaIni->format('Y');
        }
        $entrySat = date("g:i a",strtotime(substr($data->entry_sat,0,5)));
        $exitSat = date("g:i a",strtotime(substr($data->entry_sat,-5,5)));
        $horarioSat = $entrySat.' - '.$exitSat;
        $entrySun = date("g:i a",strtotime(substr($data->entry_sun,0,5)));
        $exitSun = date("g:i a",strtotime(substr($data->entry_sun,-5,5)));
        $horarioSun = $entrySun.' - '.$exitSun;
        if($horarioSat==$horarioSun) {
            $data->timeParse = 'Sábado y Domingo '.$horarioSat;
        } elseif(!empty($data->entry_sat) && !empty($data->entry_sun)) {
            $data->timeParse = 'Sábado '.$entrySat.' - '.$exitSat.' Domingo '.$entrySun.' - '.$exitSun;
        } else {
            $data->timeParse = 'Domingo '.$entrySun.' - '.$exitSun;
        }
        // dd($horario);
        return view('Brideweekend.ciudad')->with(['city' => $data]);
    }

    public function registerBwUSA(Request $request) {
        $var = array(
            'name' => $request->input('nombre'),
            'email' => $request->input('correo'),
            'phone' => $request->input('telefono'),
            'msg' => $request->input('msg'),
        );
        Mail::send('emails.contactBw', $var, function($message) {
            $message->from('noresponder@brideadvisor.mx', 'Contacto BrideWeekend');
            $message->to('mmota@grupoplexon.com')->subject('Un usuario te contacto');
        });
        return response()->json([
            'status' => 'saved'
        ]);
    }

    public function expositor() {
        return view('Brideweekend.expositor');
    }

    public function concepto() {
        $data = DB::table('brideweekend')
        ->where('active', 1)
        ->where('initial_date',  '>=', DB::raw('curdate()'))
        ->orderBy('initial_date')
        ->get();
        return view('Brideweekend.concepto')->with(['cities' => $data]);
    }

    public function ciudades() {
        $data = DB::table('brideweekend')
        ->where('active', 1)
        ->where('initial_date',  '>=', DB::raw('curdate()'))
        ->orderBy('initial_date')
        ->get();
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        foreach ($data as $key => $value) {
            $fechaIni = Carbon::parse($value->initial_date);
            $fechaFin = Carbon::parse($value->final_date);
            $mes = $meses[($fechaIni->format('n')) - 1];
            $mestwo = $meses[($fechaFin->format('n')) - 1];
            if($mes==$mestwo) {
                $value->dateParse = $mes.' '.$fechaIni->format('d').' y '. $fechaFin->format('d');
            } elseif(!empty($value->initial_date) && !empty($value->final_date)) {
                $value->dateParse = $mes.' '.$fechaIni->format('d').' y '. $mestwo .' '. $fechaFin->format('d');
            } else {
                $value->dateParse = $mes.' '.$fechaIni->format('d');
            }
        }
        // dd($data);
        return view('Brideweekend.ciudades')->with('ciudades', $data);
    }
}
