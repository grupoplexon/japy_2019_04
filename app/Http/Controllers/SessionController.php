<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Provider;
use App\Info_user;
use App\Gallery;
use App\Promotion;

class SessionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->homeByRole();
    }

    public function homeByRole()
    {
        if (Auth::user()->hasRole('admin'))
        {
            return view('admin.index');
        }
        else if (Auth::user()->hasRole('proveedor'))
        {
            return view('Provider.index');
        }
        else if (Auth::user()->hasRole('usuario'))
        {
            return view('index');
        }
    }
    public function usuarios() {
        return view('admin.usuarios');
    } 
    public function escaparate() {
        $provider = DB::table('users as a')
        ->join('providers as b', 'a.id', '=', 'b.user_id')
        ->where('a.id', Auth::user()->id)
        ->select(
            'a.*',
            'b.description',
            'b.category_id' 
        )
        ->first();
        $info = Info_user::where('user_id', Auth::user()->id)->get();
        $categories = DB::table('categories')->get();
        return view('Provider.escaparate')->with(['data' => $provider, 'category' => $categories, 'info' => $info]);
    }

    public function checkPassword(Request $request) {
        $validate = DB::table('users')->where('id', $request->input('idUser'))->first();
        if (Hash::check($request->input('password'), $validate->password)) {
            return response()->json([
                'status' => 'coincide'
            ]);
        } else {
            return response()->json([
                'status' => 'no_coincide'
            ]);
        }
    }  
    public function updateCredentials(Request $request) {
        $user = User::where('id', Auth::user()->id)->first();
        if(!empty($request->input('password'))) {
            $user->user = $request->input('user');
            $user->password = bcrypt($request->input('password'));
        } else {
            $user->user = $request->input('user');
        }
        if($user->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateContact(Request $request) {
        // $user = User::where('id', Auth::user()->id)->first();
        // $provider = Provider::where('user_id', Auth::user()->id)->first();
        // $user->email = $request->input('email');
        // $provider->name_contact = $request->input('name');
        // $provider->phone = $request->input('phone');
        // $provider->cellphone = $request->input('cellphone');
        // $provider->website = $request->input('website');
        $info = Info_user::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $info->name_contact = $request->input('name');
        $info->phone = $request->input('phone');
        $info->cellphone = $request->input('cellphone');
        $info->website = $request->input('website');
        if($info->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateCategory(Request $request) {
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $provider->category_id = $request->input('category');
        if($provider->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateCompany(Request $request) {
        $user = User::where('id', Auth::user()->id)->first();
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $description_new = str_replace("\n", "", $request->input('description'));
        $description_new = str_replace("\r", "", $description_new);
        $description_old = str_replace("\n", "", $provider->description);
        $description_old = str_replace("\r", "", $description_old);
        $saved_name = 0;
        $saved_description = 0;
        if($request->input('name_company')!=$user->name) {
            $verif = DB::table('verifications')->where('user_id', Auth::user()->id)->where('type', 'name')->first();
            if($verif) {
                $update = DB::table('verifications')
                ->where('user_id', Auth::user()->id)
                ->where('type', 'name')
                ->update([
                    'data' => $request->input('name_company')
                ]);
            } else {
                $save = DB::table('verifications')->insert([
                    'user_id' => Auth::user()->id,
                    'type' => 'name',
                    'data' => $request->input('name_company'),
                ]);
            }

            $saved_name = 1;
        }
        if($description_new!=$description_old) {
            $verif = DB::table('verifications')->where('user_id', Auth::user()->id)->where('type', 'description')->first();
            if($verif) {
                $update = DB::table('verifications')
                ->where('user_id', Auth::user()->id)
                ->where('type', 'description')
                ->update([
                    'data' => $request->input('description')
                ]);
            } else {
                $save = DB::table('verifications')->insert([
                    'user_id' => Auth::user()->id,
                    'type' => 'description',
                    'data' => $request->input('description'),
                ]);
            }
            $saved_description = 1;
        }
        if($saved_name==0 && $saved_description==0) {
            return response()->json([
                'status' => 'no_saved'
            ]);
        } else {
            return response()->json([
                'status' => 'saved'
            ]);
        }
    }


    public function localizacion() {
        $provider = DB::table('users as a')
        ->join('info_users as b', 'a.id', '=', 'b.user_id')
        ->where('a.id', Auth::user()->id)
        ->select('a.*', 'b.id as id_loc', 'b.country', 'b.state', 'b.city', 'b.address', 'b.postal_code', 'b.latitude', 'b.longitude')
        ->get();
        $country = DB::table('countries')->get();
        return view('Provider.localizacion')->with(['data' => $provider, 'countries' => $country]);
    }

    public function extractLocations(Request $request) {
        $data = Info_user::where('id', $request->input('id'))->first();
        return response()->json([
            'data' => $data
        ]);
    }

    public function searchCities(Request $request) {
        $state = DB::table('states')->where('state', $request->input('state'))->first();
        $cities = DB::table('cities')->where('state_id', $state->id)->get();
        return response()->json([
            'cities' => $cities
        ]);
    }

    public function updateAddress(Request $request) {
        $update = Info_user::where('id', $request->input('id'))->first();
        $update->city = $request->input('city');
        $update->postal_code = $request->input('postal_code');
        $update->address = $request->input('address');
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function fotos() {
        $images = Gallery::where('user_id', Auth::user()->id)->where('type', 'image')->get();
        return view('Provider.fotos')->with(['images' => $images]);
    }

    public function uploadImages(Request $request) {
        $destination = 'resources/uploads/providers';
        $quality     = 20;
        $pngQuality  = 9;
        $file = $_FILES["files"];
        $data     = file_get_contents($file["tmp_name"]);
        $extension = pathinfo($_FILES["files"]["name"])["extension"];
        $fileName  = $this->store_file($data, $extension, "uploads/providers");
        $new_name_image = $file = $destination."/".$fileName;
        // $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
        $image = Gallery::create([
            'user_id' => Auth::user()->id,
            'name_image' => $fileName,
            'type' => 'image'
        ]);
        return response()->json([
            'status' => $fileName,
            'data' => $image
        ]);
    }

    function store_file($file, $extension, $folder) {
        $fileName = uniqid().".".$extension;
        file_put_contents("resources/$folder/$fileName", $file);

        return $fileName;
    }

    public function deleteImage(Request $request) {
        $image = Gallery::where('id', $request->input('id'))->where('user_id', Auth::user()->id)->first();
        unlink('resources/uploads/providers/'.$image->name_image);
        $image->delete();
        return response()->json([
            'status' => 'deleted'
        ]);
    }

    public function updatePhoto(Request $request) {
        $profile = DB::table('gallery')
        ->where('user_id', Auth::user()->id)
        ->update(['profile' => null]);
        $update = Gallery::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $update->profile = 1;
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateLogo(Request $request) {
        $profile = DB::table('gallery')
        ->where('user_id', Auth::user()->id)
        ->update(['logo' => null]);
        $update = Gallery::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $update->logo = 1;
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function videos() {
        $video = Gallery::where('type', 'video')->where('user_id', Auth::user()->id)->get();
        return view('Provider.videos')->with(['video' => $video]);
    }

    public function uploadVideo(Request $request) {
        $exist = Gallery::where('type', 'video')->where('user_id', Auth::user()->id)->first();
        if(empty($exist)) {
            $extension = substr($_FILES['archivo']['type'], -3);
            $nombre_temporal = $_FILES['archivo']['tmp_name'];
            // $nombre = $_FILES['archivo']['name'];
            $nombre = uniqid().'.'.$extension;
            $video = Gallery::create([
                'user_id' => Auth::user()->id,
                'name_image' => $nombre,
                'type' => 'video'
            ]);
            move_uploaded_file($nombre_temporal, 'resources/uploads/providers/'.$nombre);
            return response($video);
        } else {
            return 'exist';
        }
    }

    public function checkVideo() {
        $check = Gallery::where('user_id', Auth::user()->id)->where('type', 'video')->first();
        if(!empty($check)) {
            return response()->json([
                'status' => 'no_more'
            ]);
        } else {
            return response()->json([
                'status' => 'ok'
            ]);
        }
    }

    public function deleteVideo(Request $request) {
        $delete = Gallery::where('id', $request->input('id'))->where('user_id', Auth::user()->id)->first();
        unlink('resources/uploads/providers/'.$delete->name_image);
        if($delete->delete()) {
            return response()->json([
                'status' => 'deleted'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function promociones() {
        $data = Promotion::where('user_id', Auth::user()->id)->get();
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        return view('Provider.promociones')->with(['promotions' => $data, 'provider' => $provider]);
    }

    public function saveDiscount(Request $request) {
        $discount = DB::table('providers')
        ->where('user_id', Auth::user()->id)
        ->update(['discount' => $request->input('discount')]);
        return response()->json([
            'status' => 'saved'
        ]);
    }

    public function savePromotion(Request $request) {
        date_default_timezone_set("America/Mexico_City");
        // dd($_FILES);
        $destination = 'resources/uploads/promotions';
        $quality     = 20;
        $pngQuality  = 9;
        $file = $_FILES["files"];
        $data     = file_get_contents($file["tmp_name"]);
        $extension = pathinfo($_FILES["files"]["name"])["extension"];
        $fileName  = $this->store_file($data, $extension, "uploads/promotions");
        $new_name_image = $file = $destination."/".$fileName;

        $save = Promotion::create([
            'user_id' => Auth::user()->id,
            'type' => $request->input('type'),
            'name_promotion' => $request->input('name'),
            'image_promotion' => $fileName,
            'price_original' => $request->input('price'),
            'price_secondary' => $request->input('price2'),
            'description' => $request->input('description'),
            'date_initial' => date('y-m-d'),
            'date_final' => $request->input('date'),
            'status' => 1
        ]);
    }
}