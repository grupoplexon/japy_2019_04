<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Location_provider;
use App\Gallery;
use App\Category;
use App\Provider;
use App\Verification;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->user()->authorizeRoles('proveedor');
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admin');

        $data = DB::table('users')->get();

        $user = App\User::with('location')->with('role_user')->get();

        return $user;

        
       // return view('Admin.index', $data);
    }

    public function usuarios() {
        $notification = Verification::where('status', 1)->get()->count();
        return view('admin.usuarios')->with(['notification' => $notification]);
    }

    public function extractUsers() {
        $users = User::with('role_user')->get();
        return datatables()->of($users)->toJson();
    }

    public function editUser($id) {
        $data = User::with('location_provider')->with('role_user')->with('provider')->where('id', $id)->first();
        $data->rol = Role::find($data->role_user->role_id);
        $states = DB::table('states')->where('country_id', 142)->get();
        $notification = Verification::where('status', 1)->get()->count();
        // dd($data);
        return view('Admin.edituser')->with(['data' => $data, 'states' => $states, 'notification' => $notification]);
    }

    public function saveNewLocation(Request $request) {
        $location = Location_provider::create([
            'user_id' => $request->input('idUser'),
            'active' => $request->input('active'),
            'country' => 'Mexico',
            'state' => $request->input('state'),
            'city' => $request->input('city'),
            'name_contact' => $request->input('nameContact'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'cellphone' => $request->input('cellphone'),
            'website' => $request->input('website'),
        ]);
        if($location) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function savegeneral(Request $request) {
        $user = User::where('id', $request->input('id'))->first();
        $user->name = $request->input('name');
        $user->user = $request->input('user');
        if(!empty($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }
        if($user->save()) {
            return response()->json([
                'status' => 'ok'
            ]);
        } else {
            return response()->json([
                'status' => 'failed'
            ]);
        }
    }

    public function editContact(Request $request) {
        $info = Location_provider::where('id', $request->input('id'))->first();
        $info->name_contact = $request->input('nameContact');
        $info->email = $request->input('email');
        $info->phone = $request->input('phone');
        $info->cellphone = $request->input('cellphone');
        $info->website = $request->input('website');
        $info->active = $request->input('active');
        if($info->save()) {
            return response()->json([
                'status' => 'ok'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateData(Request $request) {
        if($request->input('type') == 'active') {
            $gallery = Gallery::where('user_id', $request->input('id'))->where('type', 'image')->get()->count();
            if($gallery>7 || $request->input('value')!=2) {
                $user = User::where('id', $request->input('id'))->first();
                $user->active = $request->input('value');
                if($user->save()) {
                    return response()->json([
                        'status' => 'true'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'noComplete'
                ]);
            }
        } else {
            $provider = Provider::where('user_id', $request->input('id'))->first();
            if($request->input('type')=='account_type') {
                $provider->account_type = $request->input('value');
            } elseif($request->input('type')=='category') {
                $provider->category_id = $request->input('value');
            } elseif($request->input('type')=='time') {
                $fecha = date('Y-m-d');
                $dias = 30 * $request->input('value');
                $nuevafecha = strtotime ( '+'.$dias.' day' , strtotime ( $fecha ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $provider->expiration = $nuevafecha;
            }
            if($provider->save()) {
                return response()->json([
                    'status' => 'true'
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    public function proveedores() {
        $notification = Verification::where('status', 1)->get()->count();
        return view('admin.proveedores')->with(['notification' => $notification]);
    }

    public function extractproviders() {
        $providers = User::whereHas('role_user', function ($query) {
            $query->where('role_id', 3);
        })
        ->with('role_user')
        ->with('provider')
        ->get();
        return datatables()->of($providers)->toJson();
    }

    public function extractCategories() {
        $categories = Category::all();
        return response()->json([
            'categories' => $categories
        ]);
    }

    public function verificaciones() {
        $notification = Verification::where('status', 1)->get()->count();
        return view('admin.verificaciones')->with(['notification' => $notification]);
    }

    public function extractVerifications() {
        $data = DB::table('verifications as a')
        ->leftJoin('providers as b', 'a.user_id', '=', 'b.user_id')
        ->leftJoin('users as c', 'a.user_id', '=', 'c.id')
        ->where('status', 1)
        ->select([
            'a.id as id_verification', 
            'a.user_id', 
            'c.name', 
            'b.description', 
            'b.price_min',
            'b.price_max',
            'a.type', 
            'a.data', 
            'a.status',
        ])
        ->get();
        return datatables()->of($data)->toJson();
    }

    public function updateVerifications(Request $request) {
        $update = '';
        if($request->input('indicator')=='prices') {
            if($request->input('status')=='aceptado') {
                $update = Provider::where('user_id', $request->input('idUser'))->first();
                $data = str_replace(' ', '', $request->input('data'));
                $array = str_split($data);
                $tam = sizeof($array);
                $cont = 0;
                $bandera = 0;
                $priceMin = '';
                do {
                    if($array[$cont]!='-') {
                        $priceMin = $priceMin.''.$array[$cont];
                    } else {
                        $bandera = 1;
                    }
                    $cont++;
                } while($cont<$tam && $bandera==0);
                $cont = $tam-1;
                $bandera = 0;
                $priceMax = '';
                do {
                    if($array[$cont]!='-') {
                        $priceMax = $array[$cont].''.$priceMax;
                    } else {
                        $bandera = 1;
                    }
                    $cont--;
                } while($cont>=0 && $bandera==0);
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'prices')
                ->update([
                    'status' => 0
                ]);
                $update->price_min = $priceMin;
                $update->price_max = $priceMax;
                $update->save();
            } else {
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'prices')
                ->update([
                    'status' => 0
                ]);
            }
        } elseif($request->input('indicator')=='name') {
            if($request->input('status')=='aceptado') {
                $update = User::where('id', $request->input('idUser'))->first();
                $update->name = $request->input('data');
                $update->save();
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'name')
                ->update([
                    'status' => 0
                ]);
            } else {
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'name')
                ->update([
                    'status' => 0
                ]);
            }
        } elseif($request->input('indicator')=='description') {
            if($request->input('status')=='aceptado') {
                $update = Provider::where('user_id', $request->input('idUser'))->first();
                $update->description = $request->input('data');
                $update->save();
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'description')
                ->update([
                    'status' => 0
                ]);
            } else {
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'description')
                ->update([
                    'status' => 0
                ]);
            }
        } elseif($request->input('indicator')=='images') {
            if($request->input('status')=='aceptado') {
                $update = User::where('id', $request->input('idUser'))->first();
                $update->active = 2;
                $update->save();
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'images')
                ->update([
                    'status' => 0
                ]);
            } else {
                $verification = DB::table('verifications')
                ->where('id', $request->input('idVerification'))
                ->where('type', 'images')
                ->update([
                    'status' => 0
                ]);
            }
        }
        return response()->json([
            'status' => 'saved'
        ]);
    }

    public function admNotifications() {
        $data = Verification::where('status', 1)->get()->count();
        return response()->json([
            'data' => $data
        ]);
    }

    public function brideweekend() {
        $notification = Verification::where('status', 1)->get()->count();
        return view('Brideweekend.Admin.index')->with(['notification' => $notification]);
    }

    public function extractBW() {
        $data = DB::table('brideweekend')->get();
        return datatables()->of($data)->toJson();
    }

    public function nuevo($param) {
        if($param=='ciudad') {
            return view('Brideweekend.Admin.nueva');
        }
    }

    public function saveCity(Request $request) {
        $img_enclosure = null;
        $img_head = null;
        $logo_enclosure = null;
        $destination = 'resources/uploads/brideweekend';
        if(!empty($_FILES["img_enclosure"]["name"])) {
            $file = $_FILES["img_enclosure"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_enclosure"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $img_enclosure = $fileName;
        }
        if(!empty($_FILES["img_head"]["name"])) {
            $file = $_FILES["img_head"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_head"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $img_head = $fileName;
        }
        if(!empty($_FILES["logo_enclosure"]["name"])) {
            $file = $_FILES["logo_enclosure"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["logo_enclosure"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $logo_enclosure = $fileName;
        }
        $save = DB::table('brideweekend')->insert([
            'city' => $request->input('city'),
            'initial_date' => $request->input('initial_date'),
            'final_date' => $request->input('final_date'),
            'enclosure' => $request->input('enclosure'),
            'enclosure_description' => $request->input('description'),
            'enclosure_address' => $request->input('address'),
            'map' => $request->input('map'),
            'img_enclosure' => $img_enclosure,
            'logo_enclosure' => $logo_enclosure,
            'img_head' => $img_head,
            'organized_by' => $request->input('organized_by'),
            'entry_sat' => $request->input('entry_sat_start').'-'.$request->input('entry_sat_end'),
            'entry_sun' => $request->input('entry_sun_start').'-'.$request->input('entry_sun_end'),
            'runway_sat' => $request->input('runway_sat_start').'-'.$request->input('runway_sat_end'),
            'runway_sun' => $request->input('runway_sun_start').'-'.$request->input('runway_sun_end'),
            'active' => $request->input('group1'),
        ]);
        return redirect('admin/brideweekend');
    }

    public function activeOrInactive(Request $request) {
        $update = DB::table('brideweekend')
        ->where('id', $request->input('id'))
        ->update([
            'active' => $request->input('value'),
        ]);
        return response()->json([
            'status' => 'saved'
        ]);
    }

    public function editarBW($id) {
        // dd($id);
        $data = DB::table('brideweekend')->where('id', $id)->first();
        $data->entry_sat_start = substr($data->entry_sat, 0, 5);
        $data->entry_sat_end = substr($data->entry_sat, 6, 5);
        $data->entry_sun_start = substr($data->entry_sun, 0, 5);
        $data->entry_sun_end = substr($data->entry_sun, 6, 5);
        $data->runway_sat_start = substr($data->runway_sat, 0, 5);
        $data->runway_sat_end = substr($data->runway_sat, 6, 5);
        $data->runway_sun_start = substr($data->runway_sun, 0, 5);
        $data->runway_sun_end = substr($data->runway_sun, 6, 5);
        return view('Brideweekend.Admin.editar')->with(['data' => $data]);
    }

    public function editCity(Request $request) {
        $img_enclosure = null;
        $img_head = null;
        $logo_enclosure = null;
        $destination = 'resources/uploads/brideweekend';
        $images = DB::table('brideweekend')->where('id', $request->input('id'))->first();
        if(!empty($_FILES["img_enclosure"]["name"])) {
            if(!empty($images->img_enclosure)) {
                unlink('resources/uploads/brideweekend/'.$images->img_enclosure);
            }
            $file = $_FILES["img_enclosure"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_enclosure"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $img_enclosure = $fileName;
        }
        if(!empty($_FILES["img_head"]["name"])) {
            if(!empty($images->img_head)) {
                unlink('resources/uploads/brideweekend/'.$images->img_head);
            }
            $file = $_FILES["img_head"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_head"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $img_head = $fileName;
        }
        if(!empty($_FILES["logo_enclosure"]["name"])) {
            if(!empty($images->logo_enclosure)) {
                unlink('resources/uploads/brideweekend/'.$images->logo_enclosure);
            }
            $file = $_FILES["logo_enclosure"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["logo_enclosure"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $logo_enclosure = $fileName;
        }
        $update = DB::table('brideweekend')->where('id', $request->input('id'))->update([
            'city' => $request->input('city'),
            'initial_date' => $request->input('initial_date'),
            'final_date' => $request->input('final_date'),
            'enclosure' => $request->input('enclosure'),
            'enclosure_description' => $request->input('description'),
            'enclosure_address' => $request->input('address'),
            'map' => $request->input('map'),
            'img_enclosure' => $img_enclosure,
            'logo_enclosure' => $logo_enclosure,
            'img_head' => $img_head,
            'organized_by' => $request->input('organized_by'),
            'entry_sat' => $request->input('entry_sat_start').'-'.$request->input('entry_sat_end'),
            'entry_sun' => $request->input('entry_sun_start').'-'.$request->input('entry_sun_end'),
            'runway_sat' => $request->input('runway_sat_start').'-'.$request->input('runway_sat_end'),
            'runway_sun' => $request->input('runway_sun_start').'-'.$request->input('runway_sun_end'),
            'active' => $request->input('group1'),
        ]);
        return redirect('admin/brideweekend');
    }

    function store_file($file, $extension, $folder) {
        $fileName = uniqid().".".$extension;
        file_put_contents("resources/$folder/$fileName", $file);
        return $fileName;
    }
}
