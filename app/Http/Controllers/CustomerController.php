<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Customer;
use App\CustomerPayment;
use App\Budget;
use App\BudgetDetail;

class CustomerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    public function saveBudget(Request $request) {
        $string = str_replace(' ', '', $request->input('budget'));
        $array = str_split($string);
        $pos = 0;
        $budgetValue = '';
        do {
            if($array[$pos]!='$' && $array[$pos]!=',') {
                $budgetValue = $budgetValue.''.$array[$pos];
            }
            $pos++;
        } while ($array[$pos]!='.');
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        $customer->budget = $budgetValue;
        $customer->number_guests = $request->input('quantity');
        $customer->save();
        $budget = Budget::where('user_id', Auth::user()->id)->get();
        foreach ($budget as $key => $value) {
            $update = DB::table('budgets')->where('id', $value->id)
            ->update([
                'budget' => intval($budgetValue*($value->percentage/100))
            ]);
        }
        return redirect('/home');
    }

    public function updateBudget(Request $request) {
        if($request->input('table')!='date_asigned' && $request->input('table')!='name' && $request->input('table')!='note') {
            $string = str_replace(' ', '', $request->input('value'));
            $array = str_split($string);
            $pos = 0;
            $budgetValue = '';
            do {
                if($array[$pos]!='$' && $array[$pos]!=',') {
                    $budgetValue = $budgetValue.''.$array[$pos];
                }
                $pos++;
            } while ($array[$pos]!='.');
        } else {
            $budgetValue = $request->input('value');
        }
        $budget = DB::table('budget_details')->where('id', $request->input('id'))
        ->update([
            $request->input('table') => $budgetValue,
        ]);
        return response()->json([
            'status' => 'success'
        ]);
    }

    public function addBudget(Request $request) {

        $save = BudgetDetail::create([
            'budget_id' => $request->id,
            //'category_id' => $request->category_id
        ]);

        return response()->json([
            'data' => $save
        ]);
    }

    public function extractPayment(Request $request) {
  

        $payments = DB::table('payments_customers')
        ->where('budget_detail_id', $request->id)
        //->where('budget_id', $budget->id)
        ->get();

        
        return response()->json([
            'data' => $payments
        ]);
    }

    public function addPayment(Request $request) {
        $data = DB::table('payments_customers')->insertGetId([
            'budget_detail_id' => $request->input('id'),
        ]);
      //  return $data;
        return response()->json([
            'data' => $data
        ]);
    }

    public function updatePayments(Request $request) {
        $budgetValue = '';
        if($request->input('row')=='amount') {
            $string = str_replace(' ', '', $request->input('value'));
            $array = str_split($string);
            $pos = 0;
            do {
                if($array[$pos]!='$' && $array[$pos]!=',') {
                    $budgetValue = $budgetValue.''.$array[$pos];
                }
                $pos++;
            } while ($array[$pos]!='.');
        } else {
            $budgetValue = $request->input('value');
        }
        $data = DB::table('payments_customers')
        ->where('id', $request->input('id'))
        ->update([
            $request->input('row') => $budgetValue
        ]);
        return response()->json([
            'status' => 'success'
        ]);
    }

    public function deletePayments(Request $request) {
        $delete = DB::table('payments_customers')->where('id', $request->input('id'))->delete();
        return response()->json([
            'id' => $request->input('id')
        ]);
    }
    public function deleteRow(Request $request){
        $id = $request->id;
        $row = Budget::find($id);
        $row->delete();
        return $id;

    }
    public function updateRowPayment(Request $request){
        $id = $request->id;
        $row = $request->row;
        $val = $request->val;

        DB::table('payments_customers')
        ->where('id', $id)
        ->update([ $row => $val]);
        return 'todo ok';

    }
}
