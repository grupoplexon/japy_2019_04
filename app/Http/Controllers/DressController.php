<?php

namespace App\Http\Controllers;
use DB;
use App\TypeTrend;
use App\DescriptorTrend;
use App\Trend;
use Illuminate\Http\Request;

class DressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trends = Trend::where('id_typeTrend', 1)->select('image', 'designer')->get()->groupBy('designer');
        $typeTrends = TypeTrend::all();
        return view('dress.index', compact('trends', 'typeTrends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function categoria(Request $request)
    {
        $categoria = $request->categoria;
        $trends = Trend::where('id_typeTrend', $categoria)->get()->toArray();
        $trendsByDesigner = Trend::where('id_typeTrend',$categoria)->select('image', 'designer')->get()->groupBy('designer');
        
        return [$categoria, $trends, $trendsByDesigner];
    }
    public function search(Request $request)
    {
        if ($request->categoria == 'designer') {
        $designer = $request->designer;
        $cut = $request->cut;
        $neckline = $request->neckline;
        $season = $request->season;
      //  $retVal = (condition) ? a : b ;
        $trends = DB::table('trends')
                            ->when($designer, function ($query, $designer) {
                                return $query->where('designer', $designer);
                            })
                            ->when($cut, function ($query, $cut) {
                                return $query->where('cut', $cut);
                            })
                            ->when($neckline, function ($query, $neckline) {
                                return $query->where('neckline', $neckline);
                            })
                            ->when($season, function ($query, $season) {
                                return $query->where('season', $season);
                            })
                            ->select('image', 'designer')
                            ->get()
                            ->groupBy('designer')
                            ->toArray();
        return $trends;
    }
    }
}
