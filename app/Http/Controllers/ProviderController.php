<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Provider;
use App\Location_provider;
use App\Gallery;
use App\Promotion;
use App\Verification;
use App\Indicator;
use Carbon\Carbon;
use App\Budget;
use App\Customer;
use App\CustomerPayment;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->user()->authorizeRoles('proveedor');
        $this->middleware('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->homeByRole();
    }

    public function homeByRole()
    {
        if (Auth::user()->hasRole('admin'))
        {
            $notification = Verification::where('status', 1)->get()->count();
            return view('admin.index')->with(['notification' => $notification]);
        }
        else if (Auth::user()->hasRole('proveedor'))
        {
            $startDate = Carbon::now()->subDays(7)->format('Y-m-d');
            $endDate = Carbon::now()->format('Y-m-d');
            // dd($startDate.'/'.$endDate);
            $visitSeven = Indicator::where('user_id', Auth::user()->id)
            ->groupBy('type')
            ->select(DB::raw("SUM(quantity) as quantity"))
            ->whereBetween('created_at', array($startDate, $endDate))
            ->where('type', 'visit_profile')
            ->orderBy('type')
            ->first();
            $searchSeven = Indicator::where('user_id', Auth::user()->id)
            ->groupBy('type')
            ->select(DB::raw("SUM(quantity) as quantity"))
            ->whereBetween('created_at', array($startDate, $endDate))
            ->where('type', 'search_profile')
            ->orderBy('type')
            ->first();

            $year = Carbon::now()->format('Y');
            for ($i=1; $i < 13; $i++) { 
                if($i<10) {
                    $date = $year.'-0'.$i;
                } else {
                    $date = $year.'-'.$i;
                }
                $utlimo_dia = $this->getUltimoDiaMes(Carbon::now()->format('Y'), $i);
                $startDate = $date.'-01';
                $endDate = $date.'-'.$utlimo_dia;
                $visitYear = Indicator::where('user_id', Auth::user()->id)
                ->groupBy('type')
                ->select('type', DB::raw("SUM(quantity) as quantity"))
                ->whereBetween('created_at', array($startDate, $endDate))
                ->where('type', 'visit_profile')
                ->first();
                $searchYear = Indicator::where('user_id', Auth::user()->id)
                ->groupBy('type')
                ->select('type', DB::raw("SUM(quantity) as quantity"))
                ->whereBetween('created_at', array($startDate, $endDate))
                ->where('type', 'search_profile')
                ->first();
                if(!empty($visitYear)) {
                    $data[$i-1] = intval($visitYear->quantity);
                } else {
                    $data[$i-1] = 0;
                }
                if(!empty($searchYear)) {
                    $datatwo[$i-1] = intval($searchYear->quantity);
                } else {
                    $datatwo[$i-1] = 0;
                }
            }
            $chart1 = \Chart::title([
                'text' => 'Visitas y búsquedas de tu Escaparate',
            ])
            ->chart([
                'type'     => 'line', // pie , columnt ect
                'renderTo' => 'chart1', // render the chart into your div with id
            ])
            ->credits([
                'enabled' => false
            ])
            ->subtitle([
                'text' => 'Año actual',
            ])
            ->colors([
                // '#039be5'
            ])
            ->xaxis([
                'categories' => [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre',
                ],
                'labels'     => [
                    'rotation'  => 0,
                    'align'     => 'top',
                    // 'formatter' => 'startJs:function(){return this.value + " (Footbal Player)"}:endJs', 
                    // use 'startJs:yourjavasscripthere:endJs'
                ],
            ])
            ->yaxis([
                'title' => [
                    'text' => 'Cantidad'
                ]
            ])
            ->legend([
                'layout'        => 'vertikal',
                'align'         => 'right',
                'verticalAlign' => 'middle',
            ])
            ->series(
                [
                    [
                        'name'  => 'Visitas',
                        'data'  => $data,
                        // 'color' => '#0c2959',
                    ],
                    [
                        'name'  => 'Búsquedas',
                        'data'  => $datatwo,
                        // 'color' => '#0c2959',
                    ],
                ]
            )
            ->display();
            $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
            // dd($data);

            // $chart1 = \Chart::title([
            //     'text' => 'Visitas y búsquedas de tu Escaparate',
            // ])
            // ->chart([
            //     'type'     => 'solidgauge', // pie , columnt ect
            //     'renderTo' => 'chart1', // render the chart into your div with id
            // ])
            // ->credits([
            //     'enabled' => false
            // ])
            // ->tooltip([
            //     'borderWidth' => 0,
            //     'backgroundColor' => 'none',
            //     'shadow' => false,
            //     'style' => [
            //         'fontSize' => '16px',
            //         'opacity' => 1,
            //     ],
            //     'hideDelay' => 60000,
            //     'pointFormat' => '<span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
            //     // 'positioner' => function (labelWidth, labelHeight) {
            //     //     return {
            //     //         x: 100 - labelWidth / 2,
            //     //         y: 110
            //     //     };
            //     // }
            // ])
            // ->yAxis([
            //     'min' => 0,
            //     'max' => 100,
            //     'lineWidth' => 0,
            //     'tickPositions' => []
            // ])
            // ->plotOptions([
            //     'solidgauge' => [
            //         'borderWidth' => '10px',
            //         'dataLabels' => [
            //             'enabled' => false
            //         ],
            //         'linecap' => 'round',
            //         'stickyTracking' => false
            //     ],
            //     // 'series' => [
            //     //     'events' => [
            //     //         show => function () {
            //     //         }
            //     //     ]
            //     // ]
            // ])
            // ->series([
            //     'name' => '',
            //     'borderColor' => 'green',
            //     'data' => [
            //         'color' => 'green',
            //         'radius' => '100%',
            //         'innerRadius' => '100%',
            //         'y' => 10
            //     ],
            // ])->display();

            return view('provider.index')->with([
                'visitSeven' => $visitSeven, 
                'searchSeven' => $searchSeven, 
                'chart1' => $chart1,
                'logo' => $gallery
            ]);
        }
        else if (Auth::user()->hasRole('usuario'))
        {
           // $data = Budget::where('user_id', Auth::user()->id)->get();
            $data = DB::table('budgets')
            ->join('categories', 'categories.id', '=', 'budgets.category_id')
            ->where('budgets.user_id', Auth::user()->id)
            ->get();

         

            $customer = Customer::where('user_id', Auth::user()->id)->first();
            foreach ($data as $key => $value) {
                $string = htmlentities($value->name_category);
                $string = preg_replace('/\&(.)[^;]*;/', '\\1', $string);
                $string = str_replace(' ', '-', strtolower($string));
                $string = str_replace('/', '-', strtolower($string));
                $value->img = $string.'.png';
            }
        
          //  $totalPayment = CustomerPayment::where('user_id', Auth::user()->id)->sum('amount');
            $totalPayment = 0;
            $totalCost = 0;
           // $totalCost = Budget::where('user_id', Auth::user()->id)->sum('final_cost');
            $pending = $totalCost - $totalPayment ;
        
            return view('Customer.index')->with([
                'customer' => $customer, 
                'budgets' => $data,
                'totalPayment' => $totalPayment,
                'totalCost' => $totalCost,
                 'pending' => $pending
                ]);
        }
    }

    public function porcentaje() {
        $chart2 = \Chart::title([
            'text' => 'Visitas y búsquedas de tu Escaparate',
        ])
        ->chart([
            'type'     => 'solidgauge', // pie , columnt ect
            'renderTo' => 'chart1', // render the chart into your div with id
        ])
        ->credits([
            'enabled' => false
        ])
        ->tooltip([
            'borderWidth' => 0,
            'backgroundColor' => 'none',
            'shadow' => false,
            'style' => [
                'fontSize' => '16px',
                'opacity' => 1,
            ],
            'hideDelay' => 60000,
            'pointFormat' => '<span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
            // 'positioner' => function (labelWidth, labelHeight) {
            //     return {
            //         x: 100 - labelWidth / 2,
            //         y: 110
            //     };
            // }
        ])
        ->yAxis([
            'min' => 0,
            'max' => 100,
            'lineWidth' => 0,
            'tickPositions' => []
        ])
        ->plotOptions([
            'solidgauge' => [
                'borderWidth' => '10px',
                'dataLabels' => [
                    'enabled' => false
                ],
                'linecap' => 'round',
                'stickyTracking' => false
            ],
            // 'series' => [
            //     'events' => [
            //         show => function () {
            //         }
            //     ]
            // ]
        ])
        ->series([
            'name' => '',
            'borderColor' => 'green',
            'data' => [
                'color' => 'green',
                'radius' => '100%',
                'innerRadius' => '100%',
                'y' => 10
            ],
        ])->display();

        return $chart2;
    }

    public function getUltimoDiaMes($year, $month) {
        return date("d", (mktime(0,0,0,$month+1,1,$year)-1));
    }

    public function escaparate() {
        $provider = DB::table('users as a')
        ->join('providers as b', 'a.id', '=', 'b.user_id')
        ->where('a.id', Auth::user()->id)
        ->select(
            'a.*',
            'b.description',
            'b.category_id',
            'b.slug',
            'b.price_min',
            'b.price_max'
        )
        ->first();
        $info = Location_provider::where('user_id', Auth::user()->id)->get();
        $categories = DB::table('categories')->get();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('Provider.escaparate')->with([
            'data' => $provider, 
            'category' => $categories, 
            'info' => $info, 
            'slug' => $provider,
            'logo' => $gallery
        ]);
    }

    public function checkPassword(Request $request) {
        $validate = DB::table('users')->where('id', $request->input('idUser'))->first();
        if (Hash::check($request->input('password'), $validate->password)) {
            return response()->json([
                'status' => 'coincide'
            ]);
        } else {
            return response()->json([
                'status' => 'no_coincide'
            ]);
        }
    }

    public function updateCredentials(Request $request) {
        $user = User::where('id', Auth::user()->id)->first();
        if(!empty($request->input('password'))) {
            $user->user = $request->input('user');
            $user->password = bcrypt($request->input('password'));
        } else {
            $user->user = $request->input('user');
        }
        if($user->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateContact(Request $request) {
        $info = Location_provider::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $info->name_contact = $request->input('name');
        $info->phone = $request->input('phone');
        $info->cellphone = $request->input('cellphone');
        $info->email = $request->input('email');
        $info->website = $request->input('website');
        if($info->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateCategory(Request $request) {
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $provider->category_id = $request->input('category');
        if($provider->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateCompany(Request $request) {
        $user = User::where('id', Auth::user()->id)->first();
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $description_new = str_replace("\n", "", $request->input('description'));
        $description_new = str_replace("\r", "", $description_new);
        $description_old = str_replace("\n", "", $provider->description);
        $description_old = str_replace("\r", "", $description_old);
        $saved_name = 0;
        $saved_description = 0;
        if($request->input('name_company')!=$user->name) {
            $verif = DB::table('verifications')->where('user_id', Auth::user()->id)->where('type', 'name')->first();
            if($verif) {
                $update = DB::table('verifications')
                ->where('user_id', Auth::user()->id)
                ->where('type', 'name')
                ->update([
                    'data' => $request->input('name_company'),
                    'status' => 1
                ]);
            } else {
                $save = DB::table('verifications')->insert([
                    'user_id' => Auth::user()->id,
                    'type' => 'name',
                    'data' => $request->input('name_company'),
                    'status' => 1
                ]);
            }
            $saved_name = 1;
        }
        if($description_new!=$description_old) {
            $verif = DB::table('verifications')->where('user_id', Auth::user()->id)->where('type', 'description')->first();
            if($verif) {
                $update = DB::table('verifications')
                ->where('user_id', Auth::user()->id)
                ->where('type', 'description')
                ->update([
                    'data' => $request->input('description'),
                    'status' => 1
                ]);
            } else {
                $save = DB::table('verifications')->insert([
                    'user_id' => Auth::user()->id,
                    'type' => 'description',
                    'data' => $request->input('description'),
                    'status' => 1
                ]);
            }
            $saved_description = 1;
        }
        if($saved_name==0 && $saved_description==0) {
            return response()->json([
                'status' => 'no_saved'
            ]);
        } else {
            return response()->json([
                'status' => 'saved'
            ]);
        }
    }

    public function localizacion() {
        $provider = DB::table('users as a')
        ->join('location_providers as b', 'a.id', '=', 'b.user_id')
        ->where('a.id', Auth::user()->id)
        ->select(
            'a.*', 
            'b.id as id_loc', 
            'b.country', 
            'b.state', 
            'b.city', 
            'b.address', 
            'b.postal_code', 
            'b.latitude', 
            'b.longitude'
        )
        ->get();
        $country = DB::table('countries')->get();
        $slug = Provider::where('user_id', Auth::user()->id)->first();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('Provider.localizacion')->with([
            'data' => $provider, 
            'countries' => $country, 
            'slug' => $slug,
            'logo' => $gallery
        ]);
    }

    public function searchCities(Request $request) {
        $state = DB::table('states')->where('state', $request->input('state'))->first();
        $cities = DB::table('cities')->where('state_id', $state->id)->get();
        return response()->json([
            'cities' => $cities
        ]);
    }

    public function updateAddress(Request $request) {
        $update = Location_provider::where('id', $request->input('id'))->first();
        $update->city = $request->input('city');
        $update->postal_code = $request->input('postal_code');
        $update->address = $request->input('address');
        $update->latitude = $request->input('lat');
        $update->longitude = $request->input('lng');
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function fotos() {
        $images = Gallery::where('user_id', Auth::user()->id)->where('type', 'image')->get();
        $slug = Provider::where('user_id', Auth::user()->id)->first();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('Provider.fotos')->with([
            'images' => $images, 
            'slug' => $slug,
            'logo' => $gallery
        ]);
    }

    public function uploadImages(Request $request) {
        $destination = 'resources/uploads/providers';
        $quality     = 20;
        $pngQuality  = 9;
        $file = $_FILES["files"];
        $data     = file_get_contents($file["tmp_name"]);
        $extension = pathinfo($_FILES["files"]["name"])["extension"];
        $fileName  = $this->store_file($data, $extension, "uploads/providers");
        $new_name_image = $file = $destination."/".$fileName;
        // $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
        $image = Gallery::create([
            'user_id' => Auth::user()->id,
            'name_image' => $fileName,
            'type' => 'image'
        ]);
        $count = Gallery::where('user_id', Auth::user()->id)->where('type', 'image')->get()->count();
        if($count>7) {
            $update = Verification::where('user_id', Auth::user()->id)->where('type', 'images')->first();
            if(empty($update)) {
                $verification = DB::table('verifications')->insert([
                    'user_id' => Auth::user()->id,
                    'type' => 'images',
                    'data' => $count,
                    'status' => 1,
                ]);
            } else {
                if($update->status==1) {
                    $verification = DB::table('verifications')
                    ->where('user_id', Auth::user()->id)
                    ->where('type', 'images')
                    ->update([
                        'data' => $count,
                        'status' => 1
                    ]);
                }
            }
        }
        return response()->json([
            'status' => $fileName,
            'data' => $image
        ]);
    }

    function store_file($file, $extension, $folder) {
        $fileName = uniqid().".".$extension;
        file_put_contents("resources/$folder/$fileName", $file);

        return $fileName;
    }

    public function deleteImage(Request $request) {
        $image = Gallery::where('id', $request->input('id'))->where('user_id', Auth::user()->id)->first();
        unlink('resources/uploads/providers/'.$image->name_image);
        $image->delete();
        return response()->json([
            'status' => 'deleted'
        ]);
    }

    public function updatePhoto(Request $request) {
        $profile = DB::table('gallery')
        ->where('user_id', Auth::user()->id)
        ->update(['profile' => null]);
        $update = Gallery::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $update->profile = 1;
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updateLogo(Request $request) {
        $profile = DB::table('gallery')
        ->where('user_id', Auth::user()->id)
        ->update(['logo' => null]);
        $update = Gallery::where('user_id', Auth::user()->id)->where('id', $request->input('id'))->first();
        $update->logo = 1;
        if($update->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function videos() {
        $video = Gallery::where('type', 'video')->where('user_id', Auth::user()->id)->get();
        $slug = Provider::where('user_id', Auth::user()->id)->first();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('Provider.videos')->with([
            'video' => $video, 
            'slug' => $slug,
            'logo' => $gallery
        ]);
    }

    public function uploadVideo(Request $request) {
        $exist = Gallery::where('type', 'video')->where('user_id', Auth::user()->id)->first();
        if(empty($exist)) {
            $extension = substr($_FILES['archivo']['type'], -3);
            $nombre_temporal = $_FILES['archivo']['tmp_name'];
            // $nombre = $_FILES['archivo']['name'];
            $nombre = uniqid().'.'.$extension;
            $video = Gallery::create([
                'user_id' => Auth::user()->id,
                'name_image' => $nombre,
                'type' => 'video'
            ]);
            move_uploaded_file($nombre_temporal, 'resources/uploads/providers/'.$nombre);
            return response($video);
        } else {
            return 'exist';
        }
    }

    public function checkVideo() {
        $check = Gallery::where('user_id', Auth::user()->id)->where('type', 'video')->first();
        if(!empty($check)) {
            return response()->json([
                'status' => 'no_more'
            ]);
        } else {
            return response()->json([
                'status' => 'ok'
            ]);
        }
    }

    public function deleteVideo(Request $request) {
        $delete = Gallery::where('id', $request->input('id'))->where('user_id', Auth::user()->id)->first();
        unlink('resources/uploads/providers/'.$delete->name_image);
        if($delete->delete()) {
            return response()->json([
                'status' => 'deleted'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function promociones() {
        $data = Promotion::where('user_id', Auth::user()->id)->get();
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('Provider.promociones')->with([
            'promotions' => $data, 
            'provider' => $provider, 
            'slug' => $provider,
            'logo' => $gallery
        ]);
    }

    public function saveDiscount(Request $request) {
        $discount = DB::table('providers')
        ->where('user_id', Auth::user()->id)
        ->update(['discount' => $request->input('discount')]);
        return response()->json([
            'status' => 'saved'
        ]);
    }

    public function savePromotion(Request $request) {
        date_default_timezone_set("America/Mexico_City");
        $destination = 'resources/uploads/promotions';
        if($request->input('operation')=='save') {
            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/promotions");
            $new_name_image = $file = $destination."/".$fileName;

            $save = Promotion::create([
                'user_id' => Auth::user()->id,
                'type' => $request->input('type'),
                'name_promotion' => $request->input('name'),
                'image_promotion' => $fileName,
                'price_original' => $request->input('price'),
                'price_secondary' => $request->input('price2'),
                'description_promotion' => $request->input('description'),
                'date_initial' => date('y-m-d'),
                'date_final' => $request->input('date'),
                'status' => 1
            ]);
        } else {
            $promotion = Promotion::where('id', $request->input('id'))->first();
            unlink('resources/uploads/promotions/'.$promotion->image_promotion);

            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = $this->store_file($data, $extension, "uploads/promotions");
            $new_name_image = $file = $destination."/".$fileName;

            $promotion->type = $request->input('type');
            $promotion->name_promotion = $request->input('name');
            $promotion->image_promotion = $fileName;
            $promotion->price_original = $request->input('price');
            $promotion->price_secondary = $request->input('price2');
            $promotion->description_promotion = $request->input('description');
            $promotion->date_final = $request->input('date');
            $promotion->save();
        }
    }

    public function updatePromotion(Request $request) {
        $promotion = Promotion::where('id', $request->input('id'))->first();
        $promotion->type = $request->input('type');
        $promotion->name_promotion = $request->input('name');
        $promotion->price_original = $request->input('price');
        $promotion->price_secondary = $request->input('price2');
        $promotion->description_promotion = $request->input('description');
        $promotion->date_final = $request->input('date');
        if($promotion->save()) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function updatePrices(Request $request) {
        $prices = DB::table('verifications')->where('user_id', Auth::user()->id)->where('type', 'prices')->first();
        if(empty($prices)) {
            $price = DB::table('verifications')->insert([
                'user_id' => Auth::user()->id,
                'type' => 'prices',
                'data' => $request->input('priceMin').' - '.$request->input('priceMax'),
                'status' => 1
            ]);
        } else {
            $price = DB::table('verifications')
            ->where('user_id', Auth::user()->id)
            ->where('type', 'prices')
            ->update([
                'data' => $request->input('priceMin').' - '.$request->input('priceMax'),
                'status' => 1
            ]);
        }
        if($price || $price>0) {
            return response()->json([
                'status' => 'saved'
            ]);
        } else {
            return response()->json([
                'status' => 'error'
            ]);
        }
    }

    public function solicitudes() {
        $provider = Provider::where('user_id', Auth::user()->id)->first();
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('provider.solicitudes')->with(['slug' => $provider, 'logo' => $gallery]);
    }

    public function recomendaciones() {
        $data = DB::table('reviews')
        ->where('provider_id', Auth::user()->id)
        ->where('active', 1)
        ->get();
        setlocale(LC_ALL,"es_ES");
        $gallery = Gallery::where('user_id', Auth::user()->id)->where('logo', 1)->first();
        return view('provider.recomendaciones')->with(['reviews' => $data, 'logo' => $gallery]);
    }
}
