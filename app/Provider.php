<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'user_id', 'category_id', 'slug', 'description', 'discount', 'account_type', 'expiration', 'average', 'price_min', 'price_max', 
    ];
}
