<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{
    protected $table='role_user';

    public function rol() {
        return $this->hasOne(Role::class, 'id'); //role_user_id
    }
}
