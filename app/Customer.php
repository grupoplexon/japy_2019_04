<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'user_id', 'email', 'phone', 'gender', 'date_wedding', 'budget', 'number_guests', 
    ];
}
