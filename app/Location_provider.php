<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location_provider extends Model
{
    protected $fillable = [
        'user_id', 'active', 'country', 'state', 'city', 'address', 'postal_code', 'latitude', 'logitude', 'name_contact', 'phone', 'cellphone', 'email','website' 
    ];
}
