<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector_activity extends Model
{
    protected $table='Sector_activity';

    public function extractCategory() {
        return $this->hasMany(Category::class);
    }
}
