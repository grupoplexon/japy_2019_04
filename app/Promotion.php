<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'user_id', 'type', 'name_promotion', 'image_promotion', 'price_original', 'price_secondary', 'downloads', 'description_promotion', 'date_initial', 'date_final', 'status'
    ];
}
