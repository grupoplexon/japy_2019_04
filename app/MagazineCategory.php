<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MagazineCategory extends Model
{
    protected $table='magazine_category';
}
