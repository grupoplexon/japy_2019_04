<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $fillable = [
        'user_id', 'category_id', 'name', 'indicator', 'percentage', 'budget', 'final_cost', 'note', 'date_asigned', 'status', 
    ];
}
