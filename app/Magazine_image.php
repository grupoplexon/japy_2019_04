<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine_image extends Model
{
    protected $table='magazine_image';

    protected $fillable = [
        'id_image', 'image', 'id_article',
    ];
}
