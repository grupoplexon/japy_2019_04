<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $table='magazine';

    protected $fillable = [
        'title', 'slug', 'content', 'main_image', 'publish', 'type', 
    ];
}
