<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DescriptorTrend extends Model
{
    protected $table='descriptor_trend';

    protected $fillable = [
        'name', 'grupo', 
    ];

    public function type_trends()
    {
        return $this->belongsToMany("App\TypeTrend", "descriptor_type_trend", "id_descriptor" , "id_typeTrend");
    }
}
