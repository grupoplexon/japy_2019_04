<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $fillable = [
        'user_id', 'type', 'quantity', 'created_at', 
    ];
}
