<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='Gallery';
    protected $fillable = [
        'user_id', 'name_image', 'type', 'profile', 'logo', 
    ];
}
