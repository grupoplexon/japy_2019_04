<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function authorizeRoles($roles){
        if($this->hasAnyRole($roles)){
            return true;
        }
        abort(401, 'This action is unauthorized');
    }

    public function hasAnyRole($roles){
        if(is_array($roles)){
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }

        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){
        if($this->roles()->where('name',$role)->first()){
            return true;
        }
        return false;
    }

    public function location_provider(){
        return $this->hasMany(Location_provider::class);
    }

    public function role_user(){
        return $this->hasOne(Role_user::class);
    }
    public function provider() {
        return $this->hasOne(Provider::class);
    }
    public function verification() {
        return $this->hasMany(Verification::class);
    }
    public function gallery() {
        return $this->hasMany(Gallery::class)->orderBy('type', 'DESC');
    }
    public function review() {
        return $this->hasMany(Review::class, 'provider_id');
    }
    public function indicator() {
        return $this->hasMany(Indicator::class);
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password', 'user', 'active', 'registered_from', 'token_facebook', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
