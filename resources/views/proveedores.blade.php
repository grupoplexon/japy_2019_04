<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>Proveedores - BrideAdvisor</title>
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ URL::asset('css/lightslider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/proveedores.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/stars.css') }}">
</head>
<body>
    @include('menu')
    <form action="{{route('proveedores/filtrados')}}" method="POST" id="filterProvider">
    {{ csrf_field() }}
    <div class="row background-search">
        <div class="col l8 s12 offset-l2 padding-search">
            <div class="col l1 margins right-align hide-on-small-only">
                <p class="btn searchProvider" id="search"> <i class="fas fa-search"></i></p>
            </div>
            <div class="col s7 l8 margins">
                <input id="btn-search" type="text" placeholder="¿Qué estas buscando?" autofocus>
                <input id="URL" type="hidden" value="{{URL::asset('')}}">
            </div>
            <div class="col s5 l3 margins">
                <select name="estado"  class="browser-default  col l4" id="stateId">
                    <option id="Todos" value="" @if($filtros['estado']==null) selected @endif>Elige un estado</option>
                    <option id="Jalisco" value="Jalisco" @if($filtros['estado']=='Jalisco') selected @endif>Jalisco</option>
                    <option id="Puebla" value="Puebla" @if($filtros['estado']=='Puebla') selected @endif>Puebla</option>
                    <option id="Queretaro" value="Queretaro" @if($filtros['estado']=='Queretaro') selected @endif>Querétaro</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col l12 m12 s12">
            <div class="col l3">
                <div class="card center-align row">
                    <div class="col l10 offset-l1">
                        <p><h5>Filtrar por</h5></p>
                        <p>
                            <input type="hidden" id="valueState" value="<?= $filtros['estado'] ?>">
                            <input type="hidden" id="valueCategory" value="<?= $filtros['categoria'] ?>">
                            <input type="hidden" id="valueAverage" value="<?= $filtros['promedio'] ?>">
                            <input type="hidden" id="valuePriceMin" value="<?= $filtros['precioMin'] ?>">
                            <input type="hidden" id="valuePriceMax" value="<?= $filtros['precioMax'] ?>">
                            <h6>CATEGORÍA</h6>
                            <select class="inputs large" name="categories" id="categories">
                                <option value="" selected disabled>Selecciona una categoría</option>
                                @foreach ($categories as $val)
                                    @if($filtros['categoria'] == $val->id)
                                        <option value="{{$val->id}}" selected>{{$val->name_category}}</option>
                                    @else
                                        <option value="{{$val->id}}">{{$val->name_category}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </p>
                        <div class="col l12 m12 s12 margins">
                            <h6>RANGO DE PRECIOS</h6>
                            <div class="col l6">
                                <select class="inputs large" name="priceMin" id="priceMin">
                                    <option value="" @if($filtros['precioMin']==null) selected @endif>Min.</option>
                                    <option value="10000" @if($filtros['precioMin']==10000) selected @endif>$10,000.00</option>
                                    <option value="20000" @if($filtros['precioMin']==20000) selected @endif>$20,000.00</option>
                                    <option value="30000" @if($filtros['precioMin']==30000) selected @endif>$30,000.00</option>
                                    <option value="40000" @if($filtros['precioMin']==40000) selected @endif>$40,000.00</option>
                                    <option value="50000" @if($filtros['precioMin']==50000) selected @endif>$50,000.00</option>
                                </select>
                            </div>
                            <div class="col l6">
                                <select class="inputs large" name="priceMax" id="priceMax">
                                    <option value="" @if($filtros['precioMax']==null) selected @endif>Max.</option>
                                    <option value="50000" @if($filtros['precioMax']==50000) selected @endif>$50,000.00</option>
                                    <option value="60000" @if($filtros['precioMax']==60000) selected @endif>$60,000.00</option>
                                    <option value="70000" @if($filtros['precioMax']==70000) selected @endif>$70,000.00</option>
                                    <option value="80000" @if($filtros['precioMax']==80000) selected @endif>$80,000.00</option>
                                    <option value="90000" @if($filtros['precioMax']==90000) selected @endif>$90,000.00</option>
                                </select>
                            </div>
                            <br><br><br>
                        </div>
                        <div class="col l12 m12 s12 center-align">
                            <h6>CALIFICACIÓN</h6>
                            <fieldset class="col l12 m12 s12 rating center-align" style="width: 100% !important;">
                                <input type="radio" id="star5" name="rating" class="rating" value="5" required @if($filtros['promedio']==5) checked @endif/><label class = "full" for="star5" title="5 stars"></label>

                                <input type="radio" id="star4" name="rating" class="rating" value="4" @if($filtros['promedio']==4) checked @endif/><label class = "full" for="star4" title="4 stars"></label>

                                <input type="radio" id="star3" name="rating" class="rating" value="3" @if($filtros['promedio']==3) checked @endif/><label class = "full" for="star3" title="3 stars"></label>

                                <input type="radio" id="star2" name="rating" class="rating" value="2" @if($filtros['promedio']==2) checked @endif/><label class = "full" for="star2" title="2 stars"></label>

                                <input type="radio" id="star1" name="rating" class="rating" value="1" @if($filtros['promedio']==1) checked @endif/><label class = "full" for="star1" title="1 star"></label>
                            </fieldset>
                            <br><br>
                            <p class="btn pink lighten-5 text-content" id="filterButton">Aplicar</p>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l9">
                <div class="col l12 m12 s12" id="areaFiltros">
                    @if($filtros['estado']!=null)
                        <div class="chip">
                            {{$filtros['estado']}}
                            <i class="close material-icons" id="removeState">close</i>
                        </div>
                    @endif
                    @if($filtros['categoria']!=null)
                        <div class="chip">
                            {{$filtros['nombreCategoria']}}
                            <i class="close material-icons" id="removeCategory">close</i>
                        </div>
                    @endif
                    @if($filtros['promedio']!=null)
                        <div class="chip">
                            {{$filtros['promedio']}} estrellas
                            <i class="close material-icons" id="removeAverage">close</i>
                        </div>
                    @endif
                    @if($filtros['precioMin']!=null && $filtros['precioMax']!=null)
                        <div class="chip">
                            ${{$filtros['precioMin']}}.00 - ${{$filtros['precioMax']}}.00
                            <i class="close material-icons" id="removePrices">close</i>
                        </div>
                    @endif
                    @if($filtros['proveedor']!=null)
                        <div class="chip">
                            {{$providers[0]->name}}
                            <input type="hidden" id="searchForProvider" value="{{$providers[0]->name}}">
                            <i class="close material-icons" id="removeProvider">close</i>
                        </div>
                    @endif
                </div>
                @if(sizeof($providers)>0)
                    @foreach ($providers as $val)
                        <div class="col l4">
                            <div class="card center-align height-card">
                                @foreach ($val->gallery as $value)
                                    @if($value->profile==1)
                                        <img class="img-content profile-provider" src="{{URL::asset('resources/uploads/providers/'.$value->name_image.'')}}">
                                    @endif
                                @endforeach
                                <h5>{{$val->name}}</h5>
                                <p class="center-align">Recomendaciones</p>
                                @for ($i = 1; $i < 6; $i++)
                                    @if($i<=$val->provider->average)
                                        <i class="fa fa-star" style="color:#f9a825;"></i>
                                    @else
                                        <i class="far fa-star" style="color:#f9a825;"></i>
                                    @endif
                                @endfor
                                <p class="margin-description center-align"><?= substr($val->provider->description,0,110) ?>...</p>
                                <p class="btn pink lighten-5 text-content view-more" id="{{$val->provider->slug}}">VER MÁS</p><br><br>
                            </div>
                        </div>
                    @endforeach
                    <div class="col l12 m12 s12 center-align">
                        {{$providers->render()}}
                    </div>
                @else
                    <div class="col l12 m12 s12 center-align">
                        <br><br><br>
                        <h5>No se encontraron resultados.</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
    </form>
    @include('footer')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ URL::asset('js/lightslider.js') }}"></script>
    <script src="{{ URL::asset('js/proveedores.js') }}"></script>
</body>
</html>