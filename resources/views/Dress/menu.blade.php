<style>
        select {
            background: white !important;
            border-radius: 10px !important;
            padding-left: 10px !important;
            opacity: inherit !important;
            /*width: 100% !important;*/
            position: relative !important;
            height: 50px !important;
            pointer-events: all !important;
            border: 0px #757575 solid !important;
            box-shadow: 0px 0px 3px 0px;
            margin-bottom: 10px;
        }
        
    </style>
    <div class="row">
       <div class="col l3 s12">
        <select id="categoria" class="select browser-default">
          <option value="" disabled selected>Categoria</option>
          @foreach ($typeTrends as $typeTrend)
          <option value="{{$typeTrend->id}}"> {{$typeTrend->name}}</option>
        @endforeach
        </select>
      </div>
      <div id="trend"> </div>
        <div id="data"> </div>
     
    </div> 
 