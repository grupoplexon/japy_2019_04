<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor</title>
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
   <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->

    <link rel="stylesheet" href="{{ URL::asset('resources/slick-1.8.1/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('resources/slick-1.8.1/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/swiper.css') }}">

</head>
<body>
    @include('menu')
    <div class="row background">
        <div class="row container">
            <img style="width:100%;" src="{{URL::asset('/img/TENDENCIA.png')}}">
        </div>
        <div class="row container">
                @include('Dress.menu')
        </div>
    </div>
    <div class="row container vestidos-novia">


     <!-- Swiper -->
  @foreach ($trends as $designer => $trend)
  <h4>{{$designer}}</h4>
  <div class="responsive">
    @foreach ($trend as $image=>$data)

    <div class="comandSlider__item"><img src="{{ URL::asset('resources/uploads/trends/' .$data->image)}}" style="width:100%; height:100%;" alt=""></div>   
    @endforeach
  </div>
  @endforeach
</div>

  <div class="row container">
    <span class="trendsByCategory"> 
      
    </span>
    <span class="trendsByCategorySwiper"></span>
    </div>
  <!-- Swiper JS -->

  <div>
    <br><br><br>
    @include('footer')
  
 
</body>
<script type="text/javascript" src="{{ URL::asset('resources/slick-1.8.1/slick/slick.min.js')}}"></script>
<script src="{{ URL::asset('js/tendencia.js') }}"></script>

<script src="{{ URL::asset('js/swiper.js') }}"></script>


</html>