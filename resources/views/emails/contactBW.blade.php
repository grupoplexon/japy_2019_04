<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BrideWeekend</title>
</head>
<body>
    <p>El usuario: <b>{{$name}}</b></p>
    <p>Con correo: <b>{{$email}}</b></p>
    <p>Teléfono: <b>{{$phone}}</b></p>
    <p>Te envio el siguiente mensaje: <b>{{$msg}}</b></p>
</body>
</html>