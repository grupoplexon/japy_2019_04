<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Admin.header')
    <title>TENDENCIAS</title>
    <link rel="stylesheet" type="text/css" href="dataTables2/datatables.min.css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row ">
                {{-- <nav> --}}
                    <div class="nav-wrapper right">
                        <div class="col s12">
                        <a href="{{ route('home') }}" class="breadcrumb">Home</a>
                        <a href="{{ route('trends') }}" class="breadcrumb blue-text text-darken-2">Tendencias</a>
                        </div>
                    </div>
                {{-- </nav> --}}
            </div>
            <div class="row">
                <a href="{{ route('newTrend') }}" class="btn"> NUEVA</a>
              <div class="card-panel">
                  <div class="row center-align">
                    <h5>TENDENCIAS</h5>
                  </div>
                  <div class="row">
                    <table class="table table-striped table-bordered" id="magazineTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>NOMBRE</th>
                            <th>DISEÑADOR</th>
                            <th>TEMPORADA</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($trends as $t) : ?>
                                <tr>
                                    <th> <?php echo $t->id_trend ?></th>
                                    <th> <?php echo $t->name ?></th>
                                    <th> <?php echo $t->season ?></th>
                                    <th> <?php echo $t->designer ?></th>
                                    <th> 
                                        <a class="btn blue" href="{{ url('/editTrend', ['id' => $t->id_trend ]) }}">Editar</a>
                                        <button class="btn red" onclick="deleteTrend(<?php echo $t->id_trend ?>)">Eliminar</button>
                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <input type="hidden" name="" id="url" value="{{URL::asset('')}}">
                  </div>
              </div>
            </div>
        </div>
    </div>
</body>
@include('Admin.footer')
<script type="text/javascript" src="dataTables2/datatables.min.js"></script>
<script src="{{ URL::asset('js/admin/trends/trend.js') }}"></script>
</html>