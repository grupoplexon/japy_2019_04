@include('Admin.header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{ URL::asset('css/admin/promociones.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row ">
                {{-- <nav> --}}
                    <div class="nav-wrapper right">
                        <div class="col s12">
                            <a href="{{ route('home') }}" class="breadcrumb">Home</a>
                            <a href="{{ route('trends') }}" class="breadcrumb">Tendencias</a>
                            <a href="#" class="breadcrumb blue-text text-darken-2">Tendencia Nueva</a>
                        </div>
                    </div>
                {{-- </nav> --}}
            </div>
            <div class="row">
                <h6>Nueva tendencia</h6>
                <div class="card-panel row"> 
                    {{-- <form action="{{ route('saveArticle') }}" method="POST"  enctype="multipart/form-data" > 
                        {{ csrf_field() }} --}}
                        <table class="table centered" id="trendTable">
                            <tr>
                                <th>TIPO DE ARTICULO</th>
                                <td class="center-align">
                                    <select class="browser-default input center-align" name="category" id="category" required>
                                        <option value="" disabled selected>Elige una categoria</option>
                                        @foreach ($category as $cat)
                                            <option value="{{ $cat->id }}"> {{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>MODELO</th>
                                <td>
                                    <input type="text" placeholder="Modelo" id="name" name="name" class="input" required>
                                </td>
                            </tr>
                            <tr>
                                <th>DESCRIPCIÓN</th>
                                <td>
                                    <textarea name="description" id="description" class="input" cols="60" rows="10"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>IMAGEN PRINCIPAL</th>
                                <td>
                                    <div class="col-md-4" style="padding-bottom: 50px;">
                                        <br>
                                        <div class="upload" id="upload">
                                            <text class="text-upload" div="drop">
                                                <i class="fa fa-upload fa-2x valign "></i><br><br>
                                                Suelte la imagen o haga click en el recuadro para cargar.
                                            </text>
                                        </div>
                                    </div> 
                                </td> 
                            </tr>
                            <tr class="hide opc0 trOpc">
                                <th id="opcion0"> OPCIÓN </th>
                                <td class="center-align">
                                    <select class="browser-default input center-align categories" name="category" id="select0" required>
                                        <option value="" disabled selected>Elige una opción</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="hide opc1 trOpc">
                                <th id="opcion1">OPCIÓN</th>
                                <td class="center-align">
                                    <select class="browser-default input center-align categories" name="category" id="select1" required>
                                        <option value="" disabled selected>Elige una opción</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>DISEÑADOR</th>
                                <td>
                                    <input type="text" placeholder="Diseñador" id="designer" name="designer" class="input" required>
                                </td>
                            </tr>
                            <tr>
                                <th>TEMPORADA</th>
                                <td>
                                    <input type="text" placeholder="Temporada" id="season" name="title" class="input" required>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <div class="row center-align">
                            <br>
                            <button class="btn save" type="submit"> Guardar</button>
                        </div>
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="" id="url" value="{{URL::asset('')}}">
</body>
@include('Admin.footer')
<script src="{{ URL::asset('js/dropzone.js') }}"></script>
<script src="{{ URL::asset('js/admin/trends/newTrend.js') }}"></script>
</html>