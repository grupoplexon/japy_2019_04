@include('Admin.header')
<link rel="stylesheet" href="{{ URL::asset('css/admin/edituser.css') }}">
    {{-- <div class="row nav-wrapper">
        <div class="col s9 offset-l3">
            <a href="#!" class="breadcrumb">Home</a>
        </div>
    </div> --}}
    <div class="row">
    @include('Admin.menu')
        <div class="col l9">
            <div class="col l12">
                <a href="{{route('usuarios')}}" class="black-text">Usuarios/<a href="">Editar</a></a>
                <br><br>
            </div>
            <div class="col l6">
                <div class="col l12 grey darken-4">
                    <p class="text-white">Información general</p>
                </div>
                <div class="col l12 grey lighten-4">
                    <br>
                    <div class="col l4">
                        <p>Id:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" disabled value="{{ $data->id }}" class="inputs" id="idUser">
                    </div>
                    <div class="col l4">
                        <p>Fecha de creación:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" disabled value="{{ $data->created_at }}" class="inputs">
                    </div>
                    <div class="col l4">
                        <p>Rol:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" disabled value="{{ $data->rol->name }}" class="inputs">
                    </div>
                    <div class="col l4">
                        <p>Nombre:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" value="{{ $data->name }}" class="inputs" id="name">
                    </div>
                    {{-- <div class="col l4">
                        <p>Correo:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" value="{{ $data->email }}" class="inputs" id="email">
                    </div> --}}
                    <div class="col l4">
                        <p>Usuario:</p>
                    </div>
                    <div class="col l8">
                        <input type="text" value="{{ $data->user }}" class="inputs" id="user" autocomplete="new-user">
                    </div>
                    <div class="col l4">
                        <p>Contraseña:</p>
                    </div>
                    <div class="col l8">
                        <input type="password" placeholder="Contraseña" class="inputs" id="password" autocomplete="new-password">
                    </div>
                    <div class="col l4">
                        <p>Confirmar contraseña:</p>
                    </div>
                    <div class="col l8">
                        <input type="password" placeholder="Confirmar contraseña" class="inputs" id="confirmpassword">
                    </div>
                    <div class=" col l12 center-align">
                        <p class="btn blue" id="savegeneral">Guardar</p>
                        <br>
                    </div>
                </div>
            </div>
            @if ($data->rol->name=='proveedor')
                <div class="col l6">
                    <div class="col l12 grey darken-4">
                        <p class="col l6 left-align text-white">Estados del proveedor</p>
                        <p class="col l6 right-align white-text pointer" id="newState"><i class="fas fa-plus-circle"></i> Nuevo</p>
                    </div>
                    <div class="col l12 grey lighten-4">
                        <br>
                        <div class="col l4">
                            <p>Estado:</p>
                        </div>
                        <div class="col l8">
                            <select class="inputs" id="state">
                                @foreach ($data->location_provider as $val)
                                    <option value="{{$val->id}}">{{$val->state}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col l12 m12 s12"></div>
                        <div class="col l4">
                            <p>Contacto:</p>
                        </div>
                        <div class="col l8">
                            <input type="text" value="@if(!empty($data->location_provider[0]->name_contact)) {{ $data->location_provider[0]->name_contact }} @endif" class="inputs" id="contact">
                        </div>
                        <div class="col l4">
                            <p>Teléfono:</p>
                        </div>
                        <div class="col l8">
                            <input type="text" value="@if(!empty($data->location_provider[0]->phone)) {{ $data->location_provider[0]->phone }} @endif" class="inputs" id="phone">
                        </div>
                        <div class="col l4">
                            <p>Celular:</p>
                        </div>
                        <div class="col l8">
                            <input type="text" value="@if(!empty($data->location_provider[0]->cellphone)) {{ $data->location_provider[0]->cellphone }} @endif" class="inputs" id="cellphone">
                        </div>
                        <div class="col l4">
                            <p>Página web:</p>
                        </div>
                        <div class="col l8">
                            <input type="text" value="@if(!empty($data->location_provider[0]->website)) {{ $data->location_provider[0]->website }} @endif" class="inputs" id="website">
                        </div>
                        <div class="col l4">
                            <p>Correo electrónico de contacto:</p>
                        </div>
                        <div class="col l8">
                            <input type="text" value="@if(!empty($data->location_provider[0]->email)) {{ $data->location_provider[0]->email }} @endif" class="inputs" id="email">
                        </div>
                        <div class="col l12 m12 s12"></div>
                        <div class="col l4">
                            <p>Activación:</p>
                        </div>
                        <div class="col l8">
                            <div class="col l6 m6 s12">
                                <p>
                                <label>
                                    <input name="group1" type="radio" @if($data->location_provider[0]->active==0) checked @endif value="0" id="radioDisabled"/>
                                    <span>Desctivado</span>
                                </label>
                                </p>
                            </div>
                            <div class="col l6 m6 s12">
                                <p>
                                <label>
                                    <input name="group1" type="radio" @if($data->location_provider[0]->active==1) checked @endif value="1" id="radioEnabled"/>
                                    <span>Activado</span>
                                </label>
                                </p>
                            </div>
                        </div>
                        <div class=" col l12 center-align">
                            <p class="btn blue" id="saveContact">Guardar</p>
                            <br>
                        </div>
                    </div>
                </div>

                <!-- Modal Structure -->
                <div id="modal1" class="modal modal-fixed-footer">
                    <div class="modal-content center-align">
                        <h5>Alta en nueva Ciudad</h5>
                        <br>
                        <div class="col l2">
                            <p>Estado*</p>
                        </div>
                        <div class="col l4">
                            <select class="inputs" id="newStateSelect">
                                <option value="" selected disabled>Seleccione un Estado</option>
                                @foreach ($states as $val)
                                    <option value="{{$val->id}}">{{$val->state}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col l2">
                            <p>Ciudad*</p>
                        </div>
                        <div class="col l4">
                            <select class="inputs" id="newCity">
                                <option value="">Seleccione una Ciudad</option>
                            </select>
                        </div>
                        <div class="col l12 m12 s12"></div>
                        <div class="col l2">
                            <p>Contacto</p>
                        </div>
                        <div class="col l4">
                            <input class="inputs" type="text" id="newContact">
                        </div>
                        <div class="col l2">
                            <p>E-Mail</p>
                        </div>
                        <div class="col l4">
                            <input class="inputs" type="text" id="newEmail">
                        </div>
                        <div class="col l2">
                            <p>Teléfono</p>
                        </div>
                        <div class="col l4">
                            <input class="inputs" type="number" id="newPhone">
                        </div>
                        <div class="col l2">
                            <p>Celular</p>
                        </div>
                        <div class="col l4">
                            <input class="inputs" type="number" id="newCellphone">
                        </div>
                        <div class="col l2">
                            <p>Página Web</p>
                        </div>
                        <div class="col l4">
                            <input class="inputs" type="text" id="newWebsite">
                        </div>
                        <div class="col l2">
                            <p>Activar*</p>
                        </div>
                        <div class="col l4">
                            <div class="col l6 m6 s12">
                                    <p>
                                    <label>
                                        <input name="group2" type="radio" checked value="1"/>
                                        <span>Si</span>
                                    </label>
                                    </p>
                                </div>
                                <div class="col l6 m6 s12">
                                    <p>
                                    <label>
                                        <input name="group2" type="radio" value="0"/>
                                        <span>No</span>
                                    </label>
                                    </p>
                                </div>
                        </div>
                        <div class="col l12 m12 s12 center-align">
                            <p><b>Nota:</b> los campos marcados con un * son obligatorios.</p>
                        </div>
                        <div class="col l12 m12 s12 center-align">
                            <p class="btn green" id="saveNew">Guardar</p>
                        </div>
                        <br><br><br><br>
                    </div>
                    <div class="col l12 m12 s12 modal-footer">
                        <p class="waves-effect waves-green btn-flat closeModal">Cerrar</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@include('Admin.footer')
<script src="{{ URL::asset('js/admin/edituser.js') }}"></script>