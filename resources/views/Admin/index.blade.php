@include('Admin.header')
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row">
              <div class="nav-wrapper breadcrumbs">
                <div class="col s12">
                  <a href="#!" class="breadcrumb">Home</a>
                  {{-- <a href="#!" class="breadcrumb">Facebook</a> --}}
                </div>
              </div>
            </div>
            <div class="row">
              <h5> Novias registradas en los ultimos 7 dias</h5>
              <div class="divider"></div>
              <div class="card-panel">
                  <div id="barchart_material" style="width: 900px; height: 500px;"></div>
              </div>
              
            </div>
        </div>
    </div>
@include('Admin.footer')