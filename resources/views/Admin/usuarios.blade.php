@include('Admin.header')
<link rel="stylesheet" href="datatables/datatables.min.css">
    <div class="row">
        @include('Admin.menu')

        <div class="col l9">
            <div class="row nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb">Usuarios</a>
                </div>
            </div>
            <div class="row card-panel">
                <table class="table striped centered" id="usersTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                        {{-- <th>Acciones</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@include('Admin.footer')
<script src="js/admin/usuarios.js"></script>