@include('Admin.header')
<link rel="stylesheet" href="datatables/datatables.min.css">
    <div class="row">
        @include('Admin.menu')

        <div class="col l9">
            <div class="row nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb">Proveedores</a>
                </div>
            </div>
            <div class="row card-panel">
                <table class="table striped centered" id="providersTable">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo cuenta</th>
                        <th>Asignar expiración</th>
                        <th>Expiración</th>
                        <th>Activación</th>
                        <th>Categoría</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@include('Admin.footer')
<script src="js/admin/proveedores.js"></script>