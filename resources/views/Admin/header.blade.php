<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=p, initial-scale=1.0"> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" href="css/materializecdn.min.css"> --}}
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- <link href="css/fontawesome-5.8.1/css/all.css" rel="stylesheet" type="text/css"/> --}}
    <link href="{{ URL::asset('css/fontawesome-5.8.1/css/all.css') }}" rel="stylesheet" type="text/css"/>
    {{-- <link rel="stylesheet" href="css/admin/index.css">      --}}
    <link rel="stylesheet" href="{{ URL::asset('css/admin/index.css') }}">     
    {{-- <link rel="stylesheet" href="datatables/datatables.min.css"> --}}
    <link rel="stylesheet" href="{{ URL::asset('datatables/datatables.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link rel="stylesheet" href="css/admin/estilos.css"> --}}
    <link rel="stylesheet" href="{{ URL::asset('css/admin/estilos.css') }}">
    <title>Administrador</title>
</head>
<body>
    
    <div class="row navbar-fixed">

      <ul id="dropdown1" class="dropdown-content">
        <li>

          <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
              Cerrar sesión 
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }} 
          </form>

        </li>
      </ul>

      <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                    <a href="" class="brand-logo">BrideAdvisor</a>
                    <ul class="right hide-on-med-and-down">
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">  {{ Auth::user()->name }} <i class="material-icons right">arrow_drop_down</i></a></li>
                    </ul>  
                </div>
            </nav>
      </div>
    </div>
</body>
</html>