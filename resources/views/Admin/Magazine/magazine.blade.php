<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Admin.header')
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="dataTables2/datatables.min.css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row ">
                {{-- <nav> --}}
                    <div class="nav-wrapper right">
                        <div class="col s12">
                        <a href="{{ route('home') }}" class="breadcrumb">Home</a>
                        <a href="{{ route('blog') }}" class="breadcrumb blue-text text-darken-2">Articulos</a>
                        </div>
                    </div>
                {{-- </nav> --}}
            </div>
            <div class="row">
                <a href="{{ route('newArticle') }}" class="btn"> NUEVO ARTICULO</a>
              <div class="card-panel">
                  <div class="row center-align">
                    <h5>ARTICULOS</h5>
                  </div>
                  <div class="row">
                    <table class="table table-striped table-bordered" id="magazineTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Titulo</th>
                            <th>Creado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($articles as $art) : ?>
                                <tr>
                                    <th> <?php echo $art->id_article ?></th>
                                    <th> <?php echo $art->publish ?></th>
                                    <th> <?php echo $art->title ?></th>
                                    <th> <?php echo $art->created_at ?></th>
                                    <th> 
                                        <a class="btn blue" href="{{ url('/editArticle', ['id' => $art->id_article ]) }}">Editar</a>
                                        <button class="btn red" onclick="deleteArticle(<?php echo $art->id_article ?>)">Eliminar</button>
                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <input type="hidden" name="" id="url" value="{{URL::asset('')}}">
                  </div>
              </div>
            </div>
        </div>
    </div>
</body>
@include('Admin.footer')
<script type="text/javascript" src="dataTables2/datatables.min.js"></script>
<script src="{{ URL::asset('js/admin/magazine/blog.js') }}"></script>
</html>