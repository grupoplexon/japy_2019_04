@include('Admin.header')
<html>
<head>
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{{ URL::asset('css/admin/promociones.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<style>
    .dz-preview .dz-image img {
      width: 30% !important;
      height: 30% !important;
    }
</style>
<body>
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row ">
                {{-- <nav> --}}
                    <div class="nav-wrapper right">
                        <div class="col s12">
                        <a href="{{ route('home') }}" class="breadcrumb">Home</a>
                        <a href="{{ route('blog') }}" class="breadcrumb">Articulos</a>
                        <a class="breadcrumb blue-text text-darken-2">Editar Articulo</a>
                        </div>
                    </div>
                {{-- </nav> --}}
            </div>
            <div class="row">
                <h6>Editar entrada</h6>
                <div class="card-panel row">
                    {{-- <form action="{{ route('saveArticle') }}" method="POST"  enctype="multipart/form-data" > 
                        {{ csrf_field() }} --}}
                        <table class="table centered" id="magazineTable">
                            <tr>
                                <th>IMAGEN PRINCIPAL</th>
                                <td>
                                    <div class="col-md-4" style="padding-bottom: 50px;">
                                        <br>
                                        <div class="upload" id="upload">
                                            <text class="text-upload" div="drop">
                                                <i class="fa fa-upload fa-2x valign "></i><br><br>
                                                Suelte la imagen o haga click en el recuadro para cargar.
                                            </text>
                                        </div>
                                        <input name="file" type="hidden" class="archivo"
                                            value="{{ URL::asset('resources/uploads/magazine/'.$article->main_image.'') }}">
                                        <button type="button" class="btn dorado-2 eliminar-foto"style="width: 20%" >
                                            Eliminar Foto
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>TITULO</th>
                                <td>
                                    <input type="text" placeholder="TITULO" id="title" value="{{ $article->title }}" name="title" class="input" required>
                                </td>
                            </tr>
                            <tr>
                                <th>TIPO DE ARTICULO</th>
                                <td class="center-align">
                                    <select class="browser-default input center-align" name="category" id="category" required>
                                        <option value="" disabled selected>Elige una categoria</option>
                                        @foreach ($category as $cat)
                                        <option value="{{ $cat->id_category }}" 
                                            @if ($cat->id_category==$article->type)
                                            selected
                                        @endif
                                              > {{ $cat->name_category }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>ESTADO DEL ARTICULO</th>
                                <td>

                                    <div class="switch">
                                        <label>
                                            Borrador
                                        <input type="checkbox" id="publish" name="publish" {{ ($article->publish == 1) ? 'checked' : ''  }}> 
                                            <span class="lever"></span>
                                            Publicar
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            {{-- <tr>
                                <th>TAGS</th>
                                <td>Estado</td>
                            </tr> --}}
                        </table>
                        <br>
                        <textarea id="froala-editor" class="contenido"  name="contenido" >{{ $article->content }}</textarea>
                        <input type="hidden" name="" id="idArticle" value="{{ $article->id_article }}">
                        <div class="row center-align">
                            <br>
                            <button class="btn saveArticle" type="submit"> Guardar</button>
                        </div>
                    {{-- </form> --}}
                        <input type="hidden" name="" id="url" value="{{URL::asset('')}}">
                </div>
            </div>
        </div>
    </div>
</body>
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/js/froala_editor.pkgd.min.js'></script>
@include('Admin.footer')
<script src="{{ URL::asset('js/dropzone.js') }}"></script>
<script src="{{ URL::asset('js/admin/magazine/editArticle.js') }}"></script>
</html>