@include('Admin.header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{ URL::asset('css/admin/promociones.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="row">
            @include('Admin.menu')
            <div class="col l9">
                <div class="row ">
                    {{-- <nav> --}}
                        <div class="nav-wrapper right">
                            <div class="col s12">
                                <a href="{{ route('home') }}" class="breadcrumb">Home</a>
                                <a href="{{ route('blog') }}" class="breadcrumb">Articulos</a>
                                <a href="#" class="breadcrumb blue-text text-darken-2">Articulo Nuevo</a>
                            </div>
                        </div>
                    {{-- </nav> --}}
                </div>
                <div class="row">
                    <h6>Nueva entrada</h6>
                    <div class="card-panel row"> 
                        {{-- <form action="{{ route('saveArticle') }}" method="POST"  enctype="multipart/form-data" > 
                            {{ csrf_field() }} --}}
                            <table class="table centered" id="magazineTable">
                                <tr>
                                    <th>IMAGEN PRINCIPAL</th>
                                    <td>
                                        <div class="col-md-4" style="padding-bottom: 50px;">
                                            <br>
                                            {{-- <input type="file" name="principal" id="principal" accept="image/png" required>
                                            <br> <br>
                                            <img class="hide" id="imgPrincipal" style="width:40%;" src="" /> --}}
                                            <div class="upload" id="upload">
                                                <text class="text-upload" div="drop">
                                                    <i class="fa fa-upload fa-2x valign "></i><br><br>
                                                    Suelte la imagen o haga click en el recuadro para cargar.
                                                </text>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>TITULO</th>
                                    <td>
                                        <input type="text" placeholder="TITULO" id="title" name="title" class="input" required>
                                    </td>
                                </tr>
                                <tr>
                                    <th>TIPO DE ARTICULO</th>
                                    <td class="center-align">
                                        <select class="browser-default input center-align" name="category" id="category" required>
                                            <option value="" disabled selected>Elige una categoria</option>
                                            @foreach ($category as $cat)
                                                <option value="{{ $cat->id_category }}"> {{ $cat->name_category }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ESTADO DEL ARTICULO</th>
                                    <td>
                                        <div class="switch">
                                            <label>
                                                Borrador
                                                <input type="checkbox" id="publish" name="publish"> 
                                                <span class="lever"></span>
                                                Publicar
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <th>TAGS</th>
                                    <td>Estado</td>
                                </tr> --}}
                            </table>
                            <br>
                            <textarea id="froala-editor" class="contenido" name="contenido" >CONTENIDO DEL ARTICULO</textarea>
                            <div class="row center-align">
                                <br>
                                <button class="btn saveArticle" type="submit"> Guardar</button>
                            </div>
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.5/js/froala_editor.pkgd.min.js'></script>
    @include('Admin.footer')
    <script src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script src="{{ URL::asset('js/admin/magazine/newArticle.js') }}"></script>
    </html>