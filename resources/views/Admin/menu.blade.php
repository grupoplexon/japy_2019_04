<div class="container col l3 menu-lateral"><a href="#" data-target="nav-mobile" class="top-nav sidenav-trigger full hide-on-large-only"><i class="material-icons">menu</i></a>
  <ul id="nav-mobile" class="sidenav sidenav-fixed">
    <li class="logo"><a class="brand-logo"> Administrador </a></li>
    <li class="bold"><a href="{{ route('home') }}" class="waves-effect waves-teal"><i class="fas fa-chart-line"></i>Dashboard</a></li>
    <li class="bold"><a href="{{ route('usuarios') }}" class="waves-effect waves-teal"> <i class="fas fa-users icon-style"></i> Usuarios</a></li>
    <li class="bold"><a href="{{ route('blog') }}" class="waves-effect waves-teal"><i class="fas fa-newspaper"></i>Articulos</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal"><i class="fas fa-tags"></i> Etiquetas</a></li>
    <li class="bold"><a href="{{route('proveedores')}}" class="waves-effect waves-teal"><i class="far fa-id-badge"></i>Proveedores</a></li>
    <li class="bold"><a href="{{route('verificaciones')}}" class="waves-effect waves-teal"><i class="fas fa-check-square"></i>Verificaciones @if(isset($notification)) <span class="new badge red @if($notification==0)oculto @endif" id="admNotifications">{{$notification}}</span>@endif</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal"><i class="fas fa-eye"></i>Indicadores</a></li>
    <li class="bold"><a href="{{route('trends')}}" class="waves-effect waves-teal"><i class="fas fa-poll-h"></i>Tendencias</a></li>
    <li class="bold"><a href="" class="waves-effect waves-teal"><i class="fas fa-poll-h"></i>Tendencias</a></li>
    <li class="bold"><a href="{{route('admin/brideweekend')}}" class="waves-effect waves-teal"><i class="fas fa-ring"></i>BrideWeekend</a></li>
  </ul>
</div>