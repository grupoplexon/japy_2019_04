<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor - Registro</title>
    <link rel="stylesheet" href="{{ URL::asset('css/registroNovia.css') }}">
</head>
<body>
    @include('menu')
    <div class="row background-row">
        <div class="col l3 offset-l8 center-align">
            <form action="{{route('registerBride')}}" method="POST" id="formRegister">
                {{ csrf_field() }}
                <br>
                <h4 class="title-register">REGÍSTRATE</h4>
                <br>
                <input type="text" class="inputs" placeholder="Nombre(s)" name="name" required autofocus>
                <input type="text" class="inputs" placeholder="Apellidos" name="last_name" required>
                <select class="inputs t-black" name="gender" required>
                    <option value="" selected disabled>Novia/Novio</option>
                    <option value="Novia">Novia</option>
                    <option value="Novio">Novio</option>
                </select>
                <br><br>
                <input type="text" class="inputs" placeholder="Fecha de la boda" name="date" onfocus="(this.type='date')" required>
                <input type="number" class="inputs" placeholder="Teléfono" name="phone" required>
                <input type="email" class="inputs" placeholder="Correo" name="email" required autocomplete="new-user">
                <input type="password" class="inputs" placeholder="Contraseña" name="password" id="password" required autocomplete="new-password">
                <input type="password" class="inputs" placeholder="Confirmar Contraseña" name="last_password" id="last_password" required>
                <select class="inputs t-black" name="state" required>
                    <option value="" selected disabled>Selecciona tu estado</option>
                    @foreach ($states as $val)
                        <option value="{{$val->state}}">{{$val->state}}</option>
                    @endforeach
                </select>
                <br><br>
                <input type="hidden" id="URL" value="{{URL::asset('')}}">
                <button class="btn" id="btnRegister">REGISTRARSE</button>
            </form>
            <br>
            <p>
                Al dar clic en registrarse estás aceptando los terminos , condiciones y nuestras políticas de privacidad.
                <br><br>
                <a class="privacy" href="">Consulta las políticas de privacidad</a>
            </p>
        </div>
    </div>
    @include('footer')
    <script src="{{URL::asset('js/registroNovia.js')}}"></script>
</body>
</html>