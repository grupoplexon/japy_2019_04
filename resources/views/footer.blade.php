<div id="footer" class="col l12">
    <footer style="padding-top: unset;" class="page-footer">
        <div class="footer-copyright" style="">
            <div class="row center-align footer" style="">
                <div class="col s12 m12 l5 japy" >
                    <div class="row  " style="">
                        <a href="{{route('/')}}">
                            <img src="{{URL::asset('resources/logos/ba-rose.png')}}" style="width: 50%;" class="brideAdvisorimg" alt="Japy">
                        </a>
                    </div>
                </div>
                <div class="col s10 offset-s1 m12 l2" >
                    <div class="row  center-align" style="">
                            <h6 class="center-align titleBA" >BrideAdvisor© 2019 DERECHOS RESERVADOS</h6>
                    </div>
                </div>
                <div class="col s12 m12 l5" >
                    <div class="row  " style="">
                        <a href="https://www.nupcialmexicana.com">
                            <img src="{{URL::asset('resources/logos/anm_rose.png')}}" class="logo-anm"  alt="BrideAdvisor">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URL::asset('js/materializecdn.min.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>