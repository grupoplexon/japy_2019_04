<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>{{$provider->name}} - BrideAdvisor</title>
    <link rel="stylesheet" href="{{ URL::asset('css/fotorama.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/lightslider.css') }}">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/stars.css') }}">
</head>
<body>
    @include('menu')
    <div class="row background">
        <div class="col l5 fotorama" 
            data-fit="scaledown"
            data-keyboard="true"
            data-nav="thumbs" 
            data-transition="crossfade" 
            data-fit="cover" 
            data-loop="true"
            data-height="400px">
            @foreach ($provider->gallery as $value)
                @if($value->type!='video')
                    <img src="{{URL::asset('resources/uploads/providers/'.$value->name_image.'')}}">
                @else
                    {{-- <video src="http://localhost/japy_2019_04/public/resources/uploads/providers/5da88d990584c.mp4"></video> --}}
                    <a href="{{URL::asset('resources/uploads/providers/'.$value->name_image.'')}}"
                        data-video="true">
                        <img src="resources/media/video.png">
                    </a>
                @endif
            @endforeach
        </div>
        <div class="col l7 space-right">
            <input type="hidden" value="{{URL::asset('')}}" id="url">
            <div class="col l3">
                @foreach ($provider->gallery as $val)
                    @if($val->profile==1)
                        <img class="img-content profile-provider" src="{{URL::asset('resources/uploads/providers/'.$val->name_image.'')}}">
                    @endif
                @endforeach
            </div>
            <div class="col l7">
                <h5 class="black-text">{{$provider->name}}</h5>
                @foreach ($provider->location_provider as $val)
                    <label>
                        {{-- @if ($val->state==$provider->location_provider[0]->state)
                            <input class="radio-button" name="group1" type="radio" value="{{$val->id}}" checked />
                        @else
                            <input class="radio-button" name="group1" type="radio" value="{{$val->id}}" />
                        @endif --}}
                        @if($val->active==1)
                            @if($params[0]['state']==$val->state)
                                <input class="radio-button" name="group1" type="radio" value="{{$val->id}}" checked/>
                            @else
                                @if ($val->state==$provider->location_provider[0]->state)
                                    <input class="radio-button" name="group1" type="radio" value="{{$val->id}}" checked />
                                @else
                                    <input class="radio-button" name="group1" type="radio" value="{{$val->id}}" />
                                @endif
                            @endif
                            <span class="black-text size-content">{{$val->state}}</span>
                        @endif
                    </label>&nbsp;
                @endforeach
                <p><h6 class="black-text size-content"><i class="fas fa-map-marker-alt"></i> <span id="address"></span></h6></p>
                <p><h6 class="black-text size-content"><i class="fas fa-phone"></i> <span id="cellPhone"></span></h6></p>
            </div>
            <div class="col l12 m12 s12">
                <br><hr class="band"><br>
                <h5 class="black-text">Descripción</h5>
                <h5 class="size-content-two justify black-text"><?= $provider->provider->description ?></h5>
            </div>
        </div>
        <div class="col l12 m12 s12"><br><br><br></div>
        <div class="col l12 m12 s12">
            <div class="col l5">
                <ul id="tabs-swipe-demo" class="col l6 tabs">
                    <li class="tab col l6 s3 grey darken-3"><a class="white-text" href="#test-swipe-1">CALIFICAR</a></li>
                    <li class="tab col l6 s3 grey darken-3"><a class="white-text" href="#test-swipe-2">RESEÑAS</a></li>
                </ul>
                <div id="test-swipe-1" class="col l12 card center-align card-profile large-coment">
                    @if(isset(Auth::user()->id))
                        <br><p class="size-content bold">Califica este proveedor</p>
                        <div class="col l12 s12 center-align">
                            <fieldset class="rating" style="width: 100%;">
                                <input type="radio" id="star5" name="rating" class="rating" value="5" required /><label class = "full" for="star5" title="5 stars"></label>

                                <input type="radio" id="star4" name="rating" class="rating" value="4" /><label class = "full" for="star4" title="4 stars"></label>

                                <input type="radio" id="star3" name="rating" class="rating" value="3" /><label class = "full" for="star3" title="3 stars"></label>

                                <input type="radio" id="star2" name="rating" class="rating" value="2" /><label class = "full" for="star2" title="2 stars"></label>

                                <input type="radio" id="star1" name="rating" class="rating" value="1" /><label class = "full" for="star1" title="1 star"></label>
                            </fieldset>
                        </div>
                        <div class="input-field col l12 s12 center-align">
                            <br>
                            <input id="mensaje" name="mensaje" type="text" class="validate">
                            <label for="mensaje">Describe tu experiencia</label>
                            <input type="hidden" value="" id="proveedor" name="proveedor">
                        </div>
                        @if($provider->role_user->role_id==2)
                            <p class="btn grey darken-3">PUBLICAR</p>
                        @else
                            <p class="size-content bold black-text">En esta sección calificarán y cometarán la calidad de sus servicios.</p>
                        @endif
                    @else
                        <br><br><br><br>
                        <h5>PARA CALIFICAR Y COMENTAR DEBES INGRESAR A TU CUENTA.</h5>
                    @endif
                </div>
                <div id="test-swipe-2" class="col l12 card center-align card-profile large-coment">
                    <br><p class="size-content bold margins">Reseñas y Calificaciones</p>
                    <div class="col l12 m12 s12 left-align">
                        @foreach ($provider->review as $val)
                        <p class="justify p-coment">
                            <span class="black-text size-content justify"><i class="far fa-user-circle"></i> Miguel Mota</span><br>
                            @for ($i = 1; $i < 6; $i++)
                                @if ($i<=$val->qualification)
                                    <i class="fa fa-star" style="color:#f9a825;"></i>
                                @else
                                    <i class="far fa-star" style="color:#f9a825;"></i>
                                @endif
                            @endfor
                            <br><span class="justify">{{$val->message}}</span>
                        </p>
                            <div class="col l12 m12 s12 divider"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col l7">
                <div class="col l12 m12 s12 center-align grey darken-3">
                    <p class="size-content white-text">SOLICITA COTIZACIÓN CON {{strtoupper($provider->name)}}</p>
                </div>
                <div class="col l12 m12 s12 card card-profile cotizacion center-align">
                    @if(isset(Auth::user()->id))
                        <div class="col l2">
                            <p class="size-content">Mensaje:</p>
                        </div>
                        <div class="col l10">
                            <input class="inputs" type="text">
                        </div>
                        @if($provider->role_user->role_id==2)
                            <p class="btn grey darken-3">ENVIAR CONSULTA</p>
                            <br><br>
                        @else
                            <p class="col l12 m12 s12 size-content bold black-text">En esta sección le contactarán para preguntar por los servicios que ofrece.</p>
                        @endif
                    @else
                        <h5>PARA PONERTE EN CONTACTO DEBES INGRESAR A TU CUENTA.</h5><br>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/fotorama.js') }}"></script>
    <script src="{{ URL::asset('js/lightslider.js') }}"></script>
    <script src="{{ URL::asset('js/perfilProveedor.js') }}"></script>
</body>
</html>