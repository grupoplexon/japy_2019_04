<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor</title>
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
</head>
<body>
    @include('menu')
    <div class="row">
        <div class="slider slide hide-on-small-only">
            <ul class="slides slide2">
                <li>
                    <a href="https://brideadvisor.mx/brideweekend/expo_puebla"><img src="resources/slides/index/004_r.jpg"></a>
                </li>
                <li>
                    <img class="img-home " style="width:100%;" src="resources/slides/index/slider01.png">
                </li>
                <li>
                    <a href="https://brideadvisor.mx/magazine"><img src="resources/slides/index/002_r.png"></a>
                </li>
                <li>
                    <img class="img-home " style="width:100%;" src="resources/slides/index/003_r.png">
                </li>
            </ul>
        </div>
    </div>
    <div class="row background">
        <div class="row center-align container">
            <br><br>
            <h4 class="textPrincipal"> LA MANERA MÁS SENCILLA DE PLANEAR TU BODA</h4>
            <p class="descrip">Bienvenidos a BrideAdvisor, plataforma digital en donde te ayudaremos 
                a planear tu boda con las mejores herramientas.</p>
        </div>
        <br>
        <div class="row div-articulos center-align">
            <h5 class="title">ARTICULOS RECIENTES</h5>
        </div>
        <br>
        <hr class="hr  hide-on-small-only">
        <br>
        <div class="row center-align hide-on-small-only">
            <h4>MI PLANEADOR DE BODA</h4>
            <div class="row">
                <div class="col l2 offset-l1 center-align">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/agenda.png" alt=""></a>
                    <br>
                    <h5>AGENDA</h5>
                    <h6>No olvides agendar todos tus pendientes y actividades.</h6>
                </div>     
                <div class="col l2  center-align">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/invitados.png" alt=""></a>
                    <br>
                    <h5>INVITADOS</h5>
                    <h6>Haz una lista con la selección de tus invitados, sus contactos y mantén tus confirmaciones al día.</h6>
                </div>     
                <div class="col l2 offset-l2 center-align ">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/mesas.png" alt="">   </a>
                    <br>
                    <h5>CONTROL DE MESAS</h5>
                    <h6>Esta parte es divertida, ya que podrás acomodar a tus invitados.</h6>
                </div>     
                <div class="col l2 center-align">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/proveedores.png" alt=""></a>
                    <br>
                    <h5>PROVEEDORES</h5>
                    <h6>Tu agenda personal con tus proveedores, tus pagos y pendientes.</h6>
                </div>     
            </div>
            <br>
            <div class="row">
                <div class="col l2 offset-l2 center-align">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/presupuesto.png" alt=""></a>
                    <br>
                    <h5>PRESUPUESTADOR INTELIGENTE</h5>
                    <h6>Te ayuda a administrar y maximizar tu dinero y a seleccionar el proveedor ideal de acuerdo a tus necesidades.</h6>
                </div>     
                <div class="col l2 offset-l1 center-align">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/vestidos.png" alt=""></a>
                    <br>
                    <h5>MIS VESTIDOS</h5>
                    <h6>Encontrarás diseños exclusivos de las mejores marcas, para que elijas de tus favoritos</h6>
                </div>     
                <div class="col l2 offset-l1 center-align ">
                    <a href="{{route('planeador_boda')}}"><img class="icon" src="resources/icons/icons-pink/portal.png" alt=""></a> 
                    <br>
                    <h5>MI PORTAL WEB</h5>
                    <h6>Podrás compartir información de tu boda con tus invitados a través de redes sociales.</h6>
                </div>  
            </div>
            <br><br>
        </div>
        <div class="row div-articulos center-align">
            <h5 class="title">NUEVOS PROVEEDORES</h5>
        </div><br>
        <div class="row white">
            <br>
            @if(sizeof($providers)>0)
                <div class="col l12 m12 s12 row-providers">
                    @foreach ($providers as $val)
                        <div class="col l4 m4 s12">
                            @foreach ($val->gallery as $value)
                                @if($value->profile==1)
                                    <img class="img-providers" src="{{URL::asset('resources/uploads/providers/'.$value->name_image.'')}}">
                                @endif
                            @endforeach
                            <div class="col l12 m12 s12 paddings border-providers">
                                <div class="col l12 m12 s12">
                                    <h5 class="h5-providers">{{$val->name}}</h5>
                                    <p class="p-providers"><?= substr($val->provider->description,0,160) ?>...</p>
                                    <p class="p-providers">
                                        Calificación: 
                                        @for ($i = 1; $i < 6; $i++)
                                            @if($i<=$val->provider->average)
                                                <i class="fa fa-star" style="color:#f9a825;"></i>
                                            @else
                                                <i class="far fa-star" style="color:#f9a825;"></i>
                                            @endif
                                        @endfor
                                    </p>
                                    <div class="col l12 m12 s12 center-align">
                                        <a class="btn pink lighten-5 p-providers" href="{{URL::asset(''.$val->provider->slug.'')}}">VER MÁS</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        <br><br>
        <div class="row div-articulos center-align  hide-on-large-only">
            <h5 class="title">NOTICIAS Y ACTUALIZACIONES</h5>
        </div>
        <div class="row  center-align  hide-on-large-only">
            <div class="col l4 offset-l4 s10 offset-s1 ">
                <a class="color-a" href=""><h5 class="secciones">REGISTRATE</h5></a>
                <input name="email_susc" class="registro" style="" placeholder="Introduce tu correo electronico"/>
                <br><br>
                <button type="" class="btn searchProvider sig_in hide-on-large-only" style="border-radius: 10px 10px 10px 10px !important;" > SUSCRIBIRME</button>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/index.js') }}"></script>
</body>
</html>