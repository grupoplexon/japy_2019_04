<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
</head>
<body>
    @include('Provider.menu')
    <img class="img-content" src="resources/media/proveedor.png">
    <div class="row center-align background-index">
        <br><br>
        <h4 class="text-content">INDICADORES DE LOS ÚLTIMOS 7 DÍAS</h4>
        <div class="container">
            <div class="col l4 m6 s6 offset-l2">
                <div class="card center-align">
                    <br>
                    <i class="fa fa-users green-text size-stats"></i>
                    <span class="size-total-stats">
                        @if (!empty($visitSeven))
                            &nbsp;{{$visitSeven->quantity}}
                        @else
                            &nbsp;0
                        @endif
                    </span>
                    <p class="text-stats">Visitas a tu perfil</p>
                    <br>
                </div>
            </div>
            <div class="col l4 m6 s6">
                <div class="card center-align">
                    <br>
                    <i class="fa fa-search green-text size-stats"></i>
                    <span class="size-total-stats">
                        @if (!empty($searchSeven))
                            &nbsp;{{$searchSeven->quantity}}
                        @else
                            &nbsp;0
                        @endif
                    </span>
                    <p class="text-stats">Búsquedas de tu perfil</p>
                    <br>
                </div>
            </div>
        </div>
        <div class="col l12 m12 s12 graphics">
            <hr class="hr-stats">
            <div class="col l7 center-align">
                <h5 class="text-content">ESTADÍSTICAS</h5>
                <div class="col l12 m12 s12 card" id="chart1">

                </div>
                {!! $chart1 !!}
            </div>
            <div class="col l4 offset-l1 center-align">
                <h5 class="text-content">COMPLETA TU PERFIL</h5>
                <div class="card">
                    <p class="justify complete">
                        <span class="text-stats">¡Haz Completado tu perfil !</span><br>
                        Con un anuncio más completo tendras mayor posibilidades de que las parejas te contacten para contratar tus servicio
                    </p><br>
                </div>
            </div>
        </div>
        <br><br>
    </div>
    @include('footer')
    {{-- <script src="{{ URL::asset('js/parsley.js') }}"></script> --}}
    <script src="{{ URL::asset('js/Provider/index.js') }}"></script>
</body>
</html>