<div class="navbar row">
    
  
    <nav>
        <div class="nav-wrapper row">
            <div class="col l3 s7 right-align top-logo">
                <a href="">
                    <img class="img-menu" src="{{ URL::asset('resources/logos/BrideAdvisorBlack.png') }}">
                </a>
            </div>
            <div class="col l1 center-align top hide-on-med-and-down">
                <a href="{{route('home')}}">
                    <img class="img" src="{{URL::asset('resources/icons/proveedor/perfil.png')}}">
                    <br><span class="text-menu">MI PERFIL</span>
                </a>
            </div>
            <div class="col l1 center-align top hide-on-med-and-down">
                <a href="{{route('escaparate')}}">
                    <img class="img" src="{{URL::asset('resources/icons/proveedor/escaparate.png')}}">
                    <br><span class="text-menu">ESCAPARATE</span>
                </a>
            </div>
            <div class="col l1 center-align top hide-on-med-and-down">
                <a href="{{route('solicitudes')}}">
                    <img class="img" src="{{URL::asset('resources/icons/proveedor/solicitudes.png')}}">
                    <br><span class="text-menu">SOLICITUDES</span>
                </a>
            </div>
            <div class="col l2 center-align padding top hide-on-med-and-down">
                <a href="{{route('recomendaciones')}}">
                    <img class="img-large" src="{{URL::asset('resources/icons/proveedor/Recomendacion.png')}}">
                    <br><span class="text-menu">RECOMENDACIONES</span>
                </a>
            </div>
            <div class="col l1 center-align top hide-on-med-and-down">
                <a href="">
                    <img class="img" src="{{URL::asset('resources/icons/proveedor/notificacion.png')}}">
                    <br><span class="text-menu">NOTIFICACIONES</span>
                </a>
            </div>
            <div class="col l2 s4 offset-l1 offset-s1 center-align border">
                @if (!empty($logo))
                    <img class="dropdown-trigger logo-user" data-target="dropdown1" src="resources/uploads/providers/{{$logo->name_image}}"><br>
                @else
                    <img class="dropdown-trigger logo-user" data-target="dropdown1" src="resources/media/user.png"><br>
                @endif
                <a class="dropdown-trigger text-menu" href="#!" data-target="dropdown1">{{Auth::user()->name}}</a>
            </div>
        </div>
    </nav>
    <ul id="dropdown1" class="dropdown-content">
        <li>
            <a class="grey-text darken-5" href=""><i class="fas fa-wrench"></i>Mi Cuenta</a>
        </li>
        <li>
            <a class="grey-text darken-5" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fas fa-times"></i>
                Cerrar sesión 
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }} 
            </form>
        </li>
    </ul>
</div>