<div class="col l2 s12 m4 card card-menu-lateral left-align card-space">
    <a class="col l12 m12 s12 black-text hover hide-on-small-only" href="{{route('/').'/'.$slug->slug}}">
        <h5><i class="fas fa-home"></i> Mi buzón</h5>
        <label class="black-tetx label-escaparate">Ver mi escaparate ></label>
        <br><br>
    </a>
        <a class="col l12 m12 s1 offset-s3 hide-on-med-and-up black-text hover options home" href="{{route('/').'/'.$slug->slug}}"><i class="fas fa-home"></i> <span class="hide-on-small-only">Datos de la Empresa</span></a>
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 m12 s1 black-text hover options escaparate" href="{{route('escaparate')}}"><i class="fas fa-sign-in-alt grey-text"></i> <span class="hide-on-small-only">Entrada</span></a>
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 m12 s1 black-text hover options escaparate" href="{{route('escaparate')}}">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-envelope orange-text"></i> <span class="hide-on-small-only">No Leidos</span></a>
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 m12 s1 black-text hover options escaparate" href="{{route('escaparate')}}">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-envelope grey-text"></i> <span class="hide-on-small-only">Leidos</span></a>
    <div class="col l12 s1 m12 divider hide-on-small-only"></div>
        <a class="col l12 m12 s1 black-text hover options localizacion" href="{{route('localizacion')}}"><i class="far fa-clock orange-text"></i> <span class="hide-on-small-only">Pendientes</span></a>
    {{-- <div class="col l12 m12 s1 divider"></div>
        <a class="col l12 m12 s1 black-text hover options" href=""><i class="fas fa-check"></i> Preguntas Frecuentes</a> --}}
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 s1 m12 black-text hover options promo" href="{{route('promociones')}}"><i class="fa fa-reply blue-text darken-5"></i> <span class="hide-on-small-only">Atendidos</span></a>
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 m12 s1 black-text hover options fotos" href="{{route('fotos')}}"><i class="far fa-check-square green-text darken-5"></i> <span class="hide-on-small-only">Contratadas</span></a>
    <div class="col l12 m12 s1 divider hide-on-small-only"></div>
        <a class="col l12 s1 m12 black-text hover options videos" href="{{route('videos')}}"><i class="far fa-times-circle red-text"></i> <span class="hide-on-small-only">Descartadas</span></a>
</div>