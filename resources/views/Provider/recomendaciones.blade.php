<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        <br>
        <div class="col l10 m12 s12 offset-l1 card grey lighten-4">
            <h5>Recomendaciones</h5>
            <div class="col l12 m12 s12 divider"></div>
            <div class="col l6 m6 s12 info">
                <p class="white-text"><i class="fas fa-info"></i> 
                    &nbsp; Las buenas recomendaciones ayudan a que más clientes te contacten.
                </p>
            </div>
            <div class="col l12 m12 s12"></div>
            @if (sizeof($reviews)>0)
                @foreach ($reviews as $val)
                    <div class="col l4">
                        <div class="col l12 m12 s12 card card-content-reviews">
                            <div class="col l3 m4 s12">
                                <img class="img-content profile-recomendation" src="resources/media/user.png">
                            </div>
                            <div class="col l9 m8 s12">
                                <h6 class="bold">María del Socorro Bañuelos Robles</h6>
                                <span>{{strftime('%e de %B del %Y', strtotime($val->created_at))}}</span>
                            </div>
                            <div class="col l12 m12 s12">
                                <p>
                                    <span class="dark-grey-text">Calificación: </span>
                                    @for ($i = 1; $i < 6; $i++)
                                        @if ($i<=$val->qualification)
                                            <i class="fa fa-star" style="color:#f9a825;"></i>
                                        @else
                                            <i class="far fa-star" style="color:#f9a825;"></i>
                                        @endif
                                    @endfor
                                </p>
                                <p class="dark-grey-text justify">{{substr($val->message, 0, 340)}}@if(strlen($val->message)>340)... @endif</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <h5 class="col l12 m12 s12 not-reviews"><i class="fas fa-info"></i> &nbsp;Aun no hay recomendaciones de tu perfil.</h5>
            @endif
        </div>
    </div>
    @include('footer')
    {{-- <script src="{{ URL::asset('js/dropzone.js') }}"></script> --}}
    <script src="{{ URL::asset('js/Provider/miCuenta.js') }}"></script>
</body>
</html>