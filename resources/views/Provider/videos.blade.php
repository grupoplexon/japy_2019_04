<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulateral')
        <div class="col l8 m7 card card-space" id="test1">
            <div class="col l12 m12 s12">
                <h5>Galeria de Videos</h5>
                <div class="col l12 m12 s12 divider"></div>
                {{-- <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> 
                        &nbsp; Publique mínimo 8 fotos de su empresa relacionadas con bodas y los servicios que ofrece. Entre más fotos publique más fácil será que los usuarios contacten con usted para contratar sus servicios.
                    </p>
                </div> --}}
                <p>
                    <i class="fas fa-check green-text"></i> Sólo se permite un video en formato MP4. El cuál debe tener un peso máximo de 500 Mb.
                </p>
                <form action="" id="form_subir">
                    {{ csrf_field() }}
                    <div class="form-1-2">
                        <label>Archivo a subir: </label>
                        <input type="file" name="archivo" id="archivo" required>
                        <br><br>
                    </div>
        
                    <div class="barra oculto" id="barra">
                        <div class="barra_azul">
                            <span id="progreso"></span>
                        </div>
                    </div>
        
                    <div class="acciones">
                        <br>
                        <input type="submit" class="btn pink lighten-5" value="Subir">
                    </div>
                </form>
                <br>
                <div class="col l12 m12 s12 padding" id="video">
                    @foreach ($video as $value)
                        <div class="col l3 m6 s12" id="card-{{$value->id}}">
                            <div class="card height">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <video class="activator height cover width-video" src="resources/uploads/providers/{{$value->name_image}}"></video>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4 size-text">Editar<i class="material-icons right">close</i></span>
                                    {{-- <p>
                                        <label>
                                          <input name="group1" type="radio" class="filled-in logo" @if ($value->logo==1) checked @endif onclick="logo({{$value->id}})"/>
                                          <span>Logotipo</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                          <input name="group2" type="radio" class="filled-in" @if ($value->profile==1) checked @endif onclick="perfil({{$value->id}})"/>
                                          <span>Foto principal</span>
                                        </label>
                                    </p> --}}
                                    <a class="btn green white-text width space-top" href="resources/uploads/providers/{{$value->name_image}}" target="_nlank">Ver video</a>
                                    <span class="btn red white-text width space-top delete" id="{{$value->id}}">Eliminar</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script src="{{ URL::asset('js/Provider/videos.js') }}"></script>
</body>
</html>