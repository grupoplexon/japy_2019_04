<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulateral')
        <div class="col l8 m7 card card-space">
            <div class="col l12 m12 s12">
                <h5>Modificar Localización y Mapa</h5>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> 
                        &nbsp; Puede modificar la ubicación sobre el mapa arrastrando el mapa hasta el punto deseado. <br>
                        La dirección debe constar del nombre de la calle seguido de una coma y el resto de datos. <br>
                        Ejemplo: Av Siempre Viva 412, Colonia La Calma
                    </p>
                </div>
                <div class="col l5 s12 m6">
                    <select class="inputs" id="states">
                        @foreach ($data as $value)
                            <option value="{{$value->id_loc}}">{{$value->state}}</option>
                        @endforeach 
                    </select>
                    <br>
                    <label>Ciudad</label>
                    <input class="inputs" type="hidden" id="city" value="{{$data[0]->city}}">
                    <select class="inputs" id="cities">

                    </select>
                    <br>
                    <label>Código postal</label>
                    <input class="inputs" type="text" id="postal_code" value="{{$data[0]->postal_code}}">
                    <label>Dirección</label>
                    <input class="inputs" type="text" id="address" value="{{$data[0]->address}}">
                    <input name="latitud" id="latitud" type="hidden"
                            value="{{$data[0]->latitude}}">
                    <input name="longitud" id="longitud" type="hidden"
                            value="{{$data[0]->longitude}}">
                </div>
                <div class="col l7 s12 m6">
                    <div class="col l12 m12 s12 mapa" id="mapa">

                    </div>
                </div>
                <div class="col l12 m12 s12 center-align">
                    <br><button class="btn pink lighten-5" id="saveLoc">GUARDAR</button><br><br>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
    {{-- <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCMeC8bM7deJJH3XhsSSq_bRym5GAtHgP8'></script> --}}
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA"></script>
    <script src="{{ URL::asset('js/Provider/localizacion.js') }}"></script>
</body>
</html>