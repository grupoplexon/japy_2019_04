<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulsolicitudes')
        
        <div class="col l8 m7 card card-space" id="test1">
            <div class="col l12 m12 s12">
                <h5>Mis solicitudes</h5>
                <div class="col l12 m12 s12 divider"></div>
                
                
            </div>
        </div>
    </div>
    @include('footer')
    {{-- <script src="{{ URL::asset('js/dropzone.js') }}"></script> --}}
    {{-- <script src="{{ URL::asset('js/Provider/fotos.js') }}"></script> --}}
</body>
</html>