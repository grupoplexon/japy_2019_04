<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulateral')
        <div class="col l8 m7 card card-space">
            <div class="col l12 m12 s12">
                <h5>Mis Datos</h5>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> &nbsp;Modifica la información general de tu empresa. Es muy importante que toda la información publicada en tu escaparate y tus datos de contacto estén actualizados y sean veraces.</p>
                </div>
                <h5>Datos de Acceso</h5>
                <div class="col l12 m12 s12 divider"></div>
                <br>
                <form id="formCredentials">
                    <div class="col l6 m6 s12">
                        <label>Usuario:</label>
                        <input class="inputs" type="text" value="{{$data->user}}" required id="user" name="user">
                        <label>Contraseña actual:</label>
                        <input class="inputs" type="password" id="passwordActual">
                    </div>
                    <div class="col l6 m6 s12">
                        <label>Contraseña nueva:</label>
                        <input class="inputs" type="password" id="password" name="password">
                        <label>Confirmar nueva contraseña:</label>
                        <input class="inputs" type="password" id="password_2">
                    </div>
                    <div class="col l12 m12 s12 center-align">
                        <button class="btn pink lighten-5" id="btnCredentials">GUARDAR</button><br><br>
                    </div>
                </form>
                <h5>Describe tu empresa</h5>
                <div class="col l12 m12 s12 divider"></div>
                <br>
                <div class="col l8 s12 m8">
                    <label>Nombre de Empresa</label>
                    <input class="inputs" type="text" value="{{$data->name}}" id="name_company">
                    <input type="hidden" id="idUser" value="{{$data->id}}">
                    <label>Descripción</label>
                    <textarea class="descripcion editable" name="descripcion" id="description">{{$data->description}}</textarea>
                </div>
                <div class="col l4 m4 hide-on-small-only">
                    <p class="justify">
                        Describe detalladamente tu empresa así como los servicios o productos de bodas que ofreces con la máxima información de interés para los novios.
                    </p>
                </div>
                <div class="col l4 m4 s12">
                    <div class="col l12 m12 s12 card prices">
                        <p class="center-align bold">Precio de sus servicios</p>
                        <label>Precio mínimo</label>
                        <input class="inputs" type="number" @if(!empty($data->price_min)) value="{{$data->price_min}}" @endif id="priceMin">
                        <label>Precio máximo</label>
                        <input class="inputs" type="number" @if(!empty($data->price_max)) value="{{$data->price_max}}" @endif id="priceMax">
                        @if (empty($data->price_max) && empty($data->price_min))
                            <span class="justify">
                                <b>Nota:</b> complete los precios para que
                                su perfil se recomiende a las novias que tengan un 
                                presupuesto dentro de su rango de precios.
                            </span>
                        @else
                            <span class="justify">
                                <b>Nota:</b> Su perfil será recomendado a las novias que tengan un 
                                presupuesto dentro de su rango de precios.
                            </span><br>
                        @endif
                        <p class="btn pink lighten-5" id="btnPrices">GUARDAR</p>
                    </div>
                </div>
                <div class="col l8 s12 m8 center-align">
                    <br><button class="btn pink lighten-5" id="btnCompany">GUARDAR</button><br><br>
                </div>
                <div class="col l12 s12 m12">
                    <h5>Datos de contacto</h5>
                    <div class="col l12 m12 s12 divider"></div>
                    <br>
                </div>
                <form id="formContact">
                    <div class="col l4 m12 s12 offset-l4">
                        <select class="inputs" id="locations">
                            @foreach ($info as $val)
                                <option value="{{$val->id}}">{{$val->state}}</option>
                            @endforeach
                        </select>
                        <br>
                    </div>
                    <div class="col l12 m12 s12"></div>
                    <div class="col l4 s12 m12">
                        <label>Persona de Contacto</label>
                        <input class="inputs" type="text" id="name_contact" value="{{$info[0]->name_contact}}" required>
                    </div>
                    <div class="col l4 s12 m12">
                        <label>Correo Electrónico</label>
                        <input class="inputs" type="email" id="email" value="{{$info[0]->email}}" required>
                    </div>
                    <div class="col l4 s12 m12">
                        <label>Teléfono</label>
                        <input class="inputs" type="number" id="phone" value="{{$info[0]->phone}}" required>
                    </div>
                    <div class="col l4 s12 m12">
                        <label>Celular</label>
                        <input class="inputs" type="number" id="cellphone" value="{{$info[0]->cellphone}}" required>
                    </div>
                    <div class="col l4 s12 m12">
                        <label>Página Web</label>
                        <input class="inputs" type="text" id="website" value="{{$info[0]->website}}" required>
                    </div>
                    {{-- </div> --}}
                    {{-- <div class="col l4 m5 hide-on-small-only">
                        <p class="justify">
                            Recibirás las solicitudes de información de usuarios interesados a la dirección de correo electónico que hayas indicado, así como todas las novedades del portal que puedan ser de tu interés.
                        </p>
                    </div> --}}
                    <div class="col l12 s12 m12 center-align">
                        <button class="btn pink lighten-5" id="btnContact">GUARDAR</button><br><br>
                    </div>
                </form>
                <div class="col l12 s12 m12">
                    <h5>Categoría</h5>
                    <div class="col l12 m12 s12 divider"></div>
                    <br>
                </div>
                <div class="col l12 m12 s12 center-align">
                    <select class="inputs" id="category">
                        @foreach ($category as $value)
                            @if ($data->category_id==$value->id)
                                <option value="{{$value->id}}" selected>{{$value->name_category}}</option>
                            @else
                                <option value="{{$value->id}}">{{$value->name_category}}</option>
                            @endif
                        @endforeach
                    </select>
                    <br>
                    <button class="btn pink lighten-5"  id="btnCategory">GUARDAR</button><br><br>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/parsley.js') }}"></script>
    <script src="{{ URL::asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('js/Provider/escaparate.js') }}"></script>
</body>
</html>