<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulateral')
        
        {{-- <div class="col l8 s12 m7 card card-space">
            <ul class="tabs">
                <li class="tab col s6 primary-background secondary-text">
                    <a href="#test1" id="btn-tab-presu">Publicar nuevas fotos</a>
                </li>
                <li class="tab col s6 primary-background secondary-text">
                    <a href="#test2" id="btn-tab-pagos">Inspiraciones activas</a>
                </li>
            </ul>
        </div> --}}

        <div class="col l8 m7 card card-space" id="test1">
            <div class="col l12 m12 s12">
                <h5>Galeria Fotográfica</h5>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> 
                        &nbsp; Publique mínimo 8 fotos de su empresa relacionadas con bodas y los servicios que ofrece. Entre más fotos publique más fácil será que los usuarios contacten con usted para contratar sus servicios.
                    </p>
                </div>
                <p>
                    <i class="fas fa-check green-text"></i> Añadir mínimo 8 fotografías (en formato PNG o JPG de un máximo de 3MB cada una).
                    <br><i class="fas fa-check green-text"></i> Añadir una imagen como logotipo de la empresa y seleccione la foto principal.
                </p>
                <div class="upload" id="upload">
                    <text class="text-upload" div="drop">
                        <i class="fa fa-upload fa-2x valign "></i><br><br>
                        Suelte los archivos o haga click en el recuadro para cargar.
                    </text>
                </div>
                <br>
                <div class="col l12 m12 s12 padding" id="gallery">
                    @foreach ($images as $value)
                        <div class="col l3 m6 s8 offset-s2" id="card-{{$value->id}}">
                            <div class="card height">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator height cover" src="resources/uploads/providers/{{$value->name_image}}">
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4 size-text">Editar<i class="material-icons right">close</i></span>
                                    <p>
                                        <label>
                                          <input name="group1" type="radio" class="filled-in logo" @if ($value->logo==1) checked @endif onclick="logo({{$value->id}})"/>
                                          <span>Logotipo</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                          <input name="group2" type="radio" class="filled-in" @if ($value->profile==1) checked @endif onclick="perfil({{$value->id}})"/>
                                          <span>Foto principal</span>
                                        </label>
                                    </p>
                                    {{-- <span class="btn green white-text width">Inspiracion</span> --}}
                                    <span class="btn red white-text width space-top delete" id="{{$value->id}}">Eliminar</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        {{-- <div class="col l8 m7 card card-space tab2" id="test2" style="display: none;">
            <div class="col l12 m12 s12">
                <h5>Inspiración</h5>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> 
                        &nbsp; Publique mínimo 8 fotos de su empresa relacionadas con bodas y los servicios que ofrece. Entre más fotos publique más fácil será que los usuarios contacten con usted para contratar sus servicios.
                    </p>
                </div>
            </div>
        </div> --}}
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script src="{{ URL::asset('js/Provider/fotos.js') }}"></script>
</body>
</html>