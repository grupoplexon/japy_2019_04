<!DOCTYPE html>
<html lang="es">
<head>
    @include('Provider.styles')
    <title>BrideAdvisor - {{ Auth::user()->name }}</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materializecdn.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/provider/escaparate.css') }}">
</head>
<body>
    @include('Provider.menu')
    <div class="row background-content">
        @include('Provider.menulateral')
        <div class="col l8 m7 card card-space">
            <div class="col l12 m12 s12">
                <h5>Promociones</h5>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 info">
                    <p class="white-text"><i class="fas fa-info"></i> 
                        &nbsp; Puede añadir promociones especiales al escaparate de su empresa. Cuanto mejores sean las ofertas y descuentos que anuncie mayor interés tendrán los novios en sus servicios y recibirá más solicitudes de presupuesto.
                    </p>
                </div>
                <div class="col l4">
                    <p class="justify">
                        <strong>Descuento especial para novios en Bride Advisor.</strong><br>
                        Ofrezca a los novios un descuento por contratar sus servicios a través de Bride Advisor
                    </p>
                </div>
                <div class="col l2 offset-l1">
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="3" @if ($provider->discount==3)
                            checked
                        @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 3%</span>
                    </label>
                    <br>
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="15" @if ($provider->discount==15)
                        checked
                    @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 15%</span>
                    </label>
                </div>
                <div class="col l2">
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="5" @if ($provider->discount==5)
                        checked
                    @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 5%</span>
                    </label>
                    <br>
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="20" @if ($provider->discount==20)
                        checked
                    @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 20%</span>
                    </label>
                </div>
                <div class="col l2">
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="10" @if ($provider->discount==10)
                        checked
                    @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 10%</span>
                    </label>
                    <br>
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="30" @if ($provider->discount==30)
                        checked
                    @endif/>
                        <span><i class="fas fa-tag orange-discount"></i> 30%</span>
                    </label>
                </div>
                <div class="col l4 offset-l1">
                    <label>
                        <input class="orange-discount" name="group1" type="radio" value="0" @if ($provider->discount==0)
                        checked
                    @endif/>
                        <span>No deseo hacer ningun descuento</span>
                    </label>
                </div>
                <div class="col l7 s12 right-align">
                    <p class="btn pink lighten-5" id="saveDiscount">GUARDAR</p>
                </div>
                <br><br><br>
                <div class="col l12 m12 s12">
                    <div class="col l6 m6 s12">
                        <br>
                        <h5>Otras promociones</h5>
                    </div>
                    <div class="col l6 m6 s12 right-align">
                        <br>
                        <p class="btn pink lighten-5" id="createPromo">CREAR PROMOCIÓN</p>
                    </div>
                </div>
                <div class="col l12 m12 s12 divider"></div>
                <div class="col l12 m12 s12 promotion oculto" id="nuevo">
                    <form id="formPromo">
                        <div class="col l12 m12 s12 center-align">
                            <h5 class="space-bot">Crear promoción</h5>
                        </div>
                        <div class="col l4 m6 s12">
                            <label>Tipo de promoción</label>
                            <select class="inputs space-bot" id="type" required>
                                <option value="" selected disabled>Elige una opcion</option>
                                <option value="Regalo">Regalo</option>
                                <option value="Oferta">Oferta</option>
                                <option value="Descuento">Descuento</option>
                            </select>
                            <label>Precio Original</label>
                            <input class="inputs" type="text" id="price" required>
                        </div>
                        <div class="col l4 m6 s12">
                            <label>Nombre de la promoción</label>
                            <input class="inputs" type="text" id="name" required>
                            <label>Precio con Descuento</label>
                            <input class="inputs" type="text" id="price2" required>
                        </div>
                        <div class="col l4 m6 s12">
                            <label>Fecha de Término</label>
                            <input class="inputs" type="date" id="date" required>
                        </div>
                        <div class="col l8 m12 s12">
                            <br>
                            <label>Descripción de la promoción</label>
                            <textarea class="descripcion editable" name="descripcion" id="description"></textarea>
                        </div>
                        <div class="col l4 m12 s12 center-align">
                            <br><br>
                            <div class="upload" id="upload">
                                <text class="text-upload" div="drop">
                                    <i class="fa fa-upload fa-2x valign "></i><br><br>
                                    Suelte los archivos o haga click en el recuadro para cargar.
                                </text>
                            </div>
                            <p class="btn pink lighten-5" id="deleteImage" data-promocion="nuevo">ELIMINAR FOTO</p>
                        </div>
                        <div class="col l12 m12 s12 center-align">
                            <input type="hidden" id="indicator">
                            <br><button class="btn pink lighten-5" id="btnPromo">GUARDAR</button><br><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if (sizeof($promotions)!=0)
            <div class="col l2 card-space"></div>
            <ul class="col l8 card card-space collapsible">
                @foreach ($promotions as $value)
                    <li>
                        <div class="collapsible-header">
                            <div class="col l8">
                                <i class="fas fa-gifts"></i>
                                <span>{{$value->name_promotion}}</span>
                            </div>
                            <div class="col l2 offset-l2">
                                <span>
                                    @if ($value->date_final>=date('Y-m-d'))
                                        Activa
                                    @else
                                        Finalizada
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="collapsible-body row" id="form{{$value->id}}">
                            <form id="formPromo{{$value->id}}">
                                <div class="col l4">
                                    <label>Tipo de promoción</label>
                                    <select class="inputs" id="typeUpdate{{$value->id}}" required>
                                        <option value="Regalo" @if ($value->type=='Regalo')
                                            selected
                                        @endif>Regalo</option>
                                        <option value="Oferta" @if ($value->type=='Oferta')
                                                selected
                                            @endif>Oferta</option>
                                        <option value="Descuento" @if ($value->type=='Descuento')
                                                selected
                                            @endif>Descuento</option>
                                    </select>
                                </div>
                                <div class="col l4">
                                    <label>Nombre de la promoción</label>
                                    <input class="inputs" type="text" id="nameUpdate{{$value->id}}" value="{{$value->name_promotion}}" required>
                                </div>
                                <div class="col l4">
                                    <label>Fecha de Término</label>
                                    <input class="inputs" type="date" id="finalDateUpdate{{$value->id}}" value="{{$value->date_final}}" required>
                                </div>
                                <div class="col l4">
                                    <label>Precio Original</label>
                                    <input class="inputs" type="text" id="priceUpdate{{$value->id}}" value="{{$value->price_original}}" required>
                                </div>
                                <div class="col l4">
                                    <label>Precio con Descuento</label>
                                    <input class="inputs" type="text" id="priceTwoUpdate{{$value->id}}" value="{{$value->price_secondary}}" required>
                                </div>
                                <div class="col l8">
                                    <br>
                                    <label>Descripción de la promoción</label>
                                    <textarea class="descripcion editable" name="descripcion{{$value->id}}" id="description{{$value->id}}" required>{{$value->description_promotion}}</textarea>
                                </div>
                                <div class="col l4 center-align">
                                    <br><br>
                                    <div class="upload" id="dropzoneUpload{{$value->id}}" data-promocion="{{$value->id}}">
                                        <text class="text-upload" div="drop">
                                            <i class="fa fa-upload fa-2x valign "></i><br><br>
                                            Suelte los archivos o haga click en el recuadro para cargar.
                                        </text>
                                    </div>
                                    <input name="file" type="hidden" class="archivo"
                                                           value="resources/uploads/promotions/{{$value->image_promotion}}">
                                    <p class="btn pink lighten-5 deleteImg" data-promocion="{{$value->id}}">ELIMINAR FOTO</p>
                                </div>
                                <div class="col l12 center-align">
                                    <br><button class="btn pink lighten-5 updatePromo" id="{{$value->id}}">GUARDAR</button><br>
                                </div>
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/parsley.js') }}"></script>
    <script src="{{ URL::asset('js/dropzone.js') }}"></script>
    <script src="{{ URL::asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('js/Provider/promociones.js') }}"></script>
</body>
</html>