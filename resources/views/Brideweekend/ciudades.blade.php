<!DOCTYPE html>
<html lang="es">
<head>
    @include('Brideweekend.styles')
    <title>BrideWeekend - Ciudades</title>
    <link rel="stylesheet" href="{{URL::asset('css/brideweekend/brideweekend.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/brideweekend/ciudades.css')}}">
</head>
<body>
    @include('Brideweekend.menu')
    <br><br>
    <div class="row center-align">
        <h3>PRÓXIMOS EVENTOS</h3>
        <br>
        <div class="col l8 offset-l2 s8 offset-s2 calendario"> <h5>CALENDARIO 2020</h5></div>
        <div class="row ciudades">
            <br>
            <?php foreach($ciudades as $c){ ?>
                <a href="{{URL::asset('brideweekend/ciudad/'.$c->city.'')}}" class="col l4 s12 m6 div-ciudad" style="">
                    <div class="ciudad-general" style="background: url('{{URL::asset('/resources/uploads/brideweekend/'.$c->img_enclosure.'')}}')">
                        <div class="grises">
                            <img class="recinto" src="" alt="">
                            <h4>{{$c->city}}</h4>
                            <h5>{{$c->dateParse}}</h5>
                            <div class="div-logo-index">
                                <img class="img-logo" src="{{URL::asset('resources/uploads/brideweekend/'.$c->logo_enclosure.'')}}">
                            </div>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
    </div>
    <input type="hidden" id="url" value="{{URL::asset('')}}">
    @include('Brideweekend.footer')
</body>
</html>