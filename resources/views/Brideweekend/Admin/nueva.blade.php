@include('Admin.header')
    <div class="row">
        @include('Admin.menu')
        <link rel="stylesheet" href="{{URL::asset('css/admin/newCity.css')}}">
        <div class="col l9">
            <div class="row nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb">Alta de Ciudad</a>
                </div>
            </div>
            <div class="row card-panel">
                <form action="{{route('saveCity')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col l6">
                        <div class="col l4">
                            <span>Ciudad:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="text" name="city" required>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Fecha inicial:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="date" name="initial_date" required>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Fecha final:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="date" name="final_date">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Horario Sábado:</span>
                        </div>
                        <div class="col l8">
                            <div class="col l6 padding-left">
                                <input class="inputs" type="time" name="entry_sat_start" required>
                            </div>
                            <div class="col l6 padding-right">
                                <input class="inputs" type="time" name="entry_sat_end" required>
                            </div>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Horario Domingo:</span>
                        </div>
                        <div class="col l8">
                            <div class="col l6 padding-left">
                                <input class="inputs" type="time" name="entry_sun_start">
                            </div>
                            <div class="col l6 padding-right">
                                <input class="inputs" type="time" name="entry_sun_end">
                            </div>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Pasarela Sábado:</span>
                        </div>
                        <div class="col l8">
                            <div class="col l6 padding-left">
                                <input class="inputs" type="time" name="runway_sat_start">
                            </div>
                            <div class="col l6 padding-right">
                                <input class="inputs" type="time" name="runway_sat_end">
                            </div>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Pasarela Domingo:</span>
                        </div>
                        <div class="col l8">
                            <div class="col l6 padding-left">
                                <input class="inputs" type="time" name="runway_sun_start">
                            </div>
                            <div class="col l6 padding-right">
                                <input class="inputs" type="time" name="runway_sun_end">
                            </div>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>¿Quien organiza el evento?:</span>
                        </div>
                        <div class="col l8">
                            <select class="inputs" name="organized_by" required>
                                <option value="" selected disabled>Selecciona una opcion</option>
                                <option value="plexon">Grupo Plexon</option>
                                <option value="externo">Externo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col l6">
                        <div class="col l4">
                            <span>Nombre del recinto:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="text" name="enclosure" required>
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Descripción del recinto:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="text" name="description">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Dirección del recinto:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="text" name="address">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Mapa:</span>
                        </div>
                        <div class="col l8">
                            <input class="inputs" type="text" name="map">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Imágen del recinto:</span>
                        </div>
                        <div class="col l8">
                            <input class="files" type="file" name="img_enclosure">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Logo del recinto:</span>
                        </div>
                        <div class="col l8">
                            <input class="files" type="file" name="logo_enclosure">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Imágen de cabecera:</span>
                        </div>
                        <div class="col l8">
                            <input class="files" type="file" name="img_head">
                        </div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l12 m12 s12 space"></div>
                        <div class="col l4">
                            <span>Activar:</span>
                        </div>
                        <div class="col l8">
                            <div class="col l3">
                                <label>
                                    <input name="group1" type="radio" checked value="1"/>
                                    <span>Si</span>
                                </label>
                            </div>
                            <div class="col l3">
                                <label>
                                    <input name="group1" type="radio" value="0"/>
                                    <span>No</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col l12 m12 s12 center-align">
                        <br><br>
                        <input type="hidden" name="indicator" value="new">
                        <button class="btn blue">Guardar</button>
                    </div>
                </form>
                <input type="hidden" value="{{URL::asset('')}}" id="URL">
            </div>
        </div>
    </div>
@include('Admin.footer')
<script>$('.dropdown-trigger').dropdown();</script>