@include('Admin.header')
    <div class="row">
        @include('Admin.menu')
        <div class="col l9">
            <div class="row nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb">BrideWeekend</a>
                </div>
            </div>
            <div class="row card-panel">
                <a class="btn grey darken-4 white-text" href="{{URL::asset('nuevo/ciudad')}}">Nueva Ciudad</a><br><br>
                <table class="table striped centered" id="brideweekendTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Ciudad</th>
                        <th>Fecha inicial</th>
                        <th>Fecha final</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <input type="hidden" value="{{URL::asset('')}}" id="URL">
            </div>
        </div>
    </div>
@include('Admin.footer')
<script src="{{URL::asset('js/admin/brideweekend.js')}}"></script>