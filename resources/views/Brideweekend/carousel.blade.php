<div class="row">
    <br><br>
    <div class="col s12 l10 offset-l1">
        <div class="row jcarousel-cities center">
            @foreach($cities as $val)
                <div class="col s12 m2 l4-jcaroul post">
                    <a href="{{URL::asset('brideweekend/ciudad/'.$val->city.'')}}">
                        <div class="col l12 m12 s12" 
                        style="
                            background-image: url('{{URL::asset('/resources/uploads/brideweekend/'.$val->img_enclosure.'')}}');
                            background-size: cover;
                            background-repeat: no-repeat;
                            height: 350px;
                            width: 100% !important;
                        ">
                            <h4 class="white-text title-carousel">{{$val->city}}</h4> 
                            {{-- <h6 class="white-text title-carousel size-subtitle">{{$val->dateFormat}}</h6> --}}
                            <h6 class="white-text title-carousel size-subtitle">Enero 4 y 5</h6>
                            <div class="div-logo">
                                    <img class="img img-carousel" src="{{URL::asset('resources/uploads/brideweekend/'.$val->logo_enclosure.'')}}">
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>