<!DOCTYPE html>
<html lang="es">
<head>
    @include('Brideweekend.styles')
    <title>BrideWeekend</title>
    <link rel="stylesheet" href="{{URL::asset('css/brideweekend/brideweekend.css')}}">
</head>
<body>
    @include('Brideweekend.menu')
    <div class="row">
        <div class="slider">
            <ul class="slides">
                <li>
                    <img src="{{URL::asset('resources/media/brideweekend/slide1.png')}}"> <!-- random image -->
                </li>
                {{-- <li>
                    <img src="https://lorempixel.com/580/250/nature/2"> <!-- random image -->
                </li>
                <li>
                    <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
                </li> --}}
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="container center-align">
            <br>
            <h5>¡Consulta los próximos eventos en tu ciudad!</h5><br>
            <div class="ciudad">
                <h5>Selecciona tu ciudad</h5>
            </div>
            <div class="select-ciudad">
                <select class="select-city" id="selectCity">
                    <option value="" disabled selected> ______________</option>
                    <?php foreach($data as $val) { ?>
                        <option value="{{$val->city}}">{{$val->city}}</option>
                    <?php } ?>
                </select>
                <input type="hidden" id="URL" value="{{URL::asset('')}}">
            </div>
            <img class="img space-banner" src="{{URL::asset('resources/media/brideweekend/banner01.png')}}">
        </div>
        <div class="col l10 offset-l1 space-banner center-align">
            <h4>EXPERIENCIA BRIDE WEEKEND</h4>
            <br>
            <video class="img" src="{{URL::asset('resources/media/brideweekend/video.mp4')}}" autoplay controls autobuffer></video>
        </div>
    </div>
    @include('Brideweekend.footer')
    <script src="{{URL::asset('js/brideweekend/brideweekend.js')}}"></script>
</body>
</html>