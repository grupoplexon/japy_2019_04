<div id="header" class="">
    <nav>
        <div class="nav-wrapper">
            <a href="{{URL::asset('/brideweekend')}}" class="brand-logo center"><img class="logo" src="{{URL::asset('resources/logos/bw.png')}}"></a>
            
            <ul id="nav-mobile" class="left hide-on-med-and-down nav-left">
                <li><a href="{{route('brideweekend/concepto')}}">CONCEPTO</a></li>
                <li><a href="{{route('brideweekend/ciudades')}}">CIUDADES</a></li>
            </ul>
            <ul id="nav-mobile" class="right hide-on-med-and-down nav-right">
                <li><a href="{{route('brideweekend/expositor')}}">EXPOSITOR</a></li>
                <li><a href="badges.html">REGALOS</a></li>
            </ul>
        </div>
    </nav>
</div>
<hr class="hr-nav">