<div class="footer row center-align">
    <div class="col l3"> <img src="{{URL::asset('resources/logos/AMPROFEC.png')}}"> </div>
    <div class="col l3"><a href="https://nupcialmexicana.com"><img src="{{URL::asset('resources/logos/LOGO_C.png')}}"></a></div>
    <div class="col l3"><a href="{{URL::asset('/')}}"><img src="{{URL::asset('resources/logos/LOGO_N.png')}}"></a></div>
    <div class="col l3"> <img src="{{URL::asset('resources/logos/IWA-Logo.png')}}"> </div>
</div>
<div class="footer-copyright row center-align vertical-align" >
    <div class="col l4 ">
        <a href=""><img class="logo-footer" src="{{URL::asset('resources/logos/logobwwhite.png')}}"></a>
    </div>
    <div class="col l4">
        <h6>CONTÁCTANOS:</h6>
        <h6>contacto@brideweekend.com</h6>
        <h6>01 800 7191 421</h6>
    </div>
    <div class="col l4">
        <h5> <a href="https://www.facebook.com/brideweekendmx/"><i class="fab fa-facebook-square"></i>&nbsp;&nbsp; </a>
        <a href="https://www.instagram.com/brideweekendbodas/"><i class="fab fa-instagram"></i>&nbsp;&nbsp; </a>
        <i class="fab fa-whatsapp"></i></h5>
        <h6>© 2019 Bride Weekend</h6>
    </div>
</div>
<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URL::asset('js/materializecdn.min.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>