<!DOCTYPE html>
<html lang="es">
<head>
    @include('Brideweekend.styles')
    <title>BrideWeekend - {{$city->city}}</title>
    <link rel="stylesheet" href="{{URL::asset('css/brideweekend/brideweekend.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/brideweekend/ciudad.css')}}">
</head>
<body>
    @include('Brideweekend.menu')
    <div class="row">
        <br>
        <div class="col l12 m12 s12 width-head"
        style="background-image: url('{{URL::asset('resources/uploads/brideweekend/'.$city->img_head.'')}}');
            height: 350px !important;
            background-size: cover !important;
            background-position: bottom;
            background-repeat: no-repeat !important;
            padding: 0px !important;">
            <div class="col l12 m12 s12 center-align background-title">
                <h3 class="white-text">{{$city->city}}</h3>
                <hr class="hr-city">
                @if(!empty($city->enclosure_description))
                    <h5 class="white-text">{{$city->enclosure.', '.$city->enclosure_description}}</h5>
                @else
                    <h5 class="white-text">{{$city->enclosure}}</h5>
                @endif
            </div>
        </div>
        <div class="col l6 offset-l3">
            <div class="col l12 m12 s12 card">
                <div class="col l4 height-logo">
                    <img class="img img-logo" src="{{URL::asset('resources/uploads/brideweekend/'.$city->logo_enclosure.'')}}">
                    <br><br>
                </div>
                <div class="col l8">
                    <br>
                    <h5 class="size-date bold center-align">{{$city->dateParse}}</h5>
                    <p class="bold center-align size-schedule">{{$city->timeParse}}</p>
                    <p class="bold center-align size-schedule">{{$city->enclosure_address}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row paddings">
        <div class="col l6 s12 m6 paddings">
            <img class="img object large-location" src="{{URL::asset('resources/uploads/brideweekend/'.$city->img_enclosure.'')}}">
        </div>
        <div class="col l6 s12 m6 paddings">
            <iframe class="map" src="{{$city->map}}"></iframe>
        </div>
    </div>
    <input type="hidden" id="url" value="{{URL::asset('')}}">
    @if($city->organized_by=='plexon')
        @include('Brideweekend.contactLOC')
    @else
        @include('Brideweekend.contactEXT')
    @endif
    @include('Brideweekend.footer')
    <script src="{{URL::asset('js/brideweekend/ciudad.js')}}"></script>
</body>
</html>