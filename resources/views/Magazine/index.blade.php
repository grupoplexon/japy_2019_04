<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Magazine</title>
    @include('styles')
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/magazine/index.css') }}">
</head>
<body class="background">
    @include('menu')
    <div class="row">
        <div class="row search background" style="position: relative; ">
            <a href="{{route('magazine')}}" class=""><img class="img-home " style="width:100%;" src="{{ URL::asset('resources/media/magazine.png') }}"></a>
            <div class="col l10 offset-l1 center-align menu-magazine " >
                 <div style=""> 
                    <div class="vl hide-on-med-and-down"></div>
                    @foreach ($categories as $cate)
                        <a href="{{ url('/magazine/categoria', ['slug' => $cate->slug ]) }}"> <h5 class="title opcion space" style="color: #515151;">{{ strtoupper($cate->name_category)}}</h5> </a>
                        <div class="vl hide-on-med-and-down"></div>
                    @endforeach
                 </div> 
            </div>
        </div>
        <br><br>
        <div class="row div-articulos div-recientes center-align">
            <h5 class="title">ARTICULOS</h5>
        </div>
        <div class="row center-align">
            @if ( count($articles) !=0)
            <br>
                <div class="col l10 offset-l1" style="margin-bottom:50px;">
                    @foreach ($articles as $art)
                        <div class="col l4 m6 s12">
                            <div class="row card post" >
                                    <div class="col l12 center-align">
                                        <a style="" href="{{ url('/magazine/articulo', ['name' => $art->slug ]) }}"><img class="art-img"
                                            src="{{ URL::asset('resources/uploads/magazine/'.$art->main_image.'') }}"></a>
                                    </div>
                                    <div class="col l12 ">
                                        <br>
                                        @foreach ($categories as $cate)
                                            @if ($art->type !=null && $art->type == $cate->id_category)
                                                <h6 class="contenido">{{$cate->name_category}}</h6> 
                                            @endif
                                        @endforeach
                                        <h5 class="descrip">{{$art->title}}</h5>
                                        <h6 class=""><?php echo substr(strip_tags($art->content), 0, 150) ?>...</h6>
                                    </div>
                            </div>
                        </div>
                        
                    @endforeach
                    <div class="col l12 s12">
                        {{$articles->render()}}
                    </div>
                </div>
            @else
                <div class="col l10 offset-l1 card-panel green lighten-5">
                    <h3>Aun no hay publicaciones en esta categoria, intenta mas tarde</h3>
                </div>
            @endif
        </div>
        <div class="row div-articulos center-align">
            <h5 class="title">NOTICIAS Y ACTUALIZACIONES </h5>
        </div>
        <div class="row center-align">
            <br>
            <a class="color-a" href=""><h5 class="secciones">REGISTRATE PARA SER EL PRIMERO EN RECIBIR NOTIFICACIONES</h5></a>
            <br><input id="email_susc" class="registro col l8 offset-l2" style="" placeholder="Introduce tu correo electronico"/>
            <div class="col l12 s12">
                <br>
                <button type="" id="buttonRegistro" class="btn sig_in" style="" > SUSCRIBIRME</button>
            </div>
        </div>
        <br>
        <hr class="hr2">
        <br>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/index.js') }}"></script>
</body>
</html>