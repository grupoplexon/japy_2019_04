<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$article->title}}</title>
    @include('styles')
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/magazine/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/magazine/article.css') }}">
</head>
<body class="background" >
    @include('menu')
    <div class="row center-align">
        <h3> {{$article->title}} </h3>
        <hr>
    </div>
    <div class="row content">
        <p class="content-post" style="color: #757575"><?php echo $article->content ?></p>
    </div>
    <div class="row div-articulos center-align">
        <h5 class="title"> PODRÍA INTERESARTE TAMBIÉN</h5>
    </div>
    <br>
    <div class="row content">
        @foreach ($articles as $art)
            <div class="col l4 m6 s12">
                <div class="row card post" >
                    <br>
                        <div class="col l12 center-align ">
                            @foreach ($categories as $cate)
                                @if ($art->type !=null && $art->type == $cate->id_category)
                                    <h6 class="contenido">{{$cate->name_category}}</h6> 
                                @endif
                            @endforeach
                            <h5 class="descrip">{{$art->title}}</h5>
                        </div>
                        <div class="col l12 center-align">
                            <a style="" href="{{ url('/magazine/articulo', ['name' => $art->slug ]) }}"><img class="art-img"
                                src="{{ URL::asset('resources/uploads/magazine/'.$art->main_image.'') }}"></a>
                        </div>
                        <div class="col l12 center-align">
                            <br>
                            <h6 class=""><?php echo substr(strip_tags($art->content), 0, 150) ?>...</h6>
                        </div>
                </div>
            </div>
        @endforeach
    </div>
    <br>
    @include('footer')
    <script src="{{ URL::asset('js/index.js') }}"></script> 
</body>
</html>