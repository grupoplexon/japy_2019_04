<!DOCTYPE html>
<html lang="en">
    @include('menu')
    <link rel="stylesheet" href="css/login.css">
    <div class="background-image row">
        <div class="col l4 offset-l8 center-vertical">
            <h4 class="style-title">Login</h4>
            <h6 class="style-subtitle">Correo</h6>
            <div class="col l9 s12 margin-button">
                <input class="inputs-login" type="text">
            </div>
            <br><br><br>
            <h6 class="style-subtitle">Contraseña</h6>
            <div class="col l9 s12 margin-button">
                <input class="inputs-login" type="password">
            </div>
            <div class="col l9 s12 center-align">
                <p class="btn button-login">Iniciar Sesión</p>
                <!-- <div class="fb-login-button" data-size="medium" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div> -->
                <h6 class="text-color">Olvidaste Tu Contraseña?</h6>
                <a href="{{ route('register') }}" class="text-color underline">Crear una cuenta</a>
            </div>
        </div>
    </div>
@include('footer')
</html>