<div class="naxbar-fixed">
    <nav class="nav-wrapper" role="navigation">       
        <div class="nav-wrapper" style="margin: auto;width: 90%;"> 
            <a href="#" data-activates="mobile-demo" class="button-collapse right button-movil" ><i class="material-icons">menu</i></a>
            <nav>
                <div class="nav-wrapper">
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="{{route('/')}}"><img class="ba-menu" src="{{URL::asset('resources/logos/BrideAdvisor.png')}}"></a></li>
                        <li><a href="{{route('home')}}">MI BODA</a></li>
                        <li><a href="{{route('magazine')}}">MAGAZINE</a></li>
                        <li><a href="{{route('proveedores/index')}}">PROVEEDORES</a></li>
                        <li><a href="{{route('vestidos/index')}}">VESTIDOS</a></li>
                        <li><a href="">DESTINOS</a></li>
                    </ul>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="{{route('brideweekend')}}"><img class="bw-menu" src="{{URL::asset('resources/logos/logo.png')}}"></a></li>
                        @if(!isset(Auth::user()->id))
                            <li><a href="{{ route('cuenta') }}"><i class="fas fa-user user" ></i></a></li>
                        @endif
                        <li><a href="{{route('planeador_boda')}}">Conócenos</a></li>
                        @if(isset(Auth::user()->id))
                            <li>
                            {{-- @if (!empty($logo))
                                <img class="dropdown-trigger ba-menu" data-target="dropdown1" src="resources/uploads/providers/{{$logo->name_image}}"><br>
                            @else
                                <img class="dropdown-trigger ba-menu" data-target="dropdown1" src="resources/media/user.png">
                            @endif --}}
                            <p class="dropdown-trigger text-menu" href="#!" data-target="dropdown1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-bars"></i></p></li>
                        @endif
                    </ul>
                    <ul id="dropdown1" class="dropdown-content">
                        
                            <a class="grey-text darken-5" href=""><i class="fab fa-elementor"></i>Panel de proveedor</a>
                        
                        
                            <a class="grey-text darken-5" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                <i class="fas fa-times"></i>
                                Cerrar sesión 
                            </a>
                
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }} 
                            </form>
                        
                    </ul>
                </div>
            </nav>
            {{-- <a href="">
                <img class="img-logo img-movil brand-logo" src="resources/logos/BrideAdvisor.png" alt="">
            </a> --}}
            
            {{-- <ul class="left hide-on-med-and-down menuBA" >
                <li><a href="">MI BODA</a></li>
                <li><a href="">PROVEEDORES</a></li>
                <li><a href="">MAGAZINE</a></li>
                <li><a href="">VESTIDOS</a></li>
            </ul>
            <ul class="right hide-on-med-and-down menuBA-right" >
                <li><div class="vl2"></div></li>
                <li><a href=""><img src="resources/logos/logo.png" alt="" style="height: 7vh; margin-right: 5vh;"></a></li>
                <li ><a  href="{{ route('cuenta') }}"><i class="fas fa-user user" ></i></a></li>
                <li><a href="">Conócenos</a></li>
                <li></li>
            </ul> --}}
            <ul id="mobile-demo" class="side-nav">
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion" style="display: inline-grid;">
                        <li><a class="a-mobile" href="">MI BODA</a></li>
                        <li><a class="a-mobile" href="">PROVEEDORES</a></li>
                        <li><a class="a-mobile" href="">MAGAZINE</a></li>
                        <li><a class="a-mobile" href="">VESTIDOS</a></li>
                        <li><a class="a-mobile" href="">BrideWeekend</a></li>
                        <li><a class="a-mobile" href="">Conócenos</a></li>
                        <li><a class="a-mobile" href="">Empresa</a></li>
                        <li><a class="a-mobile" href="">Inicia Sesion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</div>