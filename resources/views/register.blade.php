<!DOCTYPE html>
<html lang="en">
    @include('menu')
    <link rel="stylesheet" href="css/register.css">
    <div class="background-image">
        <div class="container background-register">
            <div class="row">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Tipo de usuario:</h6>
                </div>
                <div class="col l4 s8 select-register">
                    <select name="type_user" id="usuario">
                        <option value="Novio">Selecciona una opcion</option>
                        <option value="Novio">Novio</option>
                        <option value="Novia">Novia</option>
                        <option value="Proveedor">Proveedor</option>
                    </select>
                </div>
                <div class="col l12 m12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold" id="labelname">Nombre:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" type="text" placeholder="Nombre" name="name" id="name">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2 indicatorone oculta">
                    <h6 class="font-bold">Apellidos:</h6>
                </div>
                <div class="col l4 s12 indicatorone oculta">
                    <input class="inputs-register" type="text" placeholder="Apellidos" name="last_name" id="last_name">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Correo:</h6>
                </div>
                <div class="col l4 s12">
                    @if (isset($email))
                        <input class="inputs-register" type="email" placeholder="Correo" value="{{ $email }}" name="email" id="email">
                    @else
                        <input class="inputs-register" type="email" placeholder="Correo" name="email" id="email">
                    @endif
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Nombre de usuario:</h6>
                </div>
                <div class="col l4 s12 indicator oculta">
                    <input class="inputs-register" type="text" placeholder="Usuario" name="user" id="user">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Teléfono:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Teléfono" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" id="phone"/>
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Celular:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Celular" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="cellphone" id="cellphone"/>
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">País:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="País" type="text" name="country" id="pais">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Estado:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Estado" type="text" name="state" id="estado">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Ciudad:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Ciudad" type="text" name="city" id="ciudad">
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Código Postal:</h6>
                </div>
                <div class="col l4 s12 indicator oculta">
                    <input class="inputs-register" placeholder="Código Postal" type="text" name="postal_code" id="postal_code">
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Dirección:</h6>
                </div>
                <div class="col l4 s12 indicator oculta">
                    <input class="inputs-register" placeholder="Dirección" type="text" name="address" id="address">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Contraseña:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Contraseña" type="password" name="password" id="password">
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l3 s12 offset-l2">
                    <h6 class="font-bold">Confirmar Contraseña:</h6>
                </div>
                <div class="col l4 s12">
                    <input class="inputs-register" placeholder="Confirmar Contraseña" type="password" name="password_confirmation" id="confirm_password">
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Descripción:</h6>
                </div>
                <div class="col l4 s12 indicator oculta">
                    <input class="inputs-register" placeholder="Descripción de la empresa" type="text" name="description" id="description">
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Página Web:</h6>
                </div>
                <div class="col l4 s12 indicator oculta">
                    <input class="inputs-register" placeholder="Página Web" type="text" name="website" id="web">
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Sector de Actividad:</h6>
                </div>
                <div class="col l4 select-register indicator oculta">
                    <select name="sector" id="sector">
                        
                    </select>
                </div>
                <div class="col l12 s12 separator indicator oculta"></div>
                <div class="col l3 s12 offset-l2 indicator oculta">
                    <h6 class="font-bold">Categoría:</h6>
                </div>
                <div class="col l4 select-register indicator oculta">
                    <select name="category" id="category">
                        <option value="Selecciona una categoria">Selecciona una categoria</option>
                    </select>
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l12 s12 separator"></div>
                <div class="col l7 s12 offset-l2 center-align">
                    <button type="submit" class="btn color-japy" id="save_register">Registrar</button>
                </div>
                <div class="col l12 s12 separator"></div>
                <div class="col l12 s12 separator"></div>
                <input type="hidden" name="latitude" id="latitude">
                <input type="hidden" name="longitude" id="longitude">
            </form>
            </div>
        </div>
    </div>
    <script src="js/register.js"></script>
    @include('footer')
</html>