<div class="row">
    <nav>
        <div class="nav-wrapper">
            <div class="col l2 large-div-menu">
                <a href="{{URL::asset('/')}}"><img class="logo-menu" src="{{URL::asset('resources/logos/ba-rose.png')}}" alt="BrideAdvisor"></a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-presupuesto1.png')}}" alt="Presupuestador">
                    <span class="pink-text">Presupuesto inteligente</span>
                </a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-mi-perfil1.png')}}" alt="Perfil">
                    <br><span class="pink-text">Perfil</span>
                </a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-tareas1.png')}}" alt="Agenda">
                    <br><span class="pink-text">Agenda</span>
                </a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-misproveedores1.png')}}" alt="Mis proveedores">
                    <br><span class="pink-text">Mis proveedores</span>
                </a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-listadeinvitados1.png')}}" alt="Invitados">
                    <br><span class="pink-text">Invitados</span>
                </a>
            </div>
            <div class="col l1 center-align large-div-menu">
                <a href="">
                    <img class="icon-menu" src="{{URL::asset('resources/icons/icons-color/icono-controldemesas1.png')}}" alt="Organizador de mesas">
                    <br><span class="pink-text">Organizador de mesas</span>
                </a>
            </div>
            <div class="col l2 offset-l1 center-align large-div-menu">
                <a href="{{route('brideweekend')}}">
                    <img class="icon-menu-bw" src="{{URL::asset('resources/logos/bw-pink.png')}}" alt="BrideWeekend">
                </a>
            </div>
            <div class="col l1 left-align large-div-menu">
                @if (!empty($logo))
                    <img class="dropdown-trigger icon-menu" data-target="dropdown1" src="resources/uploads/providers/{{$logo->name_image}}"><br>
                @else
                    <img class="dropdown-trigger icon-menu icon-menu-user" data-target="dropdown1" src="resources/media/user.png">
                @endif
                {{-- <a class="dropdown-trigger username-menu pink-text" href="#!" data-target="dropdown1">{{Auth::user()->name}}</a> --}}
                &nbsp;&nbsp;<a class="dropdown-trigger username-menu pink-text" href="#!" data-target="dropdown1"><i class="fas fa-bars"></i></a>
            </div>
        </div>
    </nav>
    <ul id="dropdown1" class="dropdown-content">
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">dashboard</i>Mi organizador<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">exposure</i>Mi presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">face</i>Mi perfil<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">content_paste</i>Mi agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">class</i>Mis proveedores<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">people_outline</i>Mis invitados<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li>
            <a class="grey-text darken-5" href=""><i class="material-icons left primary-text">settings</i>Mi cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
        </li>
        <li class="divider"></li>
        <li>
            <a class="grey-text darken-5" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fas fa-times"></i>
                Cerrar sesión 
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }} 
            </form>
        </li>
    </ul>
</div>