<div id="modalPayments{{$details->id}}" class="modal">
    <div class="modal-content">
        <h5>Registrar Pagos</h5><br>
        <div class="col l12 m12 s12">

            <table class="centered" id="">
                <thead>
                    <tr>
                        <th>Estado</th>
                        <th>Importe</th>
                        <th>Vencimiento</th>
                        <th>Pagado por</th>
                        <th>Fecha de Pago</th>
                        <th>Método de pago</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody id="content-payment{{$details->id}}">
                    
                </tbody>
            </table>

        </div>
        <div class="col l12 m12 s12 center-align">
            <br>

           <a class="addPayment waves-effect waves-light btn"><input class="id" type="hidden" value="{{$details->id}}"/>Agregar Pago</a>

        </div>
    </div>
    <div class="modal-footer col l12 m12 s12">
        <span class="modal-close waves-effect waves-green btn-flat closeModal">Cerrar</span>
    </div>
</div>