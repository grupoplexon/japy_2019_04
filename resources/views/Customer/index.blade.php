<!DOCTYPE html>
<html lang="es">
<head>
    @include('Customer.styles')
    <title>BrideAdvisor - Presupuestador</title>
    <link rel="stylesheet" href="{{URL::asset('datatables/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/customer/presupuestador.css')}}">
</head>
<body>
    @include('Customer.menu')
    <div class="row">
        <div class="col l12 m12 s12 width-content-pres">
            <div class="col l9 center-align">
                <ul id="tabs-swipe-demo" class="tabs">
                    <li class="tab col l6 s3"><a class="active grey-text bold title-pres" href="#test-swipe-1">PRESUPUESTO DE BODA</a></li>
                    <li class="tab col l6 s3"><a class="grey-text bold title-pres" href="#test-swipe-2">PANEL DE PAGOS</a></li>
                </ul>
                <br><h4 class="grey-text text-darken-2 bold">Presupuestador Inteligente</h4><br>
                <div id="test-swipe-1" class="col l12 m12 s12">
                    <ul class="collapsible expandable">
                        <input type="hidden" id="URL" value="{{URL::asset('')}}">
                        <input type="hidden" id="BUDGET" value="{{$customer->budget}}">
                        @if(empty($customer->budget))
                            <div id="modal1" class="modal modalWelcome openModal">
                                <div class="modal-content">
                                    <img class="logoWelcome" src="resources/media/indicador_pres.png">
                                </div>
                            </div>
                            <div id="modal2" class="modal">
                                <div class="modal-content">
                                    <h5>Bienvenida/o a BrideAdvisor</h5><br>
                                    <form action="{{route('saveBudget')}}" method="POST">
                                        {{ csrf_field() }}
                                        <div class="col l3 m6 s12 offset-l2 right-align">
                                            <p>Presupuesto: &nbsp;</p>
                                        </div>
                                        <div class="col l5 m6 s12 left-align">
                                            <input class="inputs input-presupuesto" name="budget" type="text" required>
                                        </div>
                                        <div class="col l12 m12 s12"><br></div>
                                        <div class="col l3 m4 s12 offset-l2 right-align">
                                            <p>Número de invitados: &nbsp;</p>
                                        </div>
                                        <div class="col l5 m3 s12">
                                            <input class="inputs" type="number" name="quantity" min="0" required>
                                        </div>
                                        <div class="col l12 m12 s12 center-align">
                                            <br>
                                            <button class="btn pink lighten-5 grey-text text-darken-2">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer col l12 m12 s12">
                                    <span class="modal-close waves-effect waves-green btn-flat closeModal">Cerrar</span>
                                </div>
                            </div>
                        @endif
                    @php
                 
                    @endphp
                        @foreach ($budgets  as $key => $value)
                            @php
                            $sum = 0;
                            @endphp
                
                                <li class="space-list">
                                    <div class="collapsible-header">
                                        <span class="title-left">
                                            <img class="icons" src="resources/icons/providers/{{$value->img}}">
                                            &nbsp;<i class="text-title">{{$value->name_category}}</i>
                                        </span>
                                        <span class="title-right">@if(empty($value->budget))$ 0.00&nbsp; @else $ {{number_format($value->budget, 2)}}&nbsp; @endif<i class="fas fa-sort-down down-right pink-text"></i></span>
                                    </div>
                                    <div class="collapsible-body">
                                        @if(!empty($customer->budget))
                                        <table class="centered table-index responsive-table" id="exampleTable">
                                            <thead>
                                                <tr>
                                                    <th>Concepto</th>
                                                    <th class="columns">Presupuesto</th>
                                                    <th class="columns">Costo Final</th>
                                                    <th class="columns">Pagado</th>
                                                    <th class="columns">Pendiente</th>
                                                    <th>Fecha</th>
                                                    <th class="add pointer blue-text lighten-text-3"  data-category="{{$value->category_id}}" data-id="{{$value->id}}">Añadir &nbsp;&nbsp;<i class="fas fa-plus"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody id="content-{{$value->id}}">
                                                @php
                                                    $budgetDetails = App\BudgetDetail::where('budget_id', $value->id)->get();
                                                @endphp
                                                @foreach ($budgetDetails as $detail)
                                              
                                                
                                                <tr>
                                                    <td><input class="input-presupuesto" type="text" value="{{$detail->name}}" data-id="{{$detail->id}}" data-tb="name"></td>
                                                    <td>
                                                        <input class="input-presupuesto number_format " type="text" value="$ {{number_format($detail->budget, 2)}}" data-id="{{$detail->id}}" data-tb="budget">
                                                    </td>
                                                    <td>
                                                        <input class="input-presupuesto number_format " type="text" value="${{$detail->final_cost}} " data-id="{{$value->id}}" data-tb="final_cost">
                                                    </td>
                                                    <td><a href="#modalPayments{{$detail->id}}" class="modal-trigger"><span class="pointer modalPayments blue-text lighten-text-3" data-id="{{$detail->id}}">{{$sum}}</span></a></td>

                                                    <td>5</td>
                                                    <td>
                                                        <input class="date" type="date" value="{{$detail->date_asigned}}" data-id="{{$detail->id}}" data-tb="date_asigned">
                                                    </td>
                                                    <td class="col6" style="width: 20%;">
                                                        <a href="" class="tooltipped" data-position="top" data-tooltip="Agregar nota" ><i class="material-icons modal-trigger" href="#note{{$detail->id}}">note</i></a>

                                                        <a class="deleteColumn"><input class="deleteRow" type="hidden" value="{{$detail->id}}"><i class="material-icons">delete</i></a>
                                                        <i class="material-icons">book</i>
                                                        <span>
                                                            <input type="checkbox" class="filled-in" id="filled-in-box{{$detail->id}}" checked="checked" />
                                                            <label for="filled-in-box{{$detail->id}}"></label>
                                                          </span>
                                                  
                                                    </td>
                                                </tr>
                                               
                                                @endforeach

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td>TOTAL</td>
                                                    <td class=""></td>
                                                    <td class=""> <span id="total{{$value->id}}" class="total"></td></span>
                                                    <td class=""> </td>
                                                    <td class=""> </td>
                                                  
                                                </tr>
                                            </tfoot>
                                        </table>
                                       
                                           
                                        @else
                                            <br>
                                            <h6 class="grey-text">Para utilizar el presupuestador es necesario que ingreses tu presupuesto para la boda. <span class="openModal">Has click aquí<span></h6>
                                        @endif
                                    </div>
                                </li>
                         
                                @foreach ($budgetDetails as $details) @include('Customer.modals.modalPayments') @endforeach
                               <!-- Modal Structure -->
                               <div id="note{{$value->id}}" style="height:60%;" class="modal">
                                <div class="modal-content" >
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">mode_edit</i>
                                        <textarea id="contenido-nota{{$value->id}}" class="materialize-textarea  contenido-nota" length="255"></textarea>
                                        <label for="icon_prefix2" class="">Nota</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <a href="#!" class="modal-close waves-effect waves-green btn-flat note"><input class="add-note" type="hidden" value="{{$value->id}}">ACCEPTAR</a>
                                </div>
                            </div>
                        @endforeach
                    </ul>
                </div>
                <div id="test-swipe-2" class="col s12 red">PANEL DE PAGOS</div>
            </div>
            <div class="col l3">
                <div class="col l12 m12 s12 border-pres">
                    <h4 class="grey-text text-darken-2">Presupuestador maestro</h4><br>
                    <div class="col l2">
                        <div class="icon-calculator"></div>
                    </div>
                    <div class="col l10">
                        <span class="grey-text text-darken-2">Costo aproximado</span><br>
                        <span class="grey-text text-darken-2 bold">$ 100,000.00</span>
                    </div>
                    <div class="col l12 m12 s12"><br></div>
                    <div class="col l2">
                        <div class="icon-calculator2"></div>
                    </div>
                    <div class="col l10">
                        <span class="grey-text text-darken-2">Costo final</span><br>
                        <span id="totalCost" class="grey-text text-darken-2 bold totalCost">{{$totalCost}}</span>
                    </div>
                    <div class="col l12 m12 s12"><br></div>
                    <div class="col l2">
                        <div class="icon-payment"></div>
                    </div>
                    <div class="col l10">
                        <span class="grey-text text-darken-2">Pagado</span><br>
                        <span class="grey-text text-darken-2 bold">{{$totalPayment}}</span>
                    </div>
                    <div class="col l12 m12 s12"><br></div>
                    <div class="col l2">
                        <div class="icon-pending"></div>
                    </div>
                    <div class="col l10">
                        <span class="grey-text text-darken-2">Pendiente</span><br>
                        <span class="grey-text text-darken-2 bold">{{$pending}}</span>
                    </div>
                    <div class="col l12 m12 s12"><br></div>
                </div>
            </div>
        </div>
        <div class="col l12 m12 s12 space"></div>
    </div>

    @include('footer')
    <script>
        $(document).ready(function(){
            $('.tooltipped').tooltip();
            $(document).on("keyup", ".final_cost", function() {
                var sum = 0;
                var total = 0;
                val = $(this).attr('data-value');
                id = $(this).attr('data-category');
            $(".final_cost"+id).each(function(){
                var res = parseFloat($(this).val().replace("$", ""));
                sum += +res;
            });

            $(".final_cost").each(function(){
                var tot = parseFloat($(this).val().replace("$", ""));
               
                total += +tot;
            });
            $("#totalCost").html(total);
            $("#total"+val).html(sum);

                    // inputs-payment

                    });
        });
        </script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script> --}}
    {{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
    {{-- <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script> --}}
    <script src="{{URL::asset('js/jquery.maskMoney.min.js')}}"></script>
    {{-- <script src="{{URL::asset('datatables/datatables.editor.min.js')}}"></script> --}}
    {{-- <script src="{{URL::asset('datatables/editor/js/dataTables.editor.min.js')}}"></script> --}}
    {{-- <script src="{{URL::asset('datatables/datatables.min.js')}}"></script> --}}
    <script src="{{URL::asset('js/customer/presupuestador.js')}}"></script>
</body>
</html>