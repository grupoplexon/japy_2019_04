<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor - Registro</title>
    <link rel="stylesheet" href="{{ URL::asset('css/registroEmpresa.css') }}">
</head>
<body>
    @include('menu')
    <div class="row background-row">
        <div class="container">
            <div class="col l12 m12 s12 backgroung-head">
                <div class="col l4 s12 m6">
                    <br><br>
                    <img class="img" src="resources/logos/BrideAdvisorBlack.png">
                </div>
                <div class="col l4 s12 m6 offset-l4">
                    <br>
                    <h5>¡Haga crecer su negocio con BrideAdvisor.mx!</h5>
                    <p class="justify">
                        Reciba solicitudes de presupuesto de novios interesados.
                        <br>
                        Consiga nuevos clientes y multiplique el éxito de su negocio.
                    </p><br>
                </div>
            </div>
            <div class="col l12 m12 s12 white content">
                <form action="{{ route('registerProvider') }}" method="POST" id="registro">
                    {{csrf_field()}}
                    <div class="col l12 m12 s12">
                        <div class="col l8 m8">
                            <h4>Datos de contacto</h4><br>
                            <label>Persona de contacto:</label>
                            <input class="inputs" type="text" required name="name">
                            <label>Correo electrónico:</label>
                            <input class="inputs" type="email" required name="email">
                            <label>Celular:</label>
                            <input class="inputs" type="number" required min="0" name="cellphone">
                            <label>Teléfono:</label>
                            <input class="inputs" type="number" required min="0" name="phone">
                            <label>Página web:</label>
                            <input class="inputs" type="text" name="website">
                        </div>
                        <div class="col l4 m4 hide-on-small-only">
                            <br><br>
                            <div class="card background-card">
                                <div class="card-content">
                                    <p class="justify text-cards">
                                        Recibirás las solicitudes de información de los usuarios 
                                        interesados ​​en la dirección de correo electrónico que hayas 
                                        indicado, así como todas las novedades del portal que pueden 
                                        ser de tu interés.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col l12 m12 s12">
                        <div class="col l8 m8">
                            <h4>Empresa</h4><br>
                            <label>Nombre:</label>
                            <input class="inputs" type="text" required name="name_company">
                            <label>Descripción de su empresa:</label>
                            <textarea class="inputs editable" rows="30" name="description"></textarea><br>
                            <label>Pais:</label>
                            <select class="inputs" id="countries" name="country">
                                <option value="" selected disabled>Selecciona un pais</option>
                                <option value="142">México</option>
                                @foreach ($countries as $item)
                                    <option value="{{$item->id}}">{{$item->country}}</option>
                                @endforeach
                            </select><br>
                            <label>Estado:</label>
                            <select class="inputs" id="states" name="state">
                                <option value="" selected disabled>Selecciona un estado</option>
                            </select><br>
                            <label>Población:</label>
                            <select class="inputs" id="cities" name="city">
                                <option value="" selected disabled>Selecciona una ciudad</option>
                            </select><br>
                            <label>Código postal:</label>
                            <input class="inputs" type="number" required min="0" name="postal_code">
                            <label>Dirección:</label>
                            <input class="inputs" type="text" required name="address">
                        </div>
                        <div class="col l4 m4 hide-on-small-only">
                            <br><br>
                            <div class="card background-card">
                                <div class="card-content">
                                    <p class="justify text-cards">
                                        Describa específicamente su empresa así como los servicios o productos de bodas que ofrece con la máxima información de interés para los novios.
                                        <br><br>
                                        No se pueden incluir datos de contacto como correo electrónico, teléfono, página web, dirección, etc.
                                        <br><br>
                                        La dirección de su empresa para que los usuarios interesados ​​puedan ver su localización. Tenga en cuenta que el buscador y los mapas se basan en esta información.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col l12 m12 s12">
                        <div class="col l8 m8">
                            <h4>Datos de acceso</h4><br>
                            <label>Usuario:</label>
                            <input class="inputs" type="text" required name="user" id="user" autocomplete="new-user">
                            <label>Contraseña:</label>
                            <input class="inputs" type="password" required name="password" id="password" autocomplete="new-password">
                            <label>Confirmar contraseña:</label>
                            <input class="inputs" type="password" required name="confirm_password" id="password_2" autocomplete="new-password2">
                        </div>
                        <div class="col l4 m4 hide-on-small-only">
                            <br><br>
                            <div class="card background-card">
                                <div class="card-content">
                                    <p class="justify text-cards">
                                        El Usuario debe tener por lo menos 6 caracteres. <br>
                                        La Contraseña debe tener por lo menos 8 caracteres. <br>
                                        Tome en cuenta mayúsculas y minúsculas.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col l12 m12 s12">
                        <h4>Categoría</h4>
                        <input type="hidden" id="location" name="location">
                        <select class="inputs" id="" name="category">
                            <option value="" selected disabled>Seleccione su categoría</option>
                            @foreach ($data as $item)
                                <option value="{{$item->id}}">{{ $item->name_category }}</option>
                            @endforeach
                        </select>
                        <br>
                    </div>
                    <div class="col l12 m12 s12 center-align">
                        <p>Al presionar "Aceptar" estás aceptando expresamente nuestras condiciones legales .</p>
                        <button class="btn" type="submit" id="btn_aceptar">Aceptar</button>
                        <br><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/parsley.js') }}"></script>
    <script src="{{ URL::asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('js/registroEmpresa.js') }}"></script>
</body>
</html>