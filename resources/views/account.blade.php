<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor - Cuenta</title>
    <link rel="stylesheet" href="{{ URL::asset('css/account.css') }}">
</head>
<body>
    @include('menu')
    <div class="row background-row padding-row">
        <div class="container">
            <div class="col l5 m6">
                <div class="col l12 s12 border-head paddings card-shadow background-bride">
                    <div class="border-head background col l12 s12 center-align">
                        <div class="col l12 s12 m12">
                            <h4 class="title-card">NOVIO / NOVIA</h4>
                        </div>
                    </div>
                </div>
                <div class="col l12 s12 m12 white center-align paddings card-shadow border-footer large-fcard">
                    <div class="col l8 offset-l2">
                        <p class="margin-desc color-text">Registra aquí tus datos para comenzar a organizar tu boda con las herramientas y proveedores que tenemos para ti.</p>
                        <a class="btn grey darken-4" href="{{ route('loginNovia') }}">Inicia sesión</a>
                    </div>
                    <div class="col l12 s12 center-align">
                        <br>
                        <p class="color-text">Si no tienes una cuenta, <a class="redirect" href="{{ route('registroNovia') }}">haz click aquí.</a></p>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col s12 hide-on-large-only hide-on-med-only space">

            </div>
            <div class="col l5 m6 offset-l1">
                <div class="col l12 s12 border-head paddings card-shadow background-company">
                    <div class="border-head background col l12 s12 center-align">
                        <div class="col l12 s12 m12">
                            <h4 class="title-card">EMPRESA</h4>
                        </div>
                    </div>
                </div>
                <div class="col l12 s12 m12 white center-align paddings card-shadow border-footer large-fcard">
                    <div class="col l8 offset-l2">
                        <p class="margin-desc color-text">Registra tu perfil como proveedor y ten acceso a todo el mercado de novios para que tu negocio crezca.</p>
                        <br>
                        <a class="btn grey darken-4" href="{{ route('loginEmpresa') }}">Inicia sesión</a>
                    </div>
                    <div class="col l12 s12 center-align">
                        <br>
                        <p class="color-text">Si no tienes una cuenta, <a class="redirect" href="{{ route('registroEmpresa') }}">haz click aquí.</a></p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</body>
</html>