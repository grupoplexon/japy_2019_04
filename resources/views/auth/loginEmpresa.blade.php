<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>BrideAdvisor - Login Empresa</title>
    <link rel="stylesheet" href="{{ URL::asset('css/loginNovia.css') }}">
    {{-- <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v4.0"></script> --}}
</head>
<body>
    @include('menu')
    <div class="row backgrounRow">
        <div class="col l4 s12 m6 offset-l4 offset-m4">
            <div class="col l12 m12 s12 center-align backgroundHeadCompany">
                <div class="col l12 s12 m12 transparent">
                    <h4 class="title-card">EMPRESA</h4>
                </div>
            </div>
            <div class="col l12 m12 s12 white center-align shadow">
                <form action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}
                    <br>
                    <p class="access">Accede con tu usuario</p>
                    <br>
                    <input class="inputs" id="email" placeholder="Usuario" type="text" required name="user" autofocus autocomplete="new-user">
                    <br>
                    <input class="inputs" id="password" placeholder="Contraseña" type="password" required name="password" autocomplete="new-password">
                    @if ($errors->has('user'))
                        <br>
                        <span class="help-block">
                            <strong class="incorrect">Usuario/Contraseña incorrectos.</strong>
                        </span>
                    @endif
                    <p class="color-text">¿Has olvidado tu contraseña? <a class="link" href="">recupérala aquí.</a></p>
                    <br><button class="btn pink lighten-5 color-text">INGRESAR</button>
                    <p class="color-text">¿No tienes una cuenta? <a class="link" href="{{ route('registroEmpresa') }}">Regístrate aquí.</a></p>
                    <br>
                </form>
            </div>
        </div>
    </div>
    @include('footer')
    <script src="{{ URL::asset('js/parsley.js') }}"></script>
    {{-- <script src="{{ URL::asset('js/registroEmpresa.js') }}"></script> --}}
</body>
</html>
