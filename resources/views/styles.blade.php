<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
{{-- <script src="css/materialize.min.css"></script> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{URL::asset('css/footer.css')}}">
<link href="{{URL::asset('css/fontawesome-5.8.1/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="icon" href="{{URL::asset('resources/logos/baIcon.png')}}">
<link rel="stylesheet" href="{{URL::asset('css/style_brideadvisor.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">