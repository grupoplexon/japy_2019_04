<!DOCTYPE html>
<html lang="es">
<head>
    @include('styles')
    <title>Conocenos - BrideAdvisor</title>
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/conocenos.css') }}">
</head>
<body>
    @include('menu')
    <div class="row background-page">
        <img class="img-content" src="resources/media/conocenos.png">
        <div class="col l10 offset-l1 pink lighten-5 content-menu">
            <div class="col l2 center-align border-right">
                <h5 class="title" onclick="view('index')">Planeador de boda</h5>
            </div>
            <div class="col l2 center-align border-right">
                <h5 class="title" onclick="view('agenda')">Agenda</h5>
            </div>
            <div class="col l2 center-align">
                <h5 class="title" onclick="view('invitados')">Invitados & Control de Mesas</h5>
            </div>
            <div class="col l2 center-align border-right border-left">
                <h5 class="title" onclick="view('proveedores')">Proveedores</h5>
            </div>
            <div class="col l2 center-align">
                <h5 class="title" onclick="view('presupuestador')">Presupuestador inteligente</h5>
            </div>
            <div class="col l2 center-align border-left">
                <h5 class="title" onclick="view('vestidos')">Mis vestidos</h5>
            </div>
        </div>
    </div>
    <div class="row space-content">
        <div class="col l4">
            <hr>
        </div>
        <div class="col l4 center-align">
            <h5 id="title-tabs">MI PLANEADOR DE BODA</h5>
        </div>
        <div class="col l4">
            <hr>
        </div>
        <div class="col l12 m12 s12 space-content-two">
            @include('conocenos.conocenos')
            @include('conocenos.agenda')
            @include('conocenos.invitados')
            @include('conocenos.proveedores')
            @include('conocenos.presupuestador')
            @include('conocenos.vestidos')
        </div>
    </div>
    @include('footer')
    <script src="{{URL::asset('js/conocenos.js')}}"></script>
</body>
</html>