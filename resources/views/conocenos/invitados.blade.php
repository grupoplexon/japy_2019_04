<div class="row row-tabs oculto" id="invitados">
    <div class="row container">
        <div class="col s12 m6" style="text-align: center;">
            <p>
                <img src="resources/icons/icons-pink/invitados.png">
                <h4>Agregar invitados.</h4>
                <h3>Adjunta un documento de excel o agrega uno por uno.</h3>
            </p>
        </div>
        <div class="col s12 m6" style="text-align: center">
            <p>
                <img src="resources/icons/icons-pink/mesas.png">
                <h4>Acomodo de mesas</h4>
                <h3>Haz las agrupaciones necesarias para que tengas el mejor
                    acomodo.</h3>
            </p>
        </div>
    </div>
    <div class="row  center-align" style="background: white;">
        <div class="row container" >
            <br>
            <h4 >¿Qui&eacute;nes ya confirmaron?</h4>
            <h3>Puedes llevar un control en tiempo real de los invitados
                confirmados.</h3>
            <br><br>
            <img src="resources/media/asistencia.png"
                    style="max-width: 100%; height: auto">
        </div>
    </div>
    <br>
    <div class="row container center-align" >
        <h4>Acomodo de mesas e invitados</h4>
        <h3>Cuando est&eacute;s acomodando tus mesas,
            podr&aacute;s ver el nombre de tus invitados, de esta manera ser&aacute; m&aacute;s f&aacute;cil
            identificarlos y seleccionar las mesas.</h3>
    </div>
    <br><br>
    <div class="row center-align" style="background: white;">
        <div class="col s12 l6 valign-wrapper caja">
            <div class="row container">
            <h4>CONTROL DE MESAS</h4>
            <h3>Ya no te preocupes por la asignación de mesas y acomodo de invitados.</h3>
            <br><br>
            <h3>Cada ajuste y cambio que hagas en el acomodo de las mesas, podrás verlo en tiempo real y hacer todos los movimientos que creas convenientes.</h3>
            </div>
        </div>
        <div class="col s12 l6 titulo">
            <br>
            <img src="resources/media/control_mesas1.png" style="max-width: 100%; height: auto">
        </div>
    </div>
</div>