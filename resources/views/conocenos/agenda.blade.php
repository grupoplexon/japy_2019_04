<div class="row row-tabs oculto" id="agenda">
    <div class="row container center-align" >
        <h5>
            Aquí encontrarás una lista de actividades por realizar con sus respectivas fechas.
            <br/>Llena cada espacio y que no te falte nada para el gran día.
        </h5>
    </div>
    <br>
    <div class="row titulo" >
        <div class="col s12 m6 l6 titulo" >
            <img class="responsive-img col s12 m10 right" src="resources/media/img-1.png">
        </div>
        <div class="col s12 m6 l6 center-align valign-wrapper caja" >
            <div class="row container">
                <h4> No olvides agendar todos tus pendientes y actividades</h4>
                <h3>
                    Una de las funciones de BrideAdvisor es ayudarte a planificar tus actividades, así que revisa tus
                    pendientes cuantas veces necesites.</h3>
            </div>
        </div>
    </div>
    <div class="row titulo">
        <div class="col s12 m6 l6  center-align valign-wrapper caja"  >
            <div class="row container">
                <h4> Además de las tareas que BrideAdvisor tiene para ti, tú puedes enlistar todas tus
                    actividades pendientes.</h4>
            </div>
        </div>
        <div class="col s12 m6 l6">
            <img class="responsive-img col s12 m8" src="resources/media/img-2.png">
        </div>
    </div>
    <div class="row titulo" >
        <div class="col s12 m12 l6 " >
            <div class="row container ">
                <img class="responsive-img " src="resources/media/img-4.png">
                <img class="responsive-img" src="resources/media/img-6.png">
            </div>
        </div>
        <div class="col s12 m12 l6 center-align valign-wrapper caja" style="">
            <div class="row container">
                <h4>
                    Las herramientas que tenemos para ti están conectadas entre sí, para que puedas llevar un
                    control entre tu presupuesto, agenda y proveedores.
                </h4>
            </div>
        </div>
    </div>
</div>