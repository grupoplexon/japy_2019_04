<div class="row row-tabs oculto" id="proveedores">
    <div class="row container center-align">
        <h5>Encuentra a todos los proveedores que necesites para cubrir cada detalle de tu boda, además de que podrás elegir a tus favoritos y recibir notificaciones sobre información de tu presupuesto.</h5>
    </div>
    <div class="row  center-align" style="background: white;">
        <div class="row container" >
            <img src="resources/media/macbook_proveedores2.png"
                    style="max-width: 100%; height: auto">
        </div>
    </div>
    <div class="row container center-align">
        <div class="col s12 l6 caja valign-wrapper">
            <div class="row">
                <h4>Tenemos a los mejores proveedores para ti</h4>
                <h3>
                    Busca varias opciones para que puedas escoger la mejor, en precio, calidad y sobre todo en gusto. Irás creando tu base de datos y así podrás tener la información de cada uno.
                </h3>
            </div>
        </div>
        <div class="col s12 l6 center-align ">
            <div class="row " >
                <img  src="resources/media/tarjeta_proveedor.png"  style="max-width: 50%; height: auto">
            </div>
        </div>
    </div>
    <div class="row center-align titulo" style="background: white;">
        <div class="col s12 l6 hide-on-med-and-down">
            <img src="resources/media/proveedor1.png">
            <img src="resources/media/proveedor2.png">
        </div>
        <div class="col l6 s12 valign-wrapper ">
            <div class="row container ">
            <h4>Tu mejor decisión</h4>
            <h3>Los proveedores que tenemos en brideadvisor.mx compartirán contigo imágenes sobre sus productos y servicios, como promociones para mejorar tu presupuesto.</h3>
            </div>
        </div>
    </div>
    <div class="row  center-align">
        <div class="col s12 l6 caja valign-wrapper">
            <div class="row">
                <h4>Las mejores recomendaciones</h4>
                <h3>
                    Olvídate de preocupaciones y conoce lo que las novias anteriores dijeron sobre los proveedores y su experiencia con ellos.
                </h3>
            </div>
        </div>
        <div class="col s12 l6">
            <img src="resources/media/recomendacion.png" style="max-width: 100%; height: auto"> 
        </div>
    </div>
</div>