<div class="row  row-tabs" id="index">
    <div class="row">
        <div class="col l2 offset-l1 center-align">
            <img class="icon" src="resources/icons/icons-pink/agenda.png" alt="">
            <br>
            <h5>AGENDA</h5>
            <h6>No olvides agendar todos tus pendientes y actividades.</h6>
        </div>     
        <div class="col l2  center-align">
            <img class="icon" src="resources/icons/icons-pink/invitados.png" alt="">
            <br>
            <h5>INVITADOS</h5>
            <h6>Haz una lista con la selección de tus invitados, sus contactos y mantén tus confirmaciones al día.</h6>
        </div>     
        <div class="col l2 offset-l2 center-align ">
            <img class="icon" src="resources/icons/icons-pink/mesas.png" alt="">   
            <br>
            <h5>CONTROL DE MESAS</h5>
            <h6>Esta parte es divertida, ya que podrás acomodar a tus invitados.</h6>
        </div>     
        <div class="col l2 center-align">
            <img class="icon" src="resources/icons/icons-pink/proveedores.png" alt="">
            <br>
            <h5>PROVEEDORES</h5>
            <h6>Tu agenda personal con tus proveedores, tus pagos y pendientes.</h6>
        </div>     
    </div>
    <br>
    <div class="row">
        <div class="col l2 offset-l2 center-align">
            <img class="icon" src="resources/icons/icons-pink/presupuesto.png" alt="">
            <br>
            <h5>PRESUPUESTADOR INTELIGENTE</h5>
            <h6>Te ayuda a administrar y maximizar tu dinero y a seleccionar el proveedor ideal de acuerdo a tus necesidades.</h6>
        </div>     
        <div class="col l2 offset-l1 center-align">
            <img class="icon" src="resources/icons/icons-pink/vestidos.png" alt="">
            <br>
            <h5>MIS VESTIDOS</h5>
            <h6>Encontrarás diseños exclusivos de las mejores marcas, para que elijas de tus favoritos</h6>
        </div>     
        <div class="col l2 offset-l1 center-align ">
            <img class="icon" src="resources/icons/icons-pink/portal.png" alt=""> 
            <br>
            <h5>MI PORTAL WEB</h5>
            <h6>Podrás compartir información de tu boda con tus invitados a través de redes sociales.</h6>
        </div>  
    </div>
</div>