<div class="row row-tabs oculto" id="presupuestador">
    <div class="row container center-align">
        <h5>
        Con esta herramienta podrá maximizar tu presupuesto y estar pendiente delo que tienes que pagar y lo que ya 
        pagaste. De esta manera no te faltará nada por cubrir. Además te recomendaremos a los mejores proveedores de 
        acuerdo a tu presupuesto.
        </h5>
    </div>
    <div class="row  center-align" style="background: white;"   >
        <br><br>
        <div class="row container">
            <div class="col s12 l6">
                <h4>PAGOS A PROVEEDORES</h4>
                <h3> Con el presupuestador inteligente podrás ver los pagos realizados y lo pendiente por pagar. 
                Además te permite agendar los pendientes en las notas para que no olvides nada.</h3>
            </div>
            <div class="col s12 l6">
                <img class="responsive-img col s12 m12" src="resources/media/costo.png"
                        alt="">
            </div>
        </div>
    </div>
    <br><br>
    <div class="row container center-align">
        <br>
        <div class="col s12 l6">
            <img class="responsive-img col s12 m12" src="resources/media/presupuesto.png"
                    style="width: 50%;" alt="">
        </div>
        <div class="col s12 l6 caja valign-wrapper">
            <div class="row">
                <h4>REVISA TU PRESUPUESTO</h4>
                <h3> Recuerda que una vez que nos compartes tu presupuesto aproximado total, seleccionaremos a los 
                proveedores que estén dentro de tu rango, además de agendar tus pagos y pendientes.</h3>
            </div>
        </div>
    </div>
</div>