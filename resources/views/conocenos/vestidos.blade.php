<div class="row row-tabs oculto" id="vestidos">
    <div class="row container center-align">
        <h5 style="font-weight: bold;">
        Encontrarás los mejores diseños de las marcas más importantes
        </h5>
        <h5>
            Cada proveedor te mostrará los diseños en tendencia y dará consejos para que puedas
            escoger el mejor vestido y así luzcas espectacular en tu gran día. Además encontrarás hermosos vestidos para
            tus damas de honor.
        </h5>
    </div>
    <div class="row container center-align">
        <div class="col l6 s12 caja valign-wrapper">
            <div class="row">
                <h4>Ponte en contacto con tu proveedor</h4>
                <h3>Agenda una cita con tu proveedor, una vez que hayas seleccionado tu vestido de novia.</h3>
            </div>
        </div>
        <div class="col l6 s12">
            <img class="responsive-img col s12 m12" src="resources/media/contactar.png">    
        </div>
    </div>
</div>